module.exports = {
  extends: ['@groupher/eslint-config-web'],
  settings: {
    'import/resolver': {
      'babel-module': {},
      'eslint-import-resolver-custom-alias': {
        alias: {
          '@/components': 'src/components',
          '@/containers': 'src/containers',
          '@/atoms': 'src/components/atoms/',
          '@/molecules': 'src/components/molecules/',
          '@/templates': 'src/components/templates/',
          '@/organisms': 'src/components/organisms/',
          '@/hooks': 'src/components/functionals/hooks/',
          '@/schemas': 'src/schemas/',
          '@/pages': 'src/components/pages/',
          '@/stores': 'src/stores/',
          '@/config': 'config/',
          '@/utils': 'src/utils',
          '@/vars': 'src/utils/vars/',
          '@/i18n': 'i18n'
        },
        extensions: ['.js', '.jsx']
      },
    },
  },
  rules: {
    'linebreak-style': ['error', 'windows']
  }
}
