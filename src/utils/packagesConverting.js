import { assoc } from 'ramda';
import { pipe } from './index';

export const converPackageToClient = (currPackage) => {
  return {
    id: currPackage.id,
    description: currPackage.places,
    title: currPackage.name,
    weight: currPackage.weight
  }
}

export const convertPackageList = (packList, toServer = false) => {
  return packList.map((el) => {
    if (toServer) {
      return converPackageToServer(el);
    }
    return converPackageToClient(el);
  })
};

export const converPackageToServer = (currPackage, id) => {
  const { gHeight, gLength, gWidth } = currPackage;
  const volume = gHeight * gLength * gWidth;
  return pipe(currPackage)(
    (x) => assoc('name', currPackage.title, x),
    (x) => assoc('gWidth', gWidth, x),
    (x) => assoc('gHeight', gHeight, x),
    (x) => assoc('gLength', gLength, x),
    (x) => assoc('volume', volume, x),
    (x) => assoc('price', 100, x),
    (x) => assoc('count', 1, x),
    (x) => assoc('orderId', id, x),
    (x) => assoc('places', currPackage.description, x)
  )
}
