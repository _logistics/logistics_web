import { notification } from 'antd';

export const wrapNumberMask = (value) => {
  if (value === '') return '';
  let str = '';
  const num = /[0-9]/g;
  if (value) str = value.match(num);
  if (str) {
    str = str.join('');
    return str;
  }
  return '';
}

export const openErrorNotifi = (title, description) => {
  notification.error({
    message: title,
    description,
    placement: 'bottomRight',
    duration: 0
  });
}

/**
 * Функци создает оповещение
 * @param {string} title
 * @param {string} description
 * @param {string} type может быть: success / info / warning / error
 */

export const openNotifi = (title, description, type) => {
  notification[type]({
    message: title,
    description,
    placement: 'bottomRight'
  });
}
