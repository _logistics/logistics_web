import { types as T, getParent } from 'mobx-state-tree'
import { keys } from 'ramda'

import { DEFAULT_THEME } from '@/config/index'
import { themeSkins } from '@/utils/index'

export const ThemeDefaults = {
  currTheme: DEFAULT_THEME,
}

export const ThemeStore = T.model('ThemeStore', {
  currTheme: T.optional(
    T.enumeration('theme', keys(themeSkins)),
    DEFAULT_THEME,
  ),
})
  .views((self) => ({
    get root() {
      return getParent(self)
    },
    get themeData() {
      return themeSkins[self.currTheme]
    },
  }))
  .actions((self) => ({
    isMemberOf(type) {
      return self.root.isMemberOf(type)
    },
    changeTheme(name) {
      self.currTheme = name
    },
  }))
