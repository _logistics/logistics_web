import { types as T, getParent } from 'mobx-state-tree'
import { markStates } from '@/utils/index'

const SidebarStore = T.model('SidebarStore', {
  pin: T.optional(T.boolean, false),
})
  .views((self) => ({
    get root() {
      return getParent(self)
    },
    get isLogin() {
      return self.root.account.isLogin
    },
    get theme() {
      return self.root.theme
    },
    get getLoading() {
      return self.loading
    },
  }))
  .actions((self) => ({
    mark(sobj) {
      markStates(sobj, self)
    },
  }))

export default SidebarStore
