/* eslint-disable no-unused-vars */

import { types as T } from 'mobx-state-tree';

const userProfile = T.model('userProfile', {
  area: T.maybeNull(T.string),
  comments: T.maybeNull(T.string),
  firstName: T.maybeNull(T.string),
  lastName: T.maybeNull(T.string)
})

const slug = T.model('slug', {
  slug: T.maybeNull(T.string)
})

const profile = T.model('profile', {
  apartment: T.maybeNull(T.string),
  area: T.maybeNull(T.string),
  bik: T.maybeNull(T.string),
  comments: T.maybeNull(T.string),
  contactPersonFio: T.maybeNull(T.string),
  firstName: T.maybeNull(T.string),
  floor: T.maybeNull(T.string),
  houseNumber: T.maybeNull(T.string),
  inn: T.maybeNull(T.string),
  lastName: T.maybeNull(T.string),
  lockedAt: T.maybeNull(T.string),
  mail: T.maybeNull(T.string),
  middleName: T.maybeNull(T.string),
  orgName: T.maybeNull(T.string),
  preferedChanel: T.maybeNull(T.string),
  rs: T.maybeNull(T.string),
  state: T.maybeNull(T.string),
  street: T.maybeNull(T.string),
  telegram: T.maybeNull(T.string),
  town: T.maybeNull(T.string),
  vk: T.maybeNull(T.string),
  whatsapp: T.maybeNull(T.string)
})

const roles = T.model('roles', {
  slug: T.maybeNull(T.string),
  id: T.maybeNull(T.integer)
})

export const User = T.model('User', {
  id: T.maybeNull(T.union(T.integer, T.string)),
  phone: T.maybeNull(T.string),
  token: T.maybeNull(T.string),
  isOrganization: T.maybeNull(T.boolean),
  profile: T.maybeNull(profile),
  roles: T.maybeNull(T.array(roles))
})

export const UserCredentials = T.model('UserCredentials', {
  phone: T.maybeNull(T.string),
  firstName: T.maybeNull(T.string),
  middleName: T.maybeNull(T.string),
  surname: T.maybeNull(T.string),
})

export const EmptyUser = {
  phone: '',
  firstName: '',
  middleName: '',
  surname: '',
}
