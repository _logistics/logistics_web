/* eslint-disable import/prefer-default-export */
import { types as T } from 'mobx-state-tree';

export const PackageItem = T.model('PackageItem', {
  id: T.maybeNull(T.integer),
  title: T.maybeNull(T.string),
  type: T.maybeNull(T.string),
  weight: T.maybeNull(T.number),
  size: T.maybeNull(T.string),
  sizeLength: T.maybeNull(T.number),
  sizeHeight: T.maybeNull(T.number),
  sizeWidth: T.maybeNull(T.number),
  volume: T.maybeNull(T.integer),
  description: T.maybeNull(T.string)
})

export const Packages = T.model('CreatingOrder', {
  packagesList: T.maybeNull(T.array(PackageItem))
})

export const Address = T.model('Address', {
  city: T.maybeNull(T.string),
  street: T.maybeNull(T.string),
  houseNumber: T.maybeNull(T.string),
  apartment: T.maybeNull(T.string),
  phone: T.maybeNull(T.string)
})
