import { types as T } from 'mobx-state-tree'
import { Packages, Address } from '../Models/CreatingOrder'
import {
  CacheStore,
  makeGQClient,
  openErrorNotifi,
  openNotifi,
  stringify,
  parse,
} from '@/utils/index'
import { P } from '@/schemas/index'
import { pipe } from 'ramda'
import { listOrderStatuses } from '@/components/5_pages/EditOrderCont/data'
import { dateTime } from '@dev-bi/rct_utils'

const CreatingOrderStore = T.model('CreateOrderStore', {
  packages: T.optional(Packages, {}),
  workMod: 'selectPackage',
  editablePackageId: T.maybeNull(T.number),
  senderAddres: T.maybeNull(Address),
  reciverAddress: T.maybeNull(Address),
})
  .views((self) => ({
    get self() {
      return self
    },
    getSenderAddres() {
      return self.senderAddres
    },
    getEditablePackage() {
      return self.editablePackage
    },
    get token() {
      return CacheStore.get('token')
    },
  }))
  .actions((self) => ({
    /* eslint-disable */
    async fillOrCreateOrder(userId) {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(
        P.getLastInComplitedOrder,
        { userId: +userId },
      )
      const lastOrder = lastInComplitedOrder.getLastIncompleted
      if (lastOrder) {
        return true
      } else {
        const newOrder = await gqlClient.request(P.createOrder)
        return newOrder
      }
    },
    async getLastInComplitedOrder(userId) {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(
        P.getLastInComplitedOrder,
        { userId: +userId },
      )
      return lastInComplitedOrder
    },
    async getOrder(orderId) {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(P.getOrder, {
        orderId: +orderId,
      })
      return lastInComplitedOrder
    },
    async putProduct(newPackage) {
      const gqlClient = await makeGQClient(self.token)
      let lastInComplitedOrder = null
      try {
        lastInComplitedOrder = await gqlClient.request(P.putProduct, {
          ...newPackage,
          name:
            newPackage.name || newPackage.places || newPackage.type || 'name',
        })
      } catch (e) {
        console.error(e)
        openErrorNotifi('Ошибка', 'Не удалось добавить продукт')
      }
      return lastInComplitedOrder
    },
    async updateProduct(newPackage) {
      const gqlClient = await makeGQClient(self.token)
      let updatedProduct = null
      try {
        updatedProduct = await gqlClient.request(P.updateProduct, newPackage)
      } catch (e) {
        console.error(e)
        openErrorNotifi('Ошибка', 'Не удалось обновить продукт')
      }
      return updatedProduct
    },
    async makeUserDocs(userId, dtStart, dtFinish) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.makeUserDocs, {
          userId: +userId,
          dtStart: dateTime.parse(dtStart, '-', 'YMD'),
          dtFinish: dateTime.parse(dtFinish, '-', 'YMD'),
        })
      } catch (e) {
        openErrorNotifi('Ошибка', 'Не удалось обновить продукт')
      }
      return answer
    },
    async makeAllDocs(dtStart, dtFinish, selectedUser) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.makeAllDocs, {
          dtStart: dateTime.parse(dtStart, '-', 'YMD'),
          dtFinish: dateTime.parse(dtFinish, '-', 'YMD'),
          userId: +selectedUser,
        })
      } catch (e) {
        openErrorNotifi('Ошибка', 'Не удалось обновить продукт')
      }
      return answer
    },
    async oneCpush(id) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.oneCPush, {
          id,
        })
        openNotifi('Успех', 'Запись в 1 с отправлена', 'success')
      } catch (e) {
        console.error(e)
        openErrorNotifi('Ошибка', 'Не удалось отправить продукт')
      }
      return answer
    },
    async deleteProduct(productId) {
      const gqlClient = await makeGQClient(self.token)
      let deletedProduct = null
      try {
        deletedProduct = await gqlClient.request(P.deleteProduct, { productId })
      } catch (e) {
        const err = JSON.parse(JSON.stringify(e))?.response?.errors
        if (err) {
          openErrorNotifi(
            'Ошибка',
            `Не удалось удалить продукт${': ' + err[0]?.message}`,
          )
        } else {
          openErrorNotifi('Ошибка', `Не удалось удалить продукт`)
        }
      }
      return deletedProduct
    },
    async deleteOrder(orderId) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient
          .request(P.deleteOrder, { orderId })
          .then(() => {
            openNotifi('Успех', 'Заказ успешно удален', 'success')
          })
      } catch (e) {
        const err = JSON.parse(JSON.stringify(e))?.response?.errors
        if (err) {
          openErrorNotifi(
            'Ошибка',
            `Не удалось удалить заказ${': ' + err[0]?.message}`,
          )
        } else {
          openErrorNotifi('Ошибка', `Не удалось удалить заказ`)
        }
      }
      return answer
    },
    async getPricesOrders() {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.pricesOrders)
      } catch (e) {
        console.error(e)
      }
      return answer
    },
    async updateOrder(order, orderStatusId = null) {
      console.log(orderStatusId)
      const gqlClient = await makeGQClient(self.token)
      let updatedOrder = null
      try {
        if (orderStatusId) {
          updatedOrder = await gqlClient.request(P.updateOrder, {
            ...order,
            senderAddress: order?.senderAddress?.value || order?.senderAddress,
            receiverAddress:
              order?.receiverAddress?.value || order?.receiverAddress,
            senderTown: order?.senderAddress?.value || order?.senderAddress,
            receiverTown:
              order?.receiverAddress?.value || order?.receiverAddress,
            orderStatusId: orderStatusId,
            paymentTypeId: +order.paymentTypeId,
          })
          const newStatus = listOrderStatuses.find(
            (el) => el.id === orderStatusId,
          ).name
          order?.sendageEmail &&
            (await gqlClient.request(P.sendMessage, {
              emails: order?.sendageEmail,
              title: 'Order by TempKurier has updated',
              message: `
                Ваш заказ получил новый статус: ${newStatus}
                Номер: ${order.id}
                Ссылка: http://tempkurier.ru/order/${order.id}
                Адрес поддержки - tempkurier@yandex.ru
              `,
            }))
          await gqlClient.request(P.sendMessage, {
            emails:
              'atctemp@yandex.ru cd5fddffsd@yandex.ru tempkurier@yandex.ru atktemp.spb@gmail.com',
            title: `Order: '${order.id}' by TempKurier`,
            message: `
                Заказ получил новый статус: ${newStatus}
                Номер: ${order.id}
                Ссылка: http://tempkurier.ru/order/${order.id}
                Адрес поддержки - tempkurier@yandex.ru
              `,
          })
        } else {
          updatedOrder = await gqlClient.request(P.updateOrder, {
            ...order,
            senderAddress: order?.senderAddress?.value || order?.senderAddress,
            receiverAddress:
              order?.receiverAddress?.value || order?.receiverAddress,
            senderTown: order?.senderAddress?.value || order?.senderAddress,
            receiverTown:
              order?.receiverAddress?.value || order?.receiverAddress,
            senderId: order?.seder?.id || null,
            paymentTypeId: +order.paymentTypeId,
          })
        }
        if (order?.courierId) {
          await self.setOrderCourier(order?.courierId, order?.id)
        }
      } catch (e) {
        openErrorNotifi('Ошибка', 'Не удалось обновить заказ')
        console.error(e)
      }
      if (updatedOrder) {
        openNotifi('Успех', 'Заказ успешно обновлен', 'success')
      }
      return updatedOrder
    },
    async sendOrder(order) {
      const gqlClient = await makeGQClient(self.token)
      let updatedOrder = null
      const receiverAddress =
        order?.receiverAddress?.value || order?.receiverAddress
      const senderAddress = order?.senderAddress?.value || order?.senderAddress

      try {
        updatedOrder = await gqlClient.request(P.updateOrder, {
          ...order,
          senderAddress: senderAddress,
          receiverAddress: receiverAddress,
          senderTown: senderAddress,
          receiverTown: receiverAddress,
          orderStatusId: 1,
          sendageEmail: order?.sEmail || '',
          paymentTypeId: +order.paymentTypeId,
        })
        await gqlClient.request(P.sendMessage, {
          emails:
            'atctemp@yandex.ru cd5fddffsd@yandex.ru tempkurier@yandex.ru atktemp.spb@gmail.com',
          title: 'New order by TempKurier',
          message: `
              Поступил запрос на доставку
              Номер: ${order.id}
              Ссылка: http://tempkurier.ru/order/${order.id}
              Адрес поддержки - tempkurier@yandex.ru
            `,
        })
        if (order?.sEmail) {
          await gqlClient.request(P.sendMessage, {
            emails: order?.sEmail,
            title: 'Order by TempKurier',
            message: `
                Здравствуйте!
                Заказ номер ${order.id} принят на обработку, пожалуйста, дождитесь пока наш логист свяжется с вами и уточнит цену и время доставки.
                Отслеживать статус заказа можно по ссылке http://tempkurier.ru/order/${order.id}
                Адрес поддержки - tempkurier@yandex.ru
              `,
          })
        }
      } catch (e) {
        openErrorNotifi('Ошибка', 'Не удалось отправить продукт')
        console.error(e)
      }
      return updatedOrder
    },
    async getCouriers() {},
    async setOrderCourier(courierId, orderId) {
      const gqlClient = await makeGQClient(self.token)
      let setOrderCourier = null
      try {
        setOrderCourier = await gqlClient.request(P.setOrderCourier, {
          courierId: +courierId,
          orderId: +orderId,
        })
      } catch (error) {
        console.log(error)
      }
      return setOrderCourier
    },
    async changeOrderStatus(orderId, orderStatusId) {
      const gqlClient = await makeGQClient(self.token)
      let updatedOrder = null
      try {
        updatedOrder = await gqlClient.request(P.updateOrder, {
          id: orderId,
          orderStatusId: orderStatusId,
        })
      } catch (e) {
        openErrorNotifi('Ошибка', 'Не удалось обновить заказ')
        console.error(e)
      }
      if (updatedOrder) {
        openNotifi('Успех', 'Заказ успешно обновлен', 'success')
      }
      return updatedOrder
    },
    updateAddress(address, type) {
      self[type] = address
    },
    setEditablePackage(id) {
      self.editablePackageId = id
    },
    updateWorkMod(newMod) {
      self.workMod = newMod
    },
    async listPaymentTypes() {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.listPaymentTypes)
      } catch (e) {
        openErrorNotifi('Ошибка', 'listPaymentTypes')
        console.error(e)
      }
      return answer
    },
    async getListOrders(page, size) {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(P.listOrders, {
        page,
        size,
      })
      return lastInComplitedOrder
    },
    async getListOrdersFull() {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(P.listOrdersFull)
      return lastInComplitedOrder
    },
    async getListOrdersByDate(startDt, endDt, page, size = 100) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.listOrdersByDate, {
          startDt,
          endDt,
          page,
          size,
        })
      } catch (error) {
        const err = pipe(stringify(undefined, 2), parse)(error)
        openErrorNotifi(error)
      }
      return answer
    },
    async getListOrdersByDateUser(startDt, endDt, selectedUser) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.listOrdersByDateUser, {
          startDt,
          endDt,
          userId: parseInt(selectedUser),
        })
      } catch (error) {
        const err = pipe(stringify(undefined, 2), parse)(error)
        openErrorNotifi(error)
      }
      return answer
    },
    async getListOrdersCourier(page, size, courierId) {
      const gqlClient = await makeGQClient(self.token)
      const lastInComplitedOrder = await gqlClient.request(
        P.listOrdersCourier,
        {
          page,
          size,
          courierId: +courierId,
        },
      )
      return lastInComplitedOrder
    },
    async getListOrdersSender(page, size, senderId) {
      const gqlClient = await makeGQClient(self.token)
      const listOrdersSender = await gqlClient.request(P.listOrdersSender, {
        page,
        size,
        senderId: +senderId,
      })
      return listOrdersSender
    },
    async relogSendOrder(id) {
      const gqlClient = await makeGQClient(self.token)
      let listOrdersSender = null
      openNotifi('Успех', 'Заказ успешно добавлен в Relog', 'success')
      try {
        listOrdersSender = await gqlClient.request(P.relogSendOrder, {
          id,
        })
      } catch (error) {
        const err = pipe(stringify(undefined, 2), parse)(error)
        openNotifi(
          'Ошибка сервиса Relog',
          err.response.errors[0].message,
          'error',
        )
      }
      return listOrdersSender
    },
    async relogSendClient(id) {
      const gqlClient = await makeGQClient(self.token)
      let relogSendClient = null
      try {
        relogSendClient = await gqlClient.request(P.relogSendClient, {
          id,
        })
        openNotifi('Успех', 'Пользователь успешно добавлен в Relog', 'success')
      } catch (error) {
        const err = pipe(stringify(undefined, 2), parse)(error)
        openNotifi(
          'Ошибка сервиса Relog',
          err.response.errors[0].message,
          'error',
        )
      }
      return relogSendClient
    },
    async relogSendCourier(id, password) {
      const gqlClient = await makeGQClient(self.token)
      let relogSendCourier = null
      try {
        relogSendCourier = await gqlClient.request(P.relogSendCourier, {
          id,
          password,
        })
        openNotifi('Успех', 'Курьер успешно добавлен в Relog', 'success')
      } catch (error) {
        const err = pipe(stringify(undefined, 2), parse)(error)
        openNotifi(
          'Ошибка сервиса Relog',
          err.response.errors[0].message,
          'error',
        )
      }
      return relogSendCourier
    },
  }))

export default CreatingOrderStore
