import { types as T } from 'mobx-state-tree'
import { ThemeStore, ThemeDefaults, AccountStore, LayoutStore, SidebarStore, CreatingOrderStore } from '../index'

const Store = T.model({
  theme: T.optional(ThemeStore, ThemeDefaults),
  locale: T.optional(T.enumeration('locale', ['ru', 'en']), 'ru'),
  errorCode: T.maybeNull(T.number),
  account: T.optional(AccountStore, {}),
  layout: T.optional(LayoutStore, {}),
  sidebar: T.optional(SidebarStore, {}),
  creatingOrder: T.optional(CreatingOrderStore, {})
})
  .views((self) => ({
    get isOnline() {
      return self.globalLayout.online
    },
    get media() {
      return self.globalLayout.media
    },
  }))
  .actions((self) => ({
    afterCreate() {

    },
    changeTheme(name) {
      self.theme.changeTheme(name)
    }
  }))

export default Store;
