import { types as T, getParent } from 'mobx-state-tree'
import { User, EmptyUser } from '../Models/User'
import {
  CacheStore,
  makeGQClient,
  parse,
  stringify,
  openNotifi,
  getArrayOfValuesByKey,
  reformate,
} from '@/utils/index'
import { P } from '@/schemas/index'
import { pipe, path, cond, T as TRUE } from 'ramda'
import { AOO } from '@dev-bi/rct_utils'
import { equals } from 'ramda'

// eslint-disable-next-line import/prefer-default-export
const AccountStore = T.model('AccountStore', {
  user: T.optional(User, {}),
  isValidSession: T.optional(T.boolean, false),
})
  .views((self) => ({
    get root() {
      return getParent(self)
    },
    get isLogin() {
      return self.isValidSession
    },
    get myUser() {
      return self.user
    },
    get self() {
      return self
    },
    get token() {
      return CacheStore.get('token')
    },
  }))
  .actions((self) => ({
    /* eslint-disable */
    async setUserSettings(userData, profileData, isOrganization) {
      const gqlClient = await makeGQClient(self.token)
      let setProfileData
      const setUserData = await gqlClient.request(P.updateUser, {
        ...userData,
        id: +userData.id,
      })
      if (isOrganization && profileData) {
        setProfileData = await gqlClient.request(P.updateOrgProfile, {
          ...profileData,
          userId: +userData.id,
        })
        if (setProfileData) {
          openNotifi(
            'Успех',
            'Данные пользователя успешно обновлены',
            'success',
          )
        }
      } else {
        const updatedUserProfile = await gqlClient.request(
          P.updateUserProfile,
          { ...profileData },
        )
        if (updatedUserProfile) {
          openNotifi(
            'Успех',
            'Данные пользователя успешно обновлены',
            'success',
          )
        }
      }
    },
    async getUser(id) {
      const gqlClient = await makeGQClient(self.token)
      let getUser
      try {
        getUser = await gqlClient.request(P.user, { id: id }, (onerror) =>
          console.log(),
        )
      } catch (e) {
        console.log()
      }
      return getUser
    },
    async getUniqueUsers(idx) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.getUniqueUsers, { userIds: idx })
      } catch (e) {
        console.log()
      }
      return answer
    },
    async login(personals) {
      const gqlClient = await makeGQClient()
      let signin = null
      const { phone, password } = personals
      const tokenPipe = pipe(stringify(undefined, 2), parse)
      try {
        const signin = await gqlClient.request(P.mSignIn, personals)
        const signData = tokenPipe(signin).signin
        self.setSession(signData.user, signData.token)
      } catch (err) {
        signin = pipe(stringify(2, undefined), parse)(err)
      }
      return path(['response', 'errors'], signin)
    },
    async register(
      phone,
      password,
      passwordConfirmation,
      isOrganization,
      roles = [2],
    ) {
      if (password !== '') {
        if (password !== passwordConfirmation) {
          openNotifi('Ошибка', 'Пароли не совпадают', 'error')
        }
      } else {
        openNotifi('Ошибка', 'Вы не указали пароль', 'error')
      }
      const gqlClient = await makeGQClient(self.token)
      let signup = null
      try {
        signup = await gqlClient
          .request(P.adminSignup, {
            phone,
            password,
            passwordConfirmation,
            isOrganization,
            roles,
          })
          .then((el) => {
            if (el?.adminSignup?.token) {
              openNotifi(
                'Успех',
                'Пользователь успешно зарегестрирован',
                'success',
              )
            } else {
              openNotifi(
                'Ошибка',
                'Не удалось зарегестрировать пользователя, свяжитесь с поддержкой',
                'error',
              )
            }
          })
      } catch (error) {
        console.error(error)
      }
      return signup
    },
    async signup(phone) {
      let answer = false
      let signup = false
      const gqlClient = await makeGQClient()
      cond([
        [
          TRUE,
          () => {
            localStorage.setItem('tryReg', '1')
            localStorage.setItem('sphone', phone)
            localStorage.setItem('sdate', new Date())
            answer = true
          },
        ],
      ])(1)
      if (answer) {
        try {
          signup = await gqlClient.request(P.signUp, {
            phone: `${phone}`,
          })
          openNotifi('Успех', 'Сообщение успешно отправлено', 'success')
        } catch (error) {
          try {
            openNotifi(
              'Ошибка',
              `Ошибка при отправке сообщения: ${
                JSON.parse(JSON.stringify(error)).response.errors[0].message
              }. Попробуйте через 24 часа`,
              'error',
            )
          } catch (_error) {
            openNotifi('Ошибка', 'Ошибка при отправке сообщения', 'error')
          }
        }
      }
      return signup
    },
    async submitSignup(
      phone,
      password,
      passwordConfirmation,
      isOrganization,
      code = '',
      roles = [2],
    ) {
      cond([
        [
          () => {
            return password === ''
          },
          () => {
            openNotifi('Ошибка', 'Вы не указали пароль', 'error')
          },
        ],
        [
          () => {
            return password !== passwordConfirmation
          },
          () => {
            openNotifi('Ошибка', 'Пароли не совпадают', 'error')
          },
        ],
        [
          () => {
            return code === ''
          },
          () => {
            openNotifi('Ошибка', 'Вы не указали код', 'error')
          },
        ],
        [TRUE, () => {}],
      ])(+phone)
      const gqlClient = await makeGQClient()
      let signup = null
      try {
        signup = await gqlClient.request(P.submitSignup, {
          phone,
          password,
          passwordConfirmation,
          isOrganization,
          roles,
          code: +code,
        })
        openNotifi('Успех', 'Пользователь успешно зарегестрирован', 'success')
      } catch (error) {
        const dataPipe = pipe(stringify(undefined, 2), parse)(error)
        console.error(error)
        openNotifi('Ошибка', dataPipe.response.errors[0].message, 'error')
      }
      return signup
    },
    isValidProfileType(profileType) {
      if (profileType !== 'OrgProfile' && profileType !== 'UserProfile') {
        const errorMessage =
          "error by AccountStore, ivalid profileType. Select 'UserProfile' or 'OrgProfile'"
        console.error(errorMessage)
        return false
      }
    },
    async getCouriers() {
      const gqlClient = await makeGQClient(self.token)
      let couriers = {
        couriers: {
          entries: [],
        },
      }
      try {
        couriers = await gqlClient.request(P.getCouriersList)
      } catch (error) {
        console.log(error)
      }
      return couriers
    },
    async getProfileDataByType(profileType) {
      if (self.isValidProfileType(profileType)) return null
      const getFunction = `get${profileType}`
      checkProfile = await gqlClient.request(P[getFunction], userId)
      const dataPipe = pipe(stringify(undefined, 2), parse)
      const checkCreated = !!dataPipe(checkProfile)[getFunction]
    },
    isAdmin() {
      const roles = path(['user', 'roles'], self)
      const rolesId = getArrayOfValuesByKey(roles, 'id')
      return rolesId && rolesId.includes(3)
    },
    isSuper() {
      const roles = path(['user', 'roles'], self)
      const rolesId = getArrayOfValuesByKey(roles, 'id')
      return rolesId && rolesId.includes(1)
    },
    isCourier() {
      let roles
      let rolesId
      try {
        roles = path(['user', 'roles'], self)
        rolesId = getArrayOfValuesByKey(roles, 'id')
      } catch (err) {
        console.log()
      }
      return (
        rolesId &&
        rolesId.includes(5) &&
        !rolesId.includes(1) &&
        !rolesId.includes(3)
      )
    },
    isLogistician() {
      let roles
      let rolesId
      try {
        roles = path(['user', 'roles'], self)
        rolesId = getArrayOfValuesByKey(roles, 'id')
      } catch (err) {
        console.log()
      }
      return rolesId && rolesId.includes(4)
    },
    isClient() {
      const roles = path(['user', 'roles'], self)
      const rolesId = getArrayOfValuesByKey(roles, 'id')
      return rolesId?.includes(2)
    },
    isSuperLgst() {
      let answer = false
      try {
        answer = self.isAdmin() || self.isSuper() || self.isLogistician()
      } catch (err) {
        console.log()
      }
      if (self.isAdmin() || self.isSuper() || self.isLogistician()) return true
      return false
    },
    async me() {
      const gqlClient = await makeGQClient(self.token)
      let answer = null
      try {
        answer = await gqlClient.request(P.me)
      } catch (e) {
        console.error(e)
        openErrorNotifi('Ошибка', 'Не удалось получить пользователя')
      }
      return answer
    },
    async getStatisticData(reportType, userId, year, mth) {
      const gqlClient = await makeGQClient(self.token)
      let answer = null

      cond([
        [
          equals('month'),
          () => {
            try {
              mth = mth + 1
              answer = gqlClient.request(P.getMthStatistic, {
                userId,
                year,
                mth,
              })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
        [
          equals('prev_month'),
          () => {
            try {
              answer = gqlClient.request(P.getMthStatistic, {
                userId,
                year,
                mth,
              })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
        [
          equals('year'),
          () => {
            try {
              answer = gqlClient.request(P.getYearStatistic, { userId, year })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
        [
          equals('prev_year'),
          () => {
            try {
              const y = year - 1
              answer = gqlClient.request(P.getYearStatistic, { userId, y })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
        [
          equals('all_time'),
          () => {
            try {
              answer = gqlClient.request(P.getAllTimeStatistic, {
                userId,
              })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
        [
          TRUE,
          () => {
            try {
              answer = gqlClient.request(P.getMthStatistic, {
                userId,
                year,
                mth,
              })
            } catch (error) {
              openErrorNotifi('Ошибка', `Не удалось получить данные статистики`)
            }
          },
        ],
      ])(reportType)

      return answer
    },
    logout() {
      self.sessionCleanup()
    },
    updateSesstion({ isValid, user }) {
      self.isValidSession = isValid
      if (isValid) {
        self.setSession(user, CacheStore.get('token'))
        return self.updateAccount(user || {})
      }
      return self.sessionCleanup()
    },
    setSession(user, token) {
      const nestUserData = {
        phone: user.phone,
        id: parseInt(user.id),
      }
      self.user = nestUserData
      CacheStore.set('user', user)
      CacheStore.set('token', token)
      CacheStore.cookie.set('jwtToken', token)
    },
    sessionCleanup() {
      self.user = EmptyUser
      self.isValidSession = false
      CacheStore.remove('user')
      CacheStore.remove('token')
      CacheStore.cookie.remove('jwtToken')
    },
    async getUsersList() {
      const gqlClient = await makeGQClient(self.token)
      const usersList = await gqlClient.request(P.getUsersList)
      return JSON.parse(JSON.stringify(usersList))
    },
  }))

export default AccountStore
