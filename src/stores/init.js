import { useMemo } from 'react'
import { applySnapshot } from 'mobx-state-tree'

import Store from './Store'

let clientsideStore;

const initStore = (snapshot = null) => {
  const store = Store.create(snapshot, {})

  if (snapshot) {
    applySnapshot(store, snapshot)
  }

  if (typeof window === 'undefined') return store

  if (!clientsideStore) clientsideStore = store

  return clientsideStore
}

// eslint-disable-next-line import/prefer-default-export
export const useStore = (initialState) => useMemo(() => initStore(initialState), [initialState])
