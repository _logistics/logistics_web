/* eslint-disable import/prefer-default-export */

// const mutationExample = {
//   funcName: 'signin',
//   queries: 'query',
//   funcArguments: [
//     {
//       titles: ['area', 'notes'],
//       type: 'String'
//     },
//     {
//       titles: ['area', 'notes'],
//       type: 'String'
//     }
//   ]
// }

const argumentsParser = (funcArguments, isHead = true) => {
  if (isHead) {
    return funcArguments.reduce((hoarderByType, { type, titles }) => (
      hoarderByType +
      titles.reduce((hoader, el) => `${hoader}$${el}: ${type}, `, '')
    ), '')
  }
  return funcArguments.reduce((hoarderByType, { titles }) => (
    hoarderByType +
    titles.reduce((hoader, el) => `${hoader}${el}: $${el}, `, '')
  ), '')
}

export const mutationCreator = ({ funcName, queries, funcArguments }) => {
  if (funcName && queries && funcArguments) {
    return `
    mutation (${argumentsParser(funcArguments, true)}) {
      ${funcName} (${argumentsParser(funcArguments, false)}) {
        ${queries}
      }
    }
  `
  }
  return ''
}
