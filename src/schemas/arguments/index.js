/* eslint-disable import/prefer-default-export */
import { commonOrgUsData, addresProfileData, typesOfCommunication, userNames, ownUserData, orgData, userId, id } from './profile'

import { order, orderId, product, productId } from './order'

export const A = {
  order,
  typesOfCommunication,
  userNames,
  ownUserData,
  orgData,
  userId,
  addresProfileData,
  commonOrgUsData,
  id,
  orderId,
  product,
  productId
}
