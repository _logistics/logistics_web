/* eslint-disable import/prefer-default-export */
export const order = [
  {
    type: 'Boolean',
    titles: ['isDelivered', 'isOrganization']
  },
  {
    type: 'String',
    titles: [
      'receiverAddress',
      'receiverArea',
      'receiverComment',
      'receiverFio',
      'receiverPhone',
      'receiverState',
      'receiverTown',
      'senderAddress',
      'senderArea',
      'senderComment',
      'senderFio',
      'senderPhone',
      'senderState',
      'senderTown',
      'trackLink',
      'senderFloat',
      'senderApartment',
      'receiverFloat',
      'receiverApartment',
      'sendageEmail',
      'payer',
      'oddMoney',
      'insurance'
    ]
  },
  {
    type: 'NaiveDateTime',
    titles: ['deletedAt', 'deliveryProofDatetime', 'deliveryAwaitedDatetime']
  },
  {
    type: 'Int',
    titles: ['courierId', 'orderStatusId', 'orderTarifId', 'paymentTypeId', 'senderId', 'updatedBy', 'price']
  },
]

export const orderId = {
  type: 'Int!',
  titles: ['orderId']
}

export const productId = {
  type: 'Int!',
  titles: ['productId']
}

export const product = [
  {
    type: 'String',
    titles: ['places']
  },
  {
    type: 'Float',
    titles: ['gHeight', 'gLength', 'volume', 'weight', 'gWidth']
  },
  {
    type: 'Int',
    titles: ['count', 'price']
  },
  {
    type: 'String',
    titles: ['name']
  },
]

export const updateOrderStatus = [
  {
    type: 'String',
    titles: ['places']
  }
]
