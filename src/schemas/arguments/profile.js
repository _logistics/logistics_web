export const ownUserData = [
  {
    type: 'Boolean',
    titles: ['isOrganization']
  },
  {
    type: 'String',
    titles: ['oldPassword', 'password', 'passwordConfirmation', 'phone', 'notes']
  },
]

export const typesOfCommunication = [
  {
    type: 'String',
    titles: ['vk', 'whatsapp', 'telegram', 'mail']
  }
]

export const addresProfileData = [
  {
    type: 'String',
    titles: ['apartment', 'area', 'floor', 'houseNumber', 'street', 'town']
  }
]

export const userNames = [
  {
    type: 'String',
    titles: ['firstName', 'lastName', 'middleName']
  }
]

export const orgData = [
  {
    type: 'String',
    titles: ['bik', 'inn', 'orgName', 'rs', 'contactPersonFio', 'comments']
  }
]

export const commonOrgUsData = [
  {
    type: 'String',
    titles: ['comments', 'preferedChanel']
  }
]

export const userId = {
  type: 'Int!',
  titles: ['userId']
}

export const id = {
  type: 'Int!',
  titles: ['id']
}
