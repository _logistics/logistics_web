import { mutationCreator } from '../schemaCreator'
import { Q } from '../queries'
import { A } from '../arguments'
import { assoc } from 'ramda'

const orderInComplitedData = {
  funcName: 'getLastIncompleted',
  queries: `
    ${Q.orderQ}
  `,
  funcArguments: [A.userId],
}

const updateOrderData = {
  funcName: 'updateOrder',
  queries: `
    ${Q.orderQ}
  `,
  funcArguments: [A.id, ...A.order],
}
export const updateOrder = mutationCreator(updateOrderData)
export const createOrder = `
  mutation {
    createOrder {
      id
    }
  }
`
export const getLastInComplitedOrder = mutationCreator(orderInComplitedData)

const orders = `
id
    receiverAddress
    deliveryProofDatetime
    insertedAt
    receiverFio
    insurance
    isDelivered
    senderAddress
    deliveryAwaitedDatetime
    price
    senderComment
    senderFio
    orderStatus {
      id
      name
    }
    courier {
      id
      userProfile {
        firstName
        middleName
        lastName
      }
    }
    products {
      id
      name
      count
      price
      gWidth
      gHeight
      gLength
      places
      volume
      weight
      comment
    }
    sender {
      id
      userProfile{
        firstName
        lastName
        middleName
      }
      orgProfile{
        orgName
        contactPersonFio
      }
    }
    courier {
      id
    }
`

const listQueryes = `
  entries {
    ${orders}
  }
  pageSize
  pageNumber
  totalPages
`

export const listOrders = `
  query($page: Int, $size: Int) {
    listOrders(filter: {
      page: $page,
      size: $size,
      sort: DESC_INSERTED
    }) {
      ${listQueryes}
    }
  }
`

export const getMthStatistic = `
  mutation($userId: Int, $year: Int, $mth: Int){
    listOrdersForGraph(userId: $userId, year: $year, mth: $mth){
    count
    payedSum
    notPayedSum
    notPayedCount
    payedCount
    notDeliveredSum
    notDeliveredCount
    price
    inserted
    orderList{
      id
      price
    	orderStatus
      orderStatusId
    }
  }
  }
`

export const getYearStatistic = `
  mutation($userId: Int, $year: Int){
    listOrdersForGraph(userId: $userId, year: $year){
    count
    price
    inserted
    payedSum
    notPayedSum
    notPayedCount
    payedCount
    notDeliveredSum
    notDeliveredCount
    orderList{
      id
      price
    	orderStatus
      orderStatusId
    }
  }
  }
`

export const getAllTimeStatistic = `
  mutation($userId: Int){
    listOrdersForGraph(userId: $userId){
    count
    price
    inserted
    payedSum
    notPayedSum
    notPayedCount
    payedCount
    notDeliveredSum
    notDeliveredCount
    orderList{
      id
      price
    	orderStatus
      orderStatusId
    }
  }
  }
`

export const listOrdersFull = `
  query {
    listOrders {
      ${listQueryes}
    }
  }
`

export const listOrdersByDate = `
  query($endDt: String, $startDt: String, $page: Int, $size: Int) {
    listOrders(filter: {startDt: $startDt,
       endDt: $endDt, 
       sort: DESC_INSERTED
       page: $page,
       size: $size
      }) {
      ${listQueryes}
    }
  }
`

export const listOrdersByDateUser = `
  query($endDt: String, $startDt: String, $userId: Int) {
    listOrdersNoFilters(startDt: $startDt, endDt: $endDt, userId: $userId) {
      ${orders}
    }
  }
`

export const listPaymentTypes = `
  query {
    listPaymentTypes {
      id
      name
      comment
    }
  }
`

export const pricesOrders = `
  query {
    listOrders (filter: {}) {
      entries {
        price
      }
    }
  }
`

export const listOrdersCourier = `
  query($courierId: Int!,$page: Int, $size: Int) {
    listOrdersCourier(
    courierId: $courierId,
    filter: {
      page: $page,
      size: $size
    }) {
      ${listQueryes}
    }
  }
`

export const getOrder = `
  query($orderId: Int!) {
    getOrder (orderId: $orderId) {
      ${Q.orderQ}
    }
  }
`

export const listOrdersSender = `
  query($senderId: Int!, $page: Int, $size: Int) {
    listOrdersSender(
    senderId: $senderId,
    filter: {
      page: $page,
      size: $size
    }) {
      ${listQueryes}
    }
  }
`

export const productData = {
  queries: `
    ${Q.products}
  `,
  funcArguments: [A.orderId, ...A.product],
}
const updateProductData = {
  funcName: 'updateProduct',
  queries: `
    ${Q.products}
  `,
  funcArguments: [A.productId, ...A.product],
}
const putProductData = assoc('funcName', 'putProduct', productData)
export const putProduct = mutationCreator(putProductData)
export const updateProduct = mutationCreator(updateProductData)
export const deleteProduct = `
  mutation($productId: Int!) {
    deleteProduct (productId: $productId)
    {
      id
    }
  }
`

export const oneCPush = `
  mutation ($id: Int!) {
    confirmOrderAndSend (id: $id) {
      message
      response {
        error
        status
        oneCResponse {
          id
        }
      }
    }
  }
`

export const setOrderCourier = `
  mutation($courierId: Int!, $orderId: Int!) {
    setOrderCourier(courierId: $courierId, orderId: $orderId) {
      ${Q.orderQ}
    }
  }
`

export const deleteOrder = `
  mutation($orderId: Int!) {
    deleteOrder (orderId: $orderId) {
      id
    }
  }
`

export const makeUserDocs = `
  mutation($userId: Int!, $dtStart: String!, $dtFinish: String!) {
    makeUserDocs(userId: $userId, dtStart: $dtStart, dtFinish: $dtFinish){
      pdf
      excel
    }
  }
`

export const makeAllDocs = `
  mutation($dtStart: String!, $dtFinish: String!, $userId: Integer!) {
    makeAllDocs(dtStart: $dtStart, dtFinish: $dtFinish, userId: $userId){
      pdf
      excel
    }
  }
`
