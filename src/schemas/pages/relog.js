export const relogSendCourier = `
  mutation($id: Int!, $password: String! ) {
    relogSendCourier (id: $id, password: $password) 
  }
`;

export const relogSendClient = `
  mutation($id: Int!) {
    relogSendClient (id: $id) 
  }
`;

export const relogSendOrder = `
  mutation($id: Int!) {
    relogSendOrder (id: $id) 
  }
`;
