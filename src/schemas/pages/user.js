import { mutationCreator } from '../schemaCreator'
import { Q } from '../queries'
import { A } from '../arguments'
import { assoc } from 'ramda'

export const user = `
  query($id: Int!) {
    user(id: $id) {
      ${Q.roles}
      ${Q.generalProfileData}
      ${Q.currentProfile}
    }
  }
`

export const me = `
  query {
    me {
      ${Q.generalProfileData}
      ${Q.currentProfile}
      ${Q.roles}
    }
  }
`

const orgProfileData = {
  queries: `
    area
  `,
  funcArguments: [
    A.userId,
    ...A.typesOfCommunication,
    ...A.orgData,
    ...A.addresProfileData,
  ],
}
const updateOrgProfileData = assoc(
  'funcName',
  'updateOrgProfile',
  orgProfileData,
)
export const updateOrgProfile = mutationCreator(updateOrgProfileData)
export const getOrgProfile = `
  query {
    getOrgProfile(userId: 1) {
      area
    }
  }
`

const userProfileData = {
  funcName: 'updateUserProfile',
  queries: `
    area
    comments
    firstName
    lastName
    lockedAt
    mail
    middleName
    preferedChanel
    state
    telegram
    town
    vk
    whatsapp
  `,
  funcArguments: [
    A.userId,
    ...A.addresProfileData,
    ...A.typesOfCommunication,
    ...A.userNames,
  ],
}
export const updateUserProfile = mutationCreator(userProfileData)
export const getUserProfile = `
  query($userId: Int!) {
    getUserProfile(userId: $userId) {
      area
      comments
      firstName
      lastName
      lockedAt
      mail
      middleName
      preferedChanel
      state
      telegram
      town
      vk
      whatsapp
    }
  }
`

const userData = {
  funcName: 'updateUser',
  queries: `
    phone
    comments
  `,
  funcArguments: [A.id, ...A.ownUserData],
}
export const updateUser = mutationCreator(userData)

const mSignInData = {
  funcName: 'signin',
  queries: `
    token,
    user {
      ${Q.user}
    }
  `,
  funcArguments: [
    {
      type: 'String!',
      titles: ['phone', 'password'],
    },
  ],
}
export const mSignIn = mutationCreator(mSignInData)

const adminSignupD = {
  funcName: 'adminSignup',
  queries: `
    token,
    user {
      ${Q.user}
    }
  `,
  funcArguments: [
    {
      type: 'String!',
      titles: ['passwordConfirmation', 'password', 'phone'],
    },
    {
      type: 'Boolean',
      titles: ['isOrganization'],
    },
    {
      type: '[Int]',
      titles: ['roles'],
    },
  ],
}
export const adminSignup = mutationCreator(adminSignupD)

const submitSignupData = {
  funcName: 'submitSignup',
  queries: `
    token,
    user {
      ${Q.user}
    }
  `,
  funcArguments: [
    {
      type: 'String!',
      titles: ['passwordConfirmation', 'password', 'phone'],
    },
    {
      type: 'Boolean',
      titles: ['isOrganization'],
    },
    {
      type: '[Int]',
      titles: ['roles'],
    },
    {
      type: 'Int',
      titles: ['code'],
    },
  ],
}
export const submitSignup = mutationCreator(submitSignupData)

export const signUp = `
  mutation($phone: String!) {
    signup (phone: $phone)
  }
`

export const getUsersList = `
  query{
    users(filter: {}) {
      entries {
        id
        isOrganization
        phone
        roles {
          id
          slug
        }
        profile {
          rs
          bik
          inn
          firstName
          middleName
          lastName
        }
      }
    }
  }
`

export const getCouriersList = `
  query {
    couriers(filter: {}) {
      entries {
        id
        profile {
          firstName
          middleName
        }
      }
    }
  }
`

export const getNewUsers = `
  query($dtStart: String!, $dtFinish: String!) {
    newUsers(dtStart: $dtStart, dtFinish: $dtFinish, ) {
      count
      message
    }
  }
`

const staticFuncTemplate = (funcName, type = 'mutation') => {
  return `
  ${type}($dtStart: String!, $dtFinish: String!, $divided: Divided!, $userId: Int) {
    ${funcName}(dtStart: $dtStart, dtFinish: $dtFinish, divided: $divided, userId: $userId) {
      count {
        dt
        type
        count
      }
      message
    }
  }
`
}

export const countOrderSumm = staticFuncTemplate('countOrderSumm')
export const countTransferedOrders = staticFuncTemplate('countTransferedOrders')
export const countOrders = staticFuncTemplate('countOrders', 'query')
export const newUsers = `
query($dtStart: String!, $dtFinish: String!, $divided: Divided!) {
  newUsers(dtStart: $dtStart, dtFinish: $dtFinish, divided: $divided) {
    count {
      dt
      type
      count
    }
    message
  }
}
`

export const sendMessage = `
  mutation($emails: String!, $title: String!, $message: String!) {
    sendMessage (emails: $emails, title: $title, message: $message) 
  }
`

export const getUniqueUsers = `
mutation($userIds: [Int]!) {
  getUniqueUsers(userIds: $userIds) {
    id
    phone
    isOrganization
    profile {
      firstName
      middleName
      lastName
      
    }
  }
}
`
