/* eslint-disable import/prefer-default-export */

export const products = `
  comment
  count
  gHeight
  gLength
  gWidth
  id
  name
  order {
    id
  }
  places
  price
  volume
  weight
`;

export const orderQ = `
  senderAddress
  senderState
  price
  sendageEmail
  deliveryAwaitedDatetime
  oddMoney
  payer
  paymentType {
    name
  }
  sender {
    id
  } 
  senderTown
  acceptanceDatetime
  senderFio
  insurance
  insertedAt
  orderTarif {
    name
  }
  canBeSent
  id
  trackLink
  receiverAddress
  receiverState
  isValid
  courier {
    id
    phone
    userProfile {
      firstName
      middleName
      lastName
    }
  }
  receiverTown
  isOrganization
  receiverArea
  receiverFio
  receiverFloat
  receiverApartment
  senderFloat
  senderApartment
  deliveryAwaitedDatetime
  products {
    ${products}
  }
  isAccepted
  deletedAt
  senderComment
  orderStatus {
    name
    id
  }
  senderPhone
  receiverComment
  senderArea
  deliveryProofDatetime
  updatedBy {
    id
  }
  isDelivered
  receiverPhone
  updatedAt
`;
