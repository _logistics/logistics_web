/* eslint-disable import/prefer-default-export */
import {
  user,
  role
} from './base'

import {
  generalProfileData,
  userProfileData,
  orgProfileData,
  currentProfile,
  roles
} from './profile'

import {
  orderQ,
  products
} from './order'

export const Q = {
  user,
  role,
  orderQ,
  generalProfileData,
  userProfileData,
  orgProfileData,
  currentProfile,
  roles,
  products
}
