export const generalProfileData = `
  id
  isOrganization
  comments
  phone
`;

export const roles = `
  roles {
    id
    slug
  }
`;

export const userProfileData = `
  middleName
  comments
  firstName
  lastName
`;

export const orgProfileData = `
  bik
  contactPersonFio
  inn
  orgName
  rs
`;

export const currentProfile = `
  profile {
    apartment
    area
    bik
    comments
    contactPersonFio
    firstName
    floor
    houseNumber
    inn
    lastName
    lockedAt
    mail
    middleName
    orgName
    preferedChanel
    rs
    state
    street
    telegram
    town
    vk
    whatsapp
  }
`;
