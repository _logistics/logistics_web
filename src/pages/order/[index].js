import React from 'react'
import { AuthPageWrapper } from '@/containers/AuthWrapper'
import { merge } from 'ramda'
import { getJwtToken, makeGQClient } from '@/utils/index'
import { P } from '../../schemas'
import { useStore } from '@/stores/init'
import { Provider } from 'mobx-react'
import LayoutWrapper from '@/containers/LayoutWrapper'
import PageHeader from '@/components/3_organisms/PageHeader'
import EditOrderCont from '@/components/5_pages/EditOrderCont'

const fetchData = async (props, attrs) => {
  const { phone } = merge({ phone: true }, attrs)
  const token = phone ? getJwtToken(props) : null
  const gqlClient = await makeGQClient(token)

  const me = gqlClient.request(P.me)

  return {
    ...(await me),
  }
}

export const getServerSideProps = async (props) => {
  let resp
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e),
      },
    }
  }

  const { me } = resp
  const initProps = {
    account: {
      user: me || {},
    },
    theme: {
      currTheme: 'main',
    },
  }

  return {
    props: {
      errorCode: null,
      ...initProps,
    },
  }
}

const EditOrder = (props) => {
  const store = useStore(props)
  const { account } = useStore({})
  const { errorCode } = store
  const isAuthenticated = !!account.user.login
  return (
    <Provider store={store}>
      <AuthPageWrapper isAuthenticated={isAuthenticated}>
        <LayoutWrapper
          page="/order/edit"
          errorCode={errorCode}
          errorPath="/order/edit"
        >
          <PageHeader>
            <h1>Редактирование заказа</h1>
          </PageHeader>
          <EditOrderCont />
        </LayoutWrapper>
      </AuthPageWrapper>
    </Provider>
  )
}

export default EditOrder
