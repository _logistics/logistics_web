/* eslint-disable prefer-destructuring */
import React from 'react'
import NextErrorComponent from 'next/error'
import { Button } from '@/components/1_atoms/Buttons'
import ThemeWrapper from '@/containers/ThemeWrapper'
import { useStore } from '@/stores/init'
import { Provider } from 'mobx-react'

const Error = (props) => {
  const store = useStore(props)
  return (
    <Provider store={store}>
      <ThemeWrapper>
        <ErrorComponent {...props} />
      </ThemeWrapper>
    </Provider>
  )
}

const ErrorComponent = ({ statusCode }) => {
  return (
    <>
      <p>
        {statusCode
            ? `An error ${statusCode} occurred on server`
            : 'An error occurred on client'}
      </p>
      <Button>Log btn</Button>
      <Button />
      <Button reversed />
    </>
  )
}

Error.getInitialProps = async ({ res, err }) => {
  const initialError = await NextErrorComponent.getInitialProps({
    res, err
  })
  initialError.hasGetInitialPropsRun = true

  if (res?.statusCode === 404) {
    return { statusCode: 404 }
  }
  if (err) {
    return { ...initialError }
  }
}

export default Error
