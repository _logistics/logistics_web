import React from 'react'
import FatalError from '@/components/4_templates/FatalError'
import { appWithTranslation } from '../../i18n'
import '../../public/static/styles/font_awesome.css'
import '../../public/static/styles/dadata.css'
import 'antd/dist/antd.css';
import '../sass/mainDatePicker.sass'
import '../sass/index.sass'


const App = ({ Component, pageProps, err }) => {
  return err ? <FatalError /> : (
    <div>
      <Component {...pageProps} />
    </div>
  )
}

export default appWithTranslation(App);
