import React from 'react'
import { merge, path } from 'ramda'
import { getJwtToken, makeGQClient } from '@/utils/index';
import { P } from '@/schemas/index'
import { useStore } from '@/stores/init';
import { Provider } from 'mobx-react';
import LayoutWrapper from '@/containers/LayoutWrapper';
import { withTranslation } from '@/i18n';
import { AuthWrapper } from '@/containers/AuthWrapper';
import StatisticPage from '@/components/5_pages/StatisticPage';

const fetchData = async (props, attrs) => {
  const { phone } = merge({ phone: true }, attrs)
  const token = phone ? getJwtToken(props) : null
  const gqlClient = await makeGQClient(token)

  const me = gqlClient.request(P.me)

  return {
    ...(await me)
  }
}

export const getServerSideProps = async (props) => {
  let resp;
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e)
      }
    }
  }

  const { me } = resp;
  const initProps = {
    account: {
      user: me || {}
    },
    theme: {
      currTheme: 'main'
    },
  }

  return {
    props: {
      errorCode: null,
      ...initProps
    }
  }
}

const Statistic = (props) => {
  const store = useStore(props);
  const { errorCode } = store;
  const isAuthenticated = path(['account', 'user', 'phone'], store);
  return (
    <Provider store={store}>
      <AuthWrapper isAuthenticated={!!isAuthenticated}>
        <LayoutWrapper
          page="/"
          errorCode={errorCode}
          errorPath="/"
        >
          <StatisticPage />
        </LayoutWrapper>
      </AuthWrapper>
    </Provider>
  )
}

export default withTranslation('common')(Statistic);
