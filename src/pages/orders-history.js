import React from 'react'
import { AuthWrapper } from '@/containers/AuthWrapper'
import { merge } from 'ramda';
import { getJwtToken, makeGQClient } from '@/utils/index';
import { P } from '../schemas';
import { useStore } from '@/stores/init';
import { Provider } from 'mobx-react';
import LayoutWrapper from '@/containers/LayoutWrapper';
import PageHeader from '@/components/3_organisms/PageHeader';
import OrdersHistoryCont from '@/components/5_pages/OrdersHistory';


const fetchData = async (props, attrs) => {
  const { phone } = merge({ phone: true }, attrs)
  const token = phone ? getJwtToken(props) : null
  const gqlClient = await makeGQClient(token)

  const me = gqlClient.request(P.me)

  return {
    ...(await me)
  }
}

export const getServerSideProps = async (props) => {
  let resp;
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e)
      }
    }
  }

  const { me } = resp;
  const initProps = {
    account: {
      user: me || {}
    },
    theme: {
      currTheme: 'main'
    },
  }

  return {
    props: {
      errorCode: null,
      ...initProps
    }
  }
}

const OrdersHistory = (props) => {
  const store = useStore(props);
  const { account } = useStore({});
  const { errorCode } = store;
  const isAuthenticated = !!account.user.phone;
  return (
    <Provider store={store}>
      <AuthWrapper isAuthenticated={isAuthenticated}>
        <LayoutWrapper
          page="/orders-history"
          errorCode={errorCode}
          errorPath="/orders-history"
        >
          <PageHeader>
            История заказов
          </PageHeader>
          <OrdersHistoryCont />
        </LayoutWrapper>
      </AuthWrapper>
    </Provider>
  )
}

export default OrdersHistory
