import React from 'react'
import { merge, path } from 'ramda'
import { getJwtToken, makeGQClient } from '@/utils/index';
import { P } from '@/schemas/index'
import { useStore } from '@/stores/init';
import { Provider } from 'mobx-react';
import LayoutWrapper from '@/containers/LayoutWrapper';
import PageHeader from '@/components/3_organisms/PageHeader';
import { Button } from '@/components/1_atoms/Buttons'
import ThemeSelector from '@/components/3_organisms/ThemeSelector';
import { withTranslation } from '@/i18n';
import { AuthWrapper } from '@/containers/AuthWrapper';

const fetchData = async (props, attrs) => {
  const { login } = merge({ login: true }, attrs)

  const token = login ? getJwtToken(props) : null;
  const gqlClient = await makeGQClient(token)
  // const isToken = nilOrEmpty(token) === false

  const me = gqlClient.request(P.me)

  return {
    ...(await me)
  }
}

export const getServerSideProps = async (props) => {
  let resp;
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e)
      }
    }
  }

  const { me } = resp;
  const initProps = {
    account: {
      user: me || {}
    },
    theme: {
      currTheme: 'main'
    },
  }

  return {
    props: {
      errorCode: null,
      ...initProps
    }
  }
}

const Index = (props) => {
  const store = useStore(props)
  const { t } = props;
  const { errorCode } = store
  const isAuthenticated = path(['account', 'user', 'login'], props);

  return (
    <Provider store={store}>
      <AuthWrapper isAuthenticated={!!isAuthenticated}>
        <LayoutWrapper
          page="/"
          errorCode={errorCode}
          errorPath="/"
        >
          <PageHeader />
          <ThemeSelector
            curTheme={store.theme.currTheme}
            changeTheme={store.theme.changeTheme}
          />
          <Button>Log btn</Button>
          <Button />
          <Button reversed />
          {t('hw')}
        </LayoutWrapper>
      </AuthWrapper>
    </Provider>
  )
}

export default withTranslation('common')(Index)
