import React from 'react'
import { AuthPageWrapper } from '@/containers/AuthWrapper'
import { merge } from 'ramda';
import { getJwtToken, makeGQClient } from '@/utils/index';
import { P } from '../../schemas';
import { useStore } from '@/stores/init';
import { Provider } from 'mobx-react';
import LayoutWrapper from '@/containers/LayoutWrapper';
import AccountCont from '@/components/5_pages/Account';
import { useRouter } from 'next/router'
import { getArrayOfValuesByKey } from 'core_js_devbi'

const fetchData = async (props, attrs) => {
  const { phone } = merge({ phone: true }, attrs)
  const token = phone ? getJwtToken(props) : null
  const gqlClient = await makeGQClient(token)

  const me = gqlClient.request(P.me)

  return {
    ...(await me)
  }
}

export const getServerSideProps = async (props) => {
  let resp;
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e)
      }
    }
  }

  const { me } = resp;
  const rolesId = getArrayOfValuesByKey(me.roles, 'id');
  let errorCode = 404;
  if (rolesId.includes(1) || rolesId.includes(3) || rolesId.includes(4)) {
    errorCode = null
  }
  const initProps = {
    account: {
      user: me || {}
    },
    theme: {
      currTheme: 'main'
    },
  }

  return {
    props: {
      errorCode,
      ...initProps
    }
  }
}

const Account = (props) => {
  const store = useStore(props);

  const { account } = useStore({});
  const { errorCode } = store
  const isAuthenticated = !!account.user.login;
  const router = useRouter();
  return (
    <Provider store={store}>
      <AuthPageWrapper isAuthenticated={isAuthenticated}>
        <LayoutWrapper
          page="/account"
          errorCode={errorCode}
          errorPath="/account"
        >
          <AccountCont userId={router.query.index || -1} />
        </LayoutWrapper>
      </AuthPageWrapper>
    </Provider>
  )
}

export default Account
