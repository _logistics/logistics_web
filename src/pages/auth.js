import React from 'react'
import { AuthPageWrapper } from '@/containers/AuthWrapper'
import { merge } from 'ramda';
import { getJwtToken, makeGQClient } from '@/utils/index';
import { P } from '../schemas';
import { useStore } from '@/stores/init';
import { Provider } from 'mobx-react';
import Authentication from '@/components/5_pages/Authentication';

const fetchData = async (props, attrs) => {
  const { phone } = merge({ phone: true }, attrs)

  const token = phone ? getJwtToken(props) : null;
  const gqlClient = await makeGQClient(token)
  // const isToken = nilOrEmpty(token) === false

  const me = gqlClient.request(P.me)

  return {
    ...(await me)
  }
}

export const getServerSideProps = async (props) => {
  let resp;
  try {
    resp = await fetchData(props)
  } catch (e) {
    return {
      props: {
        errorCode: 404,
        e: JSON.stringify(e)
      }
    }
  }

  const { me } = resp;
  const initProps = merge({
    account: {
      user: me || {}
    },
    theme: {
      currTheme: 'main'
    },
  })

  return {
    props: {
      errorCode: null,
      ...initProps
    }
  }
}

const Auth = (props) => {
  const store = useStore(props);
  const { account } = useStore({})
  const isAuthenticated = !!account.user.login;

  return (
    <Provider store={store}>
      <AuthPageWrapper isAuthenticated={isAuthenticated}>
        <Authentication />
      </AuthPageWrapper>
    </Provider>
  )
}

export default Auth
