import React from 'react'
import { ThemeProvider } from 'styled-components'
import { connectStore } from '@/utils/index'

const ThemeContainer = ({ children, theme: { themeData } }) => {
  return (
    <ThemeProvider theme={themeData}>
      <>
        <div>{children}</div>
      </>
    </ThemeProvider>
  )
}
ThemeContainer.displayName = 'ThemeContainer'
export default connectStore(ThemeContainer)
