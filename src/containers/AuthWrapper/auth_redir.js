import React from 'react'
import T from 'prop-types'
import { FullScreenLoader } from '@/components/2_molecules/Loader'
import RedirectWrapper from '../RedirectWrapper'

const AuthWrapper = ({ children, isAuthenticated }) => {
  return (
    <RedirectWrapper condition={!isAuthenticated} to="/auth">
      <div style={{ height: '100%', width: '100%' }}>
        {children}
      </div>
    </RedirectWrapper>
  )
}

AuthWrapper.propTypes = {
  isAuthenticated: T.bool.isRequired,
  children: T.node
}

AuthWrapper.defaultProps = {
  children: <FullScreenLoader />
}

export default AuthWrapper
