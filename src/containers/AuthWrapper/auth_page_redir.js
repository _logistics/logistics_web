import React from 'react'
import T from 'prop-types'
import ThemeWrapper from '../ThemeWrapper'
import RedirectWrapper from '../RedirectWrapper'

const AuthPageWrapper = ({ children, isAuthenticated }) => {
  return (
    <ThemeWrapper>
      <RedirectWrapper condition={isAuthenticated} to="/">
        {children}
      </RedirectWrapper>
    </ThemeWrapper>
  )
}

AuthPageWrapper.propTypes = {
  isAuthenticated: T.bool.isRequired,
  children: T.node
}

AuthPageWrapper.defaultProps = {
  children: <div />
}

export default AuthPageWrapper
