import React from 'react'
import T from 'prop-types'
import { FullScreenLoader } from '@/components/2_molecules/Loader';
import { useRouter } from 'next/router';

const RedirectWrapper = ({ timeout, condition, to, children }) => {
  const router = useRouter();
  if (condition) {
    setTimeout(() => {
      if (typeof window !== 'undefined') {
        router.push(to)
      }
    }, timeout);
    return <FullScreenLoader />
  }
  return <div style={{ height: '100%' }}>{children}</div>
};

export default RedirectWrapper
