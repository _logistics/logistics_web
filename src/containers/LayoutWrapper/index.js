import React, { useEffect } from 'react'
import T from 'prop-types'
import { connectStore } from '@/utils/index'
import ThemeWrapper from '../ThemeWrapper'
import { Wrapper, InnerWrapper, ContentWrapper, ContentPinWrapper } from './styles'
import ErrorPage from '@/components/5_pages/ErrorPage'
import Sidebar from '@/components/3_organisms/Sidebar/index'
import { Footer, Header } from './dynamic'
import { useInit, calcInitWidth } from './logic'
import { useMedia, useNetwork, usePlatform } from '@/components/0_functionals/hooks/index'
import Main from '@/components/4_templates/Main'

const LayoutContainer = ({
  layout: store,
  page,
  errorCode,
  errorPath,
  children,
  noSidebar
}) => {
  const [innerMinWidth, setInnerMinWidth] = React.useState('100vw')

  const { online } = useNetwork()
  const media = useMedia()
  const platform = usePlatform()

  useInit(store, { online, media, platform })
  const innerWrapperRef = React.createRef()
  const { sidebarPin } = store

  useEffect(() => {
    if (errorCode === null) {
      setInnerMinWidth(calcInitWidth(innerWrapperRef))
    }
  }, [innerWrapperRef, errorCode])

  useEffect(() => {
    if (errorCode === null) {
      setInnerMinWidth(calcInitWidth(innerWrapperRef))
    }
  }, [innerMinWidth, innerWrapperRef, errorCode])

  return (
    <ThemeWrapper>
      <Wrapper>
        {errorCode ?
          <ErrorPage errorCode={errorCode} page={page} target={errorPath} /> :
          <div style={{ width: '100%' }}>
            <InnerWrapper
              sidebarPin={sidebarPin}
              noSidebar={noSidebar}
              minWidth={innerMinWidth}
            >
              {!noSidebar && <Sidebar />}
              <ContentPinWrapper
                offsetLeft={sidebarPin}
              >
                <ContentWrapper offsetLeft>
                  <Header />
                  <Main>
                    {children}
                  </Main>
                  <Footer />
                </ContentWrapper>
              </ContentPinWrapper>
            </InnerWrapper>
          </div>
        }
      </Wrapper>
    </ThemeWrapper>
  )
}

LayoutContainer.propTypes = {
  children: T.node,
  layout: T.object.isRequired,
  noSidebar: T.bool,
  page: T.string.isRequired,
  errorCode: T.oneOf([null, 404, 500]),
  errorPath: T.oneOfType([T.string, T.instanceOf(null)]),
}

LayoutContainer.defaultProps = {
  children: <div />,
  noSidebar: false,
  errorCode: null,
  errorPath: null
}

LayoutContainer.displayName = 'LayoutContainer'

export default connectStore(LayoutContainer)
