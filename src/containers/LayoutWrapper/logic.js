import { useEffect } from 'react'
import { comStyles } from '@/utils/index';

// eslint-disable-next-line no-unused-vars
let store = null;

// eslint-disable-next-line import/prefer-default-export
export const useInit = (_store, extra) => {
  useEffect(() => {
    store = _store
    const { online, media, platform } = extra
    store.mark({ online, media, platform })
  }, [_store, extra])
}
/* eslint-disable */
export const calcInitWidth = () => {
  // const { clientWidth } = ref.current
  const MAX_WIDTH = Number(comStyles.GLOBAL_MAX_WIDTH.slice(0, -2))
  const WINDOW_WIDTH = window.innerWidth

  let minWidth
  if (WINDOW_WIDTH > MAX_WIDTH) minWidth = `${MAX_WIDTH}px`
  if (WINDOW_WIDTH < MAX_WIDTH) minWidth = `${WINDOW_WIDTH}px`

  return minWidth
}
