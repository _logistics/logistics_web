import styled from 'styled-components'
import { comStyles, theme } from '@/utils/index'

export const Wrapper = styled.div`
  ${comStyles.flex('justify-center')};
  background-color: ${theme('spaceBg')};
  background-attachment: fixed;
  width: 100vw;
`;

export const InnerWrapper = styled.div`
  ${comStyles.flexColumn()};
  max-width: ${comStyles.MAX_CONTENT_WIDTH};
  width: 100%;
  min-width: ${({ minWidth }) => minWidth};

  position: relative;
  height: 100%;
  min-height: 100vh;
  background: ${theme('bodyBg')};
  transition: all 0.2s;
  overflow-x: ${({ sidebarPin }) => (sidebarPin ? 'hidden' : '')};
  ${comStyles.media.tablet`
    position: relative;
    padding-left: 0;
  `};
  @media only screen and (max-width: 800px) {
    min-width: 0px;
  }
`;

export const ContentPinWrapper = styled.div`
  margin-left: ${({ offsetLeft }) => (offsetLeft ? '204px' : '0')};
`;

export const ContentWrapper = styled.div`
  margin-left: ${({ offsetLeft }) => (offsetLeft ? theme('sidebar.widthBigBar') : '0')};
  @media (max-width: ${theme('sreenWidth.tablet')}) {
    margin-left: ${({ offsetLeft }) => (offsetLeft ? theme('sidebar.widthSmallBar') : '0')};
  }
  @media (max-width: 800px ) {
    margin-left: 0 !important;
  }
  ${comStyles.media.mobile`
    margin-left: 0;
  `};
`;
