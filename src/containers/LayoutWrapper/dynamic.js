import React from 'react'
import dynamic from 'next/dynamic';

export const ErrorPage = dynamic({
  loader: () => import('@/components/5_pages/ErrorPage'),
  // eslint-disable-next-line react/display-name
  loading: () => <div />,
  ssr: false,
})


export const Footer = dynamic({
  loader: () => import('@/components/4_templates/Footer'),
  // eslint-disable-next-line react/display-name
  loading: () => <div />,
  ssr: false,
})

export const Header = dynamic({
  loader: () => import('@/components/4_templates/Header'),
  // eslint-disable-next-line react/display-name
  loading: () => <div />,
  ssr: false,
})
