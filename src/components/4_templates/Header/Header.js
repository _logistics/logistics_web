/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react'
import { Wrapper, InnerWrapper, LeftPart, RightPart, MobileList } from './styles'
import { useStore } from '@/stores/init';
import { getArrayOfValuesByKey } from 'core_js_devbi'
import Center from '@/components/1_atoms/Center'

const HeaderComponent = () => {
  const { account } = useStore({});
  const [isActiveMenu, setIsActiveMenu] = useState(false);
  const toggleMenu = () => {
    setIsActiveMenu(!isActiveMenu);
  };

  const getRoutes = () => {
    const rolesId = getArrayOfValuesByKey(account.user.roles, 'id');
    if (!rolesId) {
      return []
    }

    const adminRoutes = [
      {
        name: 'Главная',
        link: '/',
        icon: 'fal fa-home'
      },
      {
        name: 'Создание заказа',
        link: '/order/create',
        icon: 'fal fa-cart-plus'
      },
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Пользователи',
        link: '/account/search',
        icon: 'fal fa-users'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    const userRoutes = [
      {
        name: 'Статистика',
        link: '/',
        icon: 'fal fa-cart-plus'
      },
      {
        name: 'Создание заказа',
        link: '/order/create',
        icon: 'fal fa-cart-plus'
      },
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    const curierRoutes = [
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    switch (true) {
      case rolesId.includes(1):
        return adminRoutes;
      case rolesId.includes(3):
        return adminRoutes
      case rolesId.includes(4):
        return adminRoutes;
      case rolesId.includes(5):
        return curierRoutes
      case rolesId.includes(2):
        return userRoutes;
      default:
        return [];
    }
  }
  return (
    <Wrapper testid="header">
      <InnerWrapper>
        <LeftPart>
          {isActiveMenu ?
            <i onClick={toggleMenu} style={{ fontSize: '20px' }} className="fal fa-times" /> :
            <i onClick={toggleMenu} style={{ fontSize: '18px' }} className="far fa-bars" />
          }
        </LeftPart>
        <RightPart>
          {/* <i className="fal fa-bell" /> */}
          {/* <Personal>
          <figure>
            <img src="https://i.ytimg.com/vi/x_HL0wiK4Zc/maxresdefault.jpg" alt="123" />
          </figure>
          <span>name</span>
        </Personal> */}
          <span
            onClick={() => {
              account.logout();
              window.location.href = '/auth'
            }}
            style={{ cursor: 'pointer' }}
            className="exit"
          >
            выход
          </span>
        </RightPart>
      </InnerWrapper>
      <MobileList visible={isActiveMenu}>
        <Center>
          <div style={{ display: 'flex', flexDirection: 'column', alightItems: 'center', textAlign: 'center' }}>
            {
              getRoutes().map((el, idx) => {
                return (
                  <a style={{ marginBottom: '40px', fontSize: '20px' }} href={el.link} key={idx}>{el.name}</a>
                )
              })
            }
          </div>
        </Center>
      </MobileList>
    </Wrapper>
  )
}


export default React.memo(HeaderComponent)
