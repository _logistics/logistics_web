import React from 'react'
import T from 'prop-types'
// import { connectStore } from '@/utils/index'
import Header from './Header'

const HeaderContainer = ({ isLoginPage }) => <Header isLoginPage={isLoginPage} />

HeaderContainer.propTypes = {
  isLoginPage: T.bool
}

HeaderContainer.defaultProps = {
  isLoginPage: false
}

export default HeaderContainer
