import styled from 'styled-components';
import { theme, comStyles, getMaxWidth, getPadding } from '@/utils/index'


export const Wrapper = styled.header.attrs((props) => ({
  'data-testid': props.testid,
}))`
  z-index: ${comStyles.zIndex.header};
  width: 100%;
  ${comStyles.flex('justify-center')};
  background-color: white;
  opacity: 1;
  border-bottom: ${({ noBorder }) => (noBorder ? 'none' : '1px solid')};
  border-color: ${theme('neitralColors.neutral4')};
  margin-left: ${({ leftOffset }) => leftOffset};
  @media (max-width: 800px) {
    padding: 0 20px;
    margin-left: 0;
  }
`;

export const InnerWrapper = styled.div`
  ${comStyles.flex()};
  justify-content: space-between;
  max-width: ${comStyles.MAX_CONTENT_WIDTH};
  max-width: ${({ type }) => getMaxWidth(type)};
  padding: ${({ type }) => getPadding(type)};
  width: 100%;
  height: 64px;
  align-items: center;
  transition: all 0.2s;

  ${comStyles.media.laptopLPadding};
  ${comStyles.media.mobile`padding-right: 0`};
`;

export const Personal = styled.div`
  display: flex;
  align-items: center;
  & figure {
    margin: auto;
    margin-right: 15px;
    overflow: hidden;
    height: 30px;
    width: 30px;
    border-radius: 100%;
    display: flex;
    justify-content: center;
    & img {
      height: 100%;
    }
  }
`;

export const LeftPart = styled.div`
  & i {
    cursor: pointer;
    display: none;
  }
  @media (max-width: 1700px) {
    & i {
      display: none;
    }
  }
  @media (max-width: 800px) {
    & i {
      display: block;
    }
  }
`;

export const RightPart = styled.div`
  display: flex;
  align-items: center;
  & span.exit {
    margin-right: 20px;
    font-weight: 600;
    color: #426baa
  }
  & i {
    margin-right: 30px;
  }
`;

export const MobileList = styled.div`
  display: ${({ visible }) => visible ? 'flex' : 'none'};
  align-items: center;
  position: fixed;
  top: 70px;
  bottom: 0;
  right: 0;
  left: 0;
  background-color: white;
  z-index: 5000;
`;
