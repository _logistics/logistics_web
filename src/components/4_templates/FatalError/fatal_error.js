import React from 'react'
import { Button } from '@/components/1_atoms/Buttons'

const FatalError = () => {
  return (
    <div>
      Произошла ошибка при открытии вашего приложения
      <Button>
        Назад
      </Button>
    </div>
  )
}

export default FatalError
