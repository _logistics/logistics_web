import React from 'react'
import T from 'prop-types'
import { Wrapper } from './styles'

const Main = ({ children }) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  )
}

Main.propTypes = {
  children: T.node,
}

Main.defaultProps = {
  children: <div />,
}

export default Main
