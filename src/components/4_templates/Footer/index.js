import React from 'react'
import { connectStore } from '@/utils/index'
import { Wrapper } from './styles'
import { withTranslation } from '@/i18n'


const Ft = ({ t }) => {
  return (
    <Wrapper testid="footer">
      {t('fc')}
    </Wrapper>
  )
}

const FooterContainer = withTranslation('common')(Ft)

export default connectStore(FooterContainer)
