import styled from 'styled-components'
import { comStyles, theme } from '@/utils/index'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.footer.attrs((props) => ({
  'data-testid': props.testid,
}))`
  ${comStyles.flex('justify-center')};
  background-color: ${theme('footer.bg')};
  height: 35px
`
