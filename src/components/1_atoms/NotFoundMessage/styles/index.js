import styled from 'styled-components'
import { theme } from '@/utils/index'

export const HintTitle = styled.div`
  color: ${theme('thread.articleTitle')};
  font-size: 1.2rem;
  margin-bottom: 10px;
`

export const IssueLink = styled.a`
  text-decoration: underline;
  font-weight: bolder;
  transition: color 0.3s;
  color: ${theme('banner.title')};
  margin-left: 5px;

  &:hover {
    color: ${theme('banner.title')};
    cursor: pointer;
  }
`
