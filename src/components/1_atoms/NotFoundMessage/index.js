import React from 'react'
import { isEmpty } from 'ramda'
import { HintTitle } from './styles'


const NotFoundMessage = ({ page, target }) => {
  switch (page) {
    case 'user':
      return (
        <HintTitle>
          Пользователь не найден
          {!isEmpty(target) && <span>: {target}</span>}
        </HintTitle>
      )
    default:
      return <HintTitle>Страница не найдена</HintTitle>
  }
}

export default NotFoundMessage
