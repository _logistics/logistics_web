import React from 'react'
import { Wrapper } from './styles';

const Devider = ({ style }) => {
  return (
    <Wrapper style={style}>
      <div />
      <span>Или</span>
      <div />
    </Wrapper>
  )
}

export default Devider
