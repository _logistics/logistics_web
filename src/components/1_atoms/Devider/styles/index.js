import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  & div {
    height: 1px;
    width: 100%;
    background-color: #F0F0F0;
  }
  & span {
    color: #D9D9D9;
    padding: 0 10px;
  }
`
