import styled from 'styled-components';
import { theme, comStyles } from '@/utils/index';


export const Wrapper = styled.div`
  background-color: ${theme('modal.bg')};
  background-image: url('./images/from.jpg');
  background-size: cover;
  background-position: center;
  max-width: 560px;
  width: 100%;
  border-radius: 20px;
  box-shadow: ${theme('shadow')};
  ${comStyles.flex('justify-center')};
  overflow: hidden;
  display: flex;
  align-items: center;
  @media (max-height: 700px) {
    min-height: 100vh;
  }
  &>div:first-child{
    width: 70%;
    height: 100%;
  }
`;


export const x = 1
