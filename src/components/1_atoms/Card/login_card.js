import React from 'react'
import { Wrapper } from './styles'

const LoginCard = ({ children }) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  )
}

export default LoginCard
