import React from 'react'
import { Wrapper } from './styles'

const FigureImage = (props) => {
  const { src } = props
  return (
    <Wrapper {...props}>
      <img src={src} alt="" />
    </Wrapper>
  )
}

export default FigureImage
