/* eslint-disable import/prefer-default-export */
import styled from 'styled-components'

export const Wrapper = styled.figure`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  height: ${({ height }) => height};
  width: ${({ width }) => width};
  border-radius: ${({ borderRadius }) => borderRadius};
  & img {
    width: 100%
  }
`
