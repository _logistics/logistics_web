import React from 'react'
import { InputSubmit } from './styles'

const SubmitInput = ({ children }) => {
  return (
    <InputSubmit>
      {children}
    </InputSubmit>
  )
}

export default SubmitInput
