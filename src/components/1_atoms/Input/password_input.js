import React, { useCallback } from 'react'
import T from 'prop-types'
import { InputWrapper, InputError, InputLabel } from './styles'
import { withTranslation } from '@/i18n'

const PasswordInput = ({ error, label, input, className, required, t, withIcon }) => {
  // eslint-disable-next-line no-unused-vars
  const [inputType, changeInputType] = React.useState('password')
  const { change } = input
  const onChange = useCallback((e) => {
    e.preventDefault();
    change && change(e.target.value);
  }, [change]);

  return (
    <InputWrapper className={className} withIcon={withIcon}>
      {/* <InputImage
        src={`/images/password_${inputType === 'password' ? 'closed' : 'opened'}.svg`}
        width="25px"
        heigth="25px"
        onClick={() => changeInputType(inputType === 'password' ? 'text' : 'password')}
        pointer="true"
      /> */}
      <input
        type={inputType}
        placeholder={label}
        value={input.value || ''}
        onChange={onChange}
      />
      <InputLabel
        isInputHaveValue={!!input.value}
        className="label"
      >
        {t(label)}
      </InputLabel>
      {!error && required && <InputError>*</InputError>}
      {error && <InputError>{error}</InputError>}
    </InputWrapper>
  )
}

PasswordInput.propTypes = {
  error: T.string,
  label: T.string,
  input: T.exact({
    change: T.func.isRequired,
    value: T.string
  }),
  className: T.string,
  required: T.bool,
  t: T.func.isRequired,
  withIcon: T.bool
}

PasswordInput.defaultProps = {
  error: null,
  label: 'input',
  input: {
    value: '',
    type: 'string'
  },
  className: null,
  required: false,
  withIcon: false
}

export default withTranslation(['common', 'auth'])(PasswordInput)
