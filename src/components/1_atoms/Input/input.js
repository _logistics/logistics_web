/* eslint-disable react/prop-types */
import React, { useCallback } from 'react'
import ReactInputMask from 'react-input-mask'
import T from 'prop-types'
import { InputWrapper, InputError, InputLabel } from './styles'
import { withTranslation } from '@/i18n'
import { path } from 'ramda'
import { wrapNumberMask } from '@/utils/index'
import { Tooltip } from 'antd';

const exSVG = [
  'login',
  'password',
  'name'
]

const Input = ({ error,
  label,
  input,
  className,
  mask,
  required,
  icon,
  t,
  styles,
  disabled,
  style
}) => {
  const { change } = input;
  const size = path(['size'], styles);
  const marginR = path(['marginR'], styles);
  const onlyNumber = !!path(['mask', 'onlyNumber'], input);
  const onChange = useCallback((e) => {
    e.preventDefault();
    let { value } = e.target;
    if (onlyNumber) {
      value = wrapNumberMask(value);
    }
    change && change(value);
  }, [change, onlyNumber]);
  const isImage = icon && exSVG.findIndex((el) => el === icon.toLowerCase()) > -1

  const renderInput = () => {
    if (mask) {
      return (
        <ReactInputMask
          mask={mask}
          value={input.value || ''}
          onChange={onChange}
        >
          <input
            disabled={disabled}
            type={input.type}
            placeholder={label}
          />
        </ReactInputMask>
      )
    }

    return (
      <input
        disabled={disabled}
        type={input.type}
        placeholder={label}
        value={input.value || ''}
        onChange={onChange}
      />
    )
  }

  return (
    <Tooltip placement="topLeft" title={disabled && 'свяжитесь с организацией для изменения данных'}>
      <InputWrapper
        className={className}
        withIcon={isImage}
        marginR={marginR}
        size={size}
        style={style}
      >
        {renderInput()}
        <InputLabel
          isInputHaveValue={!!input.value}
          className="label"
        >
          {t(label)}
        </InputLabel>
        {!error && required && <InputError>*</InputError>}
        {error && <InputError>{error}</InputError>}
      </InputWrapper>
    </Tooltip>
  )
}

Input.propTypes = {
  error: T.string,
  label: T.string,
  input: T.object,
  className: T.string,
  mask: T.string,
  required: T.bool,
  icon: T.string,
  t: T.func.isRequired
}

Input.defaultProps = {
  error: null,
  label: 'input',
  input: {
    value: '',
    type: 'string',
    change: () => { },
  },
  className: null,
  mask: null,
  required: false,
  icon: null
}

export default withTranslation(['common', 'auth'])(Input)
