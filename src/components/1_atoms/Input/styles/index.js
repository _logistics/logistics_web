import styled from 'styled-components'
import { theme, comStyles } from '@/utils/index';
import { ReactSVG } from 'react-svg';

// eslint-disable-next-line import/prefer-default-export
export const InputWrapper = styled.div`
  display: flex;
  ${comStyles.flex('justify-center')};
  width: 100%;
  position: relative;
  width: ${({ size }) => size || '100%'};
  height: 50px;
  min-width: 150px;
  margin-bottom: 20px;
  border-color: white;
  border-radius: 5px;
  border: 1px solid gray;
  &:focus{
    border-color: blue
  }
  & input {
    border: none;
    box-sizing: border-box;
    width: 100%;
    max-width: 100%;
    display: inline-block;
    background-color: transparent !important;
    padding-left: 10px;
    font-size: 16px;
    opacity: 1;
    z-index: 10;
    &::placeholder {
      color: transparent;
    }
    &:focus + div.label {
      top: 7px;
      left: 15px;
      transform: translate(-10px, -99%);
      font-size: 13px;
      background-color: white;
      padding: 0px 7px;
    }
    &:focus ~ legend.legend {
      display: inline-block !important;
    }
  }
  @media (max-width: 800px) {
    width: 100%;
  }
`;


export const InputError = styled.div`

`;
export const InputLabel = styled.div`
  position: absolute;
  top: ${({ isInputHaveValue }) => isInputHaveValue ? '30px' : '45%'};
  left: ${({ isInputHaveValue }) => isInputHaveValue ? '15px' : '2px'};
  font-size: ${({ isInputHaveValue }) => isInputHaveValue ? '13px' : '16px'};
  transform: ${({ isInputHaveValue }) => isInputHaveValue ? 'translate(-10px, -200%)' : 'translate(20px, -50%)'};
  background-color: ${({ isInputHaveValue }) => isInputHaveValue ? 'white' : 'transparent'};
  padding: ${({ isInputHaveValue }) => isInputHaveValue ? '0 7px' : '0'};
  z-index: 9;
  transition: all .2s ease;
  pointer-events: none;
  color: ${theme('neitralColors.neutral6')};
`;
export const InputImage = styled(ReactSVG)`
  width: 20px;
  height: 20px;
  padding: 0 8px;
  transform: translate(-5px, -5px);
  cursor: ${({ pointer }) => pointer === 'true' ? 'pointer' : 'info'};
  & div{
    margin: 0;
  }
`;
export const InputSubmit = styled.button`
  width: 100%;
  height: 40px;
  box-shadow: ${theme('shadow')};
  color: white;
  font-weight: bold;
  border: 1px solid ${theme('modal.button')};
  border-radius: 20px;
  cursor: pointer;
  background-color: ${theme('modal.button')};
  text-transform: uppercase;
  padding: 10px 0;
`;
