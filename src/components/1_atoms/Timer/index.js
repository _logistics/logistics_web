import React, { useEffect } from 'react';
import { dateTime } from '@dev-bi/rct_utils';
import { pipe } from 'ramda';

const Timer = ({ t, ct }) => {
  useEffect(() => {
    if (t > 0) {
      setTimeout(() => {
        ct(t - 1);
      }, 1000);
    }
  }, [t, ct]);

  const timeStr = pipe(dateTime.prepSec, (t) =>
    dateTime.reformate(t, 'time-only')
  )(t);

  return <div>{t ? timeStr : null}</div>;
};

export default Timer;
