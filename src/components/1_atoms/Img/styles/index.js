import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const Image = styled.img`
  position: ${({ loaded }) => (loaded ? 'relative' : 'absolute')};
  opacity: ${({ loaded }) => (loaded ? 1 : 0)};
  z-index: ${({ loaded }) => (loaded ? 1 : -1)};
`
