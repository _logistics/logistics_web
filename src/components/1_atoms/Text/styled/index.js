import styled from 'styled-components'
import { theme } from '@/utils/index'

export const SecondaryText = styled.span`
  color: #333333;
  font-size: .9em;
`

export const Text = styled.span`
  color: ${({ colored }) => colored ? theme('modal.headers.color') : 'black'};
`
