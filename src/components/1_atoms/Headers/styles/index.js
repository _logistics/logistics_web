import styled from 'styled-components'
import { theme } from '@/utils/index';


export const SignHeader = styled.div`
  text-align: center;
  color: ${({ noColor }) => noColor ? 'black' : theme('modal.headers.color')};
  font-family: 'Nunito', sans-serif;
  font-size: 1.2em;
  font-weight: bold;
  margin-bottom: 25px;
  & h1 {
    font-size: 24px;
    color: #10239E;
    margin-bottom: 5px;
    font-weight: 800;
    text-transform: uppercase;
  }
  & p {
    font-size: 13px;
    font-weight: 300;
    color: #8C8C8C;
  }
`;
export const SignPageHeader = styled.div``;
