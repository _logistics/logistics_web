import styled from 'styled-components';

export const TextLink = styled.a`
  color: #666;
  padding: 0 5px;
  &:hover{
    color: black;
  }
  &:visited{
    color: #666;
  }
  &:active{
    color: black;
  }
`

export const A = styled.a`

`
