import React from 'react'
import { Wrapper } from './styles'

const Center = ({ children, className }) => {
  return (
    <Wrapper className={className}>
      <div className="center">
        {children}
      </div>
    </Wrapper>
  )
}

export default Center
