/* eslint-disable import/prefer-default-export */
import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  & figure {
    margin: 20px auto;
  }
  & div.center {

  }
`
