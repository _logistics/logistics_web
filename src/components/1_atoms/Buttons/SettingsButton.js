import React from 'react'
import { Wrapper, SettingsIcon } from './styles/settings_button'

const SettingsButton = () => {
  return (
    <Wrapper>
      <SettingsIcon src="/images/settings.svg" />
    </Wrapper>
  )
}

export default SettingsButton
