import React from 'react'
import T from 'prop-types'

import { Wrapper } from './styles/button';

const Button = ({
  children, reversed, type, onClick, size, className, whiteTxt, mod, icon, iconColor
}) => {
  switch (type) {
    default: {
      return (
        <Wrapper
          reversed={reversed}
          onClick={onClick}
          size={size}
          className={className}
          whiteTxt={whiteTxt}
          mod={mod}
          icon={icon}
          iconColor={iconColor}
        >
          {icon ? <i className={icon} /> : null}
          {children}
        </Wrapper>
      )
    }
  }
}

Button.propTypes = {
  children: T.oneOfType([T.string, T.node]),
  reversed: T.bool,
  type: T.oneOf(['primary', 'reversed']),
  size: T.oneOf(['default', 'small', 'full']),
  onClick: T.func,
  className: T.string,
  whiteTxt: T.bool,
  icon: T.string,
  iconColor: T.string
}

Button.defaultProps = {
  children: 'Button',
  reversed: false,
  type: 'primary',
  size: 'default',
  // eslint-disable-next-line no-console
  onClick: console.log,
  className: '',
  whiteTxt: false,
  icon: null,
  iconColor: '#000000'
}

export default React.memo(Button)
