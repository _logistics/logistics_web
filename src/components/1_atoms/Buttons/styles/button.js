import styled from 'styled-components'
import { comStyles, theme } from '../../../../utils';
import { lighten } from 'polished'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.button`
  ${comStyles.flex('jcc')};
  --webkit-appearance: button;
  outline: none;
  line-height: 1.5;
  font-weight: bold;
  text-align: center;
  padding: ${({ size }) => {
    switch (size) {
      case 'default':
        return '10px 15px'
      case 'full':
        return '10px 0'
      default:
        return '4px 15px'
    }
  }};
  width: ${({ size }) => {
    switch (size) {
      case 'full':
        return '100%'
      default:
        return 'auto'
    }
  }};
  ${({ size }) => {
    switch (size) {
      case 'full':
        return 'display: flex; justify-content: center; align-items: center; text-transform: uppercase;'
      default:
        return null
    }
  }};
  touch-action: manipulation;
  border-radius: 5px;
  border: 1px solid;
  cursor: pointer;
  margin: 10px 5px;

  color: ${({ reversed, mod }) => {
    switch (mod) {
      case 'secondary':
        return reversed ? theme('button.bg') : theme('button.secondary')
      default:
        return reversed ? theme('button.bg') : theme('button.primary')
    }
  }};
  border-color: ${({ mod }) => {
    switch (mod) {
      case 'secondary':
        return theme('button.secondary')
      default:
        return theme('button.primary')
    }
  }};
  background-color: ${({ reversed }) =>
    reversed ? theme('button.primary') : 'transparent'};

  & i, svg {
    margin-right: 10px;
  }

  & i {
    font-size: 18px;
    color: ${({ iconColor }) => iconColor
  }
  }

  &:hover {
    color: ${({ reversed, whiteTxt, mod }) => {
    if (whiteTxt) {
      return reversed ? 'white' : theme('button.primary')
    }
    switch (mod) {
      case 'secondary':
        return reversed ? theme('button.secondary') : 'white'
      default:
        return reversed ? theme('button.primary') : 'white'
    }
  }};

    border-color: ${({ mod }) => {
    switch (mod) {
      case 'secondary':
        return theme('button.secondary');
      default:
        return theme('button.hover');
    }
  }};
    background-color: ${({ reversed, mod }) => {
    switch (mod) {
      case 'secondary':
        return reversed ? 'transparent' : theme('button.secondary');
      default:
        return reversed ? 'transparent' : theme('button.hover');
    }
  }};
  }

  &:hover i {
    color: ${({ iconColor }) => lighten(0.2, iconColor)
  };

  &:focus {
    color: ${({ reversed }) => {
    return reversed ? theme('button.bg') : theme('button.primary');
  }};
    border-color: ${theme('button.hover')};
    background-color: ${({ reversed }) =>
    reversed ? theme('button.hover') : 'transparent'};
  }
  &:active {
    color: ${({ reversed }) =>
    reversed ? theme('button.bg') : theme('button.primary')};
    border-color: ${theme('button.hover')};
    background-color: ${({ reversed }) =>
    reversed ? theme('button.hover') : 'transparent'};
  }
`
