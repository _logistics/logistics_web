import styled from 'styled-components'
import { theme } from '@/utils/index'
import { Wrapper as SidebarWrapper } from '@/components/3_organisms/Sidebar/styles'
import { ReactSVG } from 'react-svg'

export const Wrapper = styled.div`
  cursor: pointer;
`
export const SettingsIcon = styled(ReactSVG)`
  fill: ${({ pin }) => (pin === 'true' ? theme('sidebar.pinActive') : 'grey')};
  & svg{
    margin-right: 10px;
    width: 18px;
    height: 18px;
  }
  &:hover {
    visibility: ${({ pin }) => (pin === 'true' ? 'visible' : 'hidden')};
    opacity: ${({ pin }) => (pin === 'true' ? 1 : 0)};
    transition: visibility 0s, opacity 0.3s linear;
  }
  cursor: pointer;

  ${SidebarWrapper}:hover & {
    visibility: visible;
    opacity: 1;
    transition-delay: 0.4s;
  }
`
