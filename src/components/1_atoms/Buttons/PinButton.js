import React from 'react'
import T from 'prop-types'
import { Wrapper, PinIcon } from './styles/pin_button'

const PinButton = ({ pin, setPin }) => {
  return (
    <Wrapper onClick={() => setPin(!pin)}>
      <PinIcon pin={`${pin}`} src="/images/pin.svg" />
    </Wrapper>
  )
}

PinButton.propTypes = {
  pin: T.bool.isRequired,
  setPin: T.func.isRequired
}


export default PinButton
