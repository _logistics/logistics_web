/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-case-declarations */
import React from 'react';
import { Wrapper, Map, ContList } from './styles';

const ContactsPage = () => {
  return (
    <Wrapper>
      <div style={{ width: '60%' }}>
        <figure style={{ overflow: 'hidden', width: '100%', height: '70vh' }}>
          <Map />
        </figure>
      </div>
      <ContList>
        <li>
          <span>Почта:</span>
          <a href="tempkurier@yandex.ru">tempkurier@yandex.ru</a>
        </li>
        <li>
          <span>Телефон:</span>
          <a href="tel:+78123099691">+7(812)309-96-91</a>
        </li>
        <li>
          <span>Адрес:</span>
          <span>площадь Конституции, дом 7, офис 300Б</span>
        </li>
      </ContList>
    </Wrapper>
  )
}

export default ContactsPage
