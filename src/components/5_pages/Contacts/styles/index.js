import styled from 'styled-components';
import { lighten } from 'polished'

/* eslint-disable import/prefer-default-export */
export const Wrapper = styled.div`
  width: 100%;
  padding: 20px;
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export const Map = styled.div`
  height: 100%;
  background: url('https://sun9-63.userapi.com/impg/vu4dB76-leovUWlnKK0eTcpnTszVBFxqz4aQbg/jVodLIGN80E.jpg?size=1138x929&quality=96&proxy=1&sign=27148896587c1cad24c3b9d36ffdecac&type=album') no-repeat scroll 0 0 transparent;
  background-position: center;
`;

export const ContList = styled.div`
  width: 35%;
  list-style-type: none;
  font-size: 16px;
  color: ${lighten(0.4, 'black')};
  & li {
    margin-bottom: 20px;
  }
  & li span:first-child {
    min-width: 120px;
    display: block;
  }
`;
