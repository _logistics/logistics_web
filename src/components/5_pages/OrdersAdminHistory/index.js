/* eslint-disable new-cap */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-case-declarations */
import React, { useEffect, useState } from 'react'
import { Table, Button, Space, DatePicker, Tooltip, Select } from 'antd'
import { useStore } from '@/stores/init'
import { Wrapper } from './styles'
import { jsPDF } from 'jspdf'
import { LinkOutlined } from '@ant-design/icons'
import html2canvas from 'html2canvas'
import { dateTime } from '@dev-bi/rct_utils'
import { uniq, pipe, cond, T, equals } from 'ramda'
import { GRAPHQL_ENDPOINT } from '@/config/index'
import { openNotifi } from '@/utils/index'
import { always } from 'ramda'
import { is } from 'ramda'

const OrdersAdminHistory = () => {
  const { creatingOrder, account } = useStore({})
  const [ordersData, setOrdersData] = useState([])
  const [users, setUsers] = useState([])
  const [selectedUser, setSelectedUser] = useState('0')
  const [sumPrices, setSumPrices] = useState(null)
  const [dateStart, setDateStart] = useState(null)
  const [deteEnd, setDeteEnd] = useState(null)
  const [loading, setLoading] = useState(false)
  const [uniqProfiles, setUniqProfiles] = useState([])
  const [reportLoading, setReportLoading] = useState(false)
  const size = 10
  const domain = GRAPHQL_ENDPOINT.split('/help')[0].split('/api')[0]
  Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + h * 60 * 60 * 1000)
    return this
  }

  const logistOrAdmin =
    account.isAdmin() || account.isLogistician() || account.isSuper()

  const readSenderRecords = (record) => {
    if (!uniqProfiles) {
      return 'нет данных'
    }
    const id = record?.sender?.id
    const profile = uniqProfiles.find((el) => +el.id === +id)
    const senderFio = record?.senderFio
    return cond([
      [() => !profile, () => 'нет данных'],
      [() => !profile.profile, () => 'профиль не заполнен'],
      [
        () => profile.isOrganization,
        () => profile.profile.orgName || 'Название не заполнено',
      ],
      [
        () => !profile.isOrganization,
        () => {
          const { firstName, middleName, lastName } = profile.profile
          const fullName = [firstName, middleName, lastName]
          return (
            fullName.reduce((acc, cur) => (cur ? `${acc} ${cur[0]}.` : acc)) +
              (senderFio ? `(${senderFio})` : '') || 'Имя не заполнено'
          )
        },
      ],
      [T, () => 'error'],
    ])(profile)
  }

  const calcWeght = (record) =>
    record.products.reduce((acc, cur) => {
      return acc + cur.weight
    }, 0)

  const colums = [
    {
      title: 'Номер заказа',
      dataIndex: 'id',
      key: 'id',
      width: '8%',
      align: 'center',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: 'Дата доставки',
      dataIndex: 'deliveryAwaitedDatetime',
      key: 'deliveryAwaitedDatetime',
      render: (text, record) => {
        const dt = record?.deliveryAwaitedDatetime
        return `${
          (dt &&
            new Date(dt).addHours(3).toLocaleString('ru-RU').slice(0, -3)) ||
          ''
        }`
      },
      sorter: (a, b) =>
        new Date(a.deliveryAwaitedDatetime) -
        new Date(b.deliveryAwaitedDatetime),
    },
    {
      title: 'Статус заказа',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => {
        const rcdName = record.orderStatus?.name || ''
        const clr = cond([
          [equals('Подтвержден'), always('#64b5f6')],
          [equals('Заказ доставлен'), always('#dce775')],
          [equals('Заказ оплачен'), always('#81c784')],
          [equals('Заказ в работе'), always('#a1887f')],
          [equals('Заказ в пути'), always('#a1887f')],
          [equals('Ожидает подтверждения'), always('#ffb74d')],
          [T, always('red')],
        ])(rcdName)
        return <span style={{ color: clr }}>{rcdName}</span>
      },
      sorter: (a, b) => a.orderStatus?.id - b.orderStatus?.id,
      filters: [
        { text: 'Заказ доставлен', value: 'Заказ доставлен' },
        { text: 'Заказ оплачен', value: 'Заказ оплачен' },
        { text: 'Подтвержден', value: 'Подтвержден' },
        { text: 'Заказ в работе', value: 'Заказ в работе' },
        { text: 'Заказ не оплачен', value: 'Заказ не оплачен' },
        { text: 'Заказ в пути', value: 'Заказ в пути' },
        { text: 'Ожидает подтверждения', value: 'Ожидает подтверждения' },
      ],
      onFilter: (value, record) => record.orderStatus?.name === value,
    },
    {
      title: 'Отправитель',
      key: 'sender',
      render: (_, record) => readSenderRecords(record),
      sorter: (a, b) =>
        readSenderRecords(a).localeCompare(readSenderRecords(b)),
    },
    {
      title: 'Адрес отправителя',
      dataIndex: 'senderAddress',
      key: 'senderAddress',
    },
    {
      title: 'Получатель',
      dataIndex: 'receiverFio',
      key: 'receiverFio',
    },
    {
      title: 'Адрес получателя',
      dataIndex: 'receiverAddress',
      key: 'receiverAddress',
    },
    {
      title: 'Доставлено',
      dataIndex: 'isDelivered',
      align: 'center',
      sorter: (a, b) => {
        const isA = a.orderStatus?.id > 4,
          isB = b.orderStatus?.id > 4
        return isA === isB ? 0 : isA ? -1 : 1
      },
      render: (text, record) => {
        if (record.orderStatus?.id > 4)
          return (
            <span
              style={{
                textAlign: 'center',
                fontSize: '20px',
                fontWeight: '900',
              }}
            >
              +
            </span>
          )
        return ''
      },
    },
    {
      title: 'Вес',
      dataIndex: 'weight',
      key: 'weight',
      render: (_, record) => calcWeght(record),
      sorter: (a, b) => calcWeght(a) - calcWeght(b),
    },
    {
      title: 'Страховка',
      dataIndex: 'insurance',
      key: 'insurance',
    },
    {
      title: 'Цена',
      dataIndex: 'price',
      key: 'price',
      fixed: 'right',
      sorter: (a, b) => a.price - b.price,
    },
  ]

  const downloadPDFWithjsPDF = () => {
    html2canvas(document.querySelector('#pdfTable')).then((canvas) => {
      const imgData = canvas.toDataURL('image/jpeg')
      const doc = new jsPDF({
        orientation: 'p',
        unit: 'px',
        format: [canvas.height * 0.6, 900],
      })
      doc.addImage(imgData, 'jpeg', 10, 10)
      doc.save('TK_report.pdf')
    })
  }

  const prepairDate = (date) => {
    return `${dateTime.parse(date, '-', 'YMD')} ${dateTime.parse(
      date,
      '.',
      'HMS',
    )}`
  }

  const getAllPages = async (pageNumber, totalPages, oldData) => {
    if (pageNumber < totalPages) {
      creatingOrder
        .getListOrdersByDate(
          prepairDate(dateStart),
          prepairDate(deteEnd),
          pageNumber + 1,
        )
        .then((el) => {
          const { totalPages, pageNumber, entries } = el.listOrders
          const newData = [...oldData, ...entries]
          getAllPages(pageNumber, totalPages, newData)
        })
    } else {
      prepOrdersData(oldData)
      setSumPrices(
        oldData.reduce((acc, cur) => {
          return cur.price + acc
        }, 0),
      )
      setLoading(false)
      return null
    }
  }
  useEffect(() => {
    account.getUsersList().then(({ users: { entries } }) => setUsers(entries))
  }, [])

  const prepOrdersData = async (data) => {
    pipe(
      (x) => x.map((el) => +el.sender?.id),
      (x) => uniq(x),
      (x) =>
        account.getUniqueUsers(x).then((el) => {
          el && setUniqProfiles(el.getUniqueUsers)
          if (is(Array, data)) {
            setOrdersData(data.filter((uData) => uData.orderStatus))
          }
        }),
    )(data)
  }

  const getOrders = () => {
    setLoading(true)
    creatingOrder
      .getListOrdersByDateUser(
        prepairDate(dateStart),
        prepairDate(deteEnd),
        selectedUser,
      )
      .then((el) => {
        const list = el.listOrdersNoFilters
        if (is(Array, list)) {
          prepOrdersData(list)
        } else {
          setOrdersData([])
        }
        setLoading(false)
      })
  }

  const getDataLink = () => {
    setReportLoading(true)
    creatingOrder.makeAllDocs(dateStart, deteEnd, selectedUser).then((el) => {
      if (el) {
        const excel = el && el?.makeAllDocs?.excel
        window.open(`${domain}/${excel}`, '_blank')
        openNotifi('Успех', 'Файл успешно создан', 'success')
        setReportLoading(false)
      }
    })
  }
  return (
    <Wrapper>
      <Space direction="vertical">
        <Space style={{ margin: 20 }} size="middle">
          <Select
            showSearch
            style={{ width: 200 }}
            placeholder="Выберите пользователя"
            optionFilterProp="children"
            onSelect={(e) => setSelectedUser(e)}
            defaultValue={'0'}
          >
            <Select.Option value={`0`} key={`0`}>
              Все
            </Select.Option>
            {users.map((e) => (
              <Select.Option value={`${e.id}`} key={`${e.id}`}>
                {e?.profile?.firstName
                  ? `${e?.profile?.firstName} ${e?.profile?.middleName} ${e?.profile?.lastName}`
                  : e?.phone}
              </Select.Option>
            ))}
          </Select>
          <DatePicker
            placeholder="Дата начала"
            value={dateStart}
            showTime
            onChange={(v) => {
              setDateStart(v)
            }}
            onOk={() => {}}
          />
          <DatePicker
            placeholder="Дата конца"
            value={deteEnd}
            showTime
            onChange={(v) => {
              setDeteEnd(v)
            }}
            onOk={() => {}}
          />
          <Tooltip
            title={
              !dateStart || !deteEnd
                ? 'Необходимо заполнить дату начала и дату конца'
                : ''
            }
          >
            <Button
              disabled={!dateStart || !deteEnd}
              type="primary"
              onClick={getOrders}
            >
              Получить данные
            </Button>
          </Tooltip>
          {/* <Tooltip
            title={
              !dateStart || !ordersData.length || !deteEnd
                ? 'Необходимо заполнить дату начала, дату конца и выгрузить данные'
                : ''
            }
          >
            <Button
              disabled={!dateStart || !deteEnd || !ordersData.length}
              type="primary"
              ghost
              onClick={downloadPDFWithjsPDF}
              icon={<DownloadOutlined />}
            >
              Выгрузить pdf
            </Button>
          </Tooltip> */}
          <Tooltip
            title={
              !dateStart || !deteEnd
                ? 'Необходимо заполнить дату начала и дату конца'
                : ''
            }
          >
            <Button
              loading={reportLoading}
              disabled={!dateStart || !deteEnd}
              type="primary"
              ghost
              onClick={getDataLink}
              icon={<LinkOutlined />}
            >
              Выгрузить xlsx
            </Button>
          </Tooltip>
        </Space>
        <div id="pdfTable">
          <Table
            dataSource={ordersData}
            columns={colums}
            size={size}
            loading={loading}
            pagination={{ pageSize: 10 }}
            style={{ maxWidth: 'calc(100% - 20px)' }}
            scroll={{ x: 'calc(700px + 50%)' }}
            onRow={(record) => {
              return {
                onClick: () => {
                  window.location.href = `/order/${record.id}`
                },
              }
            }}
          />
          {logistOrAdmin && sumPrices > 0 && (
            <p style={{ padding: 20 }}>{`Общая сумма: ${sumPrices} рублей`}</p>
          )}
        </div>
      </Space>
    </Wrapper>
  )
}

export default OrdersAdminHistory
