/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-case-declarations */
import React, { useState, useEffect } from 'react'
import { Button, Table, Tabs } from 'antd'
import { path } from 'ramda';
import { useStore } from '@/stores/init'
import { getArrayOfValuesByKey } from 'core_js_devbi'
import { Wrapper } from './style'

const AccountsManage = () => {
  const { account } = useStore({});
  const [users, setUsers] = useState([]);
  const [courier, setCourier] = useState([]);
  const [logistician, setLogistician] = useState([]);
  const getObjArrayByRole = (array, role) => {
    return array.map((el) => {
      const roles = getArrayOfValuesByKey(el && el.roles, 'slug');
      if (roles.includes(role)) {
        return el;
      }
      return null
    }).filter((el) => el)
  }
  useEffect(() => {
    account.getUsersList().then((el) => {
      const { entries } = el.users;
      setUsers(getObjArrayByRole(entries, 'пользователь'));
      setCourier(getObjArrayByRole(entries, 'курьер'));
      setLogistician(getObjArrayByRole(entries, 'логист'));
      setUsers(el.users.entries);
    })
  }, [])
  const { TabPane } = Tabs;

  function callback() {
    return null
  }

  const columns = [
    {
      title: 'ФИО / Телефон',
      dataIndex: 'nickname',
      key: 'nickname'
    }
  ]

  const prepareUserRows = (usersList) => {
    return usersList.map((el, idx) => {
      const firstName = path(['profile', 'firstName'], el);
      const middleName = path(['profile', 'middleName'], el);
      const lastName = path(['profile', 'lastName'], el);
      if (firstName || middleName || lastName) {
        return {
          nickname: `${firstName || ''} ${middleName || ''} ${lastName || ''}`,
          key: idx,
          id: el.id
        }
      }
      return {
        nickname: el.phone,
        key: idx,
        id: el.id
      }
    })
  }

  const newAccount = () => {
    window.location.href = '/account/create';
  }

  return (
    <Wrapper>
      <Tabs defaultActiveKey="1" onChange={callback}>
        <TabPane tab="Курьеры" key="1">
          <Button type="primary" onClick={newAccount}>Создать нового</Button>
          <Table
            size={10}
            dataSource={prepareUserRows(courier)}
            columns={columns}
            onRow={(record) => {
              return {
                onClick: () => { window.location.href = `/account/${record.id}` }
              };
            }}
          />
        </TabPane>
        <TabPane tab="Логисты" key="2">
          <Button type="primary" onClick={newAccount}>Создать нового</Button>
          <Table
            dataSource={prepareUserRows(logistician)}
            columns={columns}
            onRow={(record) => {
              return {
                onClick: () => { window.location.href = `/account/${record.id}` }
              };
            }}
          />
        </TabPane>
        <TabPane tab="Пользователи" key="3">
          <Button type="primary" onClick={newAccount}>Создать нового</Button>
          <Table
            dataSource={prepareUserRows(users)}
            columns={columns}
            onRow={(record) => {
              return {
                onClick: () => { window.location.href = `/account/${record.id}` }
              };
            }}
          />
        </TabPane>
      </Tabs>
    </Wrapper>
  )
}

export default AccountsManage
