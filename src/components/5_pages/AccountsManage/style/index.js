import styled from 'styled-components';
/* eslint-disable import/prefer-default-export */
export const Wrapper = styled.div`
  width: 100%;
  @media only screen and (max-width: 800px) {
    width: 700px;
  }
  @media only screen and (max-width: 700px) {
    width: 600px;
  }
  @media only screen and (max-width: 600px) {
    width: 500px;
    margin-bottom: -100px;
  }
  @media only screen and (max-width: 500px) {
    width: 400px;
  }
  @media only screen and (max-width: 400px) {
    width: 320px;
  }
`;
