import styled from 'styled-components';
import { comStyles, theme } from '@/utils/index'

export const Wrapper = styled.div`
  ${comStyles.flexColumn('align-center', 'justify-between')};
  height: 100vh;
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
  background: ${theme('banner.bg')};
  border-top: 4px solid;
  border-top-color: #ff8c77;
`

export const LogoWrapper = styled.div`
  align-self: center;
  margin-right: -5px;
  margin-top: 5px;
`
export const TextWrapper = styled.div`
  ${comStyles.flexColumn('align-center')};
  margin-top: 40px;
`
