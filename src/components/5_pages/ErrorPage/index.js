import React from 'react'
import T from 'prop-types'
import { Wrapper, TextWrapper } from './styles'
import NotFoundMessage from '@/components/1_atoms/NotFoundMessage'


const ErrorPage = ({ errorCode, page, target }) => (
  <Wrapper>
    <TextWrapper>
      {errorCode === 404 ? <NotFoundMessage /> : <div>{page} : {target}</div>}
    </TextWrapper>
  </Wrapper>
)

ErrorPage.propTypes = {
  errorCode: T.number,
  page: T.string,
  target: T.string,
}

ErrorPage.defaultProps = {
  errorCode: 404,
  page: '',
  target: '',
}

export default React.memo(ErrorPage)
