import styled from 'styled-components'
import { comStyles } from '@/utils/index';

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  min-height: calc(100vh);
  background-color: #2196F3;
  background-image: url('https://sun9-43.userapi.com/zrsvOuNEoKZbTR5tLKJ4Jasb96XonYue8aOeLA/jdp2MX6WcDM.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover;
  width: 100%;
  align-items: center;
  ${comStyles.flex('justify-center')};
`;

export const FormsWrapper = styled.div`
  width: 30%;
  min-width: 400px;
  max-height: calc(90vh);
  display: flex;
  align-items: center;
  @media (max-height: 700px) {
    min-height: 100vh;
  }
  padding: 10px 20px;
  overflow-y: hidden;
`;
