import React, { useState } from 'react'
import { Wrapper, FormsWrapper } from './styles'
import { LoginCard } from '@/components/1_atoms/Card'
import { LoginForm, RegisterForm } from '@/components/2_molecules/Form'

const Authentication = () => {
  const [step, setStep] = useState('login');
  const getForm = () => {
    switch (step) {
      case 'login':
        return <LoginForm setStep={setStep} />;
      case 'signup':
        return <RegisterForm prefix="s" setStep={setStep} />;
      case 'recovery':
        return <RegisterForm prefix="r" setStep={setStep} />;
      default:
        return <LoginForm setStep={setStep} />;
    }
  }
  return (
    <Wrapper>
      <LoginCard>
        <FormsWrapper>
          {getForm()}
        </FormsWrapper>
      </LoginCard>
    </Wrapper>
  )
}


export default Authentication
