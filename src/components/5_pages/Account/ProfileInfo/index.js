// eslint-disable-next-line import/no-unresolved
import Center from '@/atoms/Center/'
import FigureImage from '@/components/1_atoms/FigureImage'
import React from 'react'
import { Wrapper, OtherInfo } from './styles'

const ProfileInfo = ({ id, isOrganization, FIO, roles, contactPersonFio }) => {
  const name = FIO && FIO.trim() ? FIO : contactPersonFio;
  return (
    <Wrapper>
      <Center className="profileInfo_mainInfo">
        <FigureImage src="https://picsum.photos/200/300" height="100px" width="100px" borderRadius="100%" />
        <p className="name">{name}</p>
        <p className="slug">{roles && roles[0].slug}</p>
      </Center>
      <OtherInfo>
        <p>id: {id}</p>
        <p>{isOrganization ? 'Юр. лицо' : 'Физ. лицо'}</p>
      </OtherInfo>
    </Wrapper>
  )
}

export default ProfileInfo
