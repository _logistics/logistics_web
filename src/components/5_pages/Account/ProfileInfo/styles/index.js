import styled from 'styled-components'
import { lighten } from 'polished'

export const OtherInfo = styled.div`
  border-bottom: 1px solid ${lighten(0.4, 'gray')};
  @media only screen and (max-width: 800px) {
    border: none;
  }
`


export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 80vh;
  border-right: 1px solid ${lighten(0.4, 'gray')};
  padding: 24px;
  max-width: 360px;
  width: 100%;
  & .profileInfo_mainInfo {
    & p {
      text-align: center;
    }
  }
  @media only screen and (max-width: 800px) {
    height: 240px;
    border: none;
    flex-direction: row;
    justify-content: space-between;
    align-items: center
  }
`
