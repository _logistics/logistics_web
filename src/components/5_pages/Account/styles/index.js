import styled from 'styled-components'

export const Wrapper = styled.div`
  display:flex;
  @media only screen and (max-width: 800px) {
    flex-direction: column;
  }
`

export const SettingsCont = styled.div`
  width: 100%;
  padding: 30px 24px;
`

export const Buttons = styled.div`
  display: flex;
`

export const ChoosePersonal = styled.div`
  width: 100%;
  margin-bottom: 20px;
`
