/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
import { Button } from '@/components/1_atoms/Buttons'
import Center from '@/components/1_atoms/Center'
import { formBuilder } from '@/components/0_functionals/builder'
import { useStore } from '@/stores/init'
import { curry, path, pick, assoc } from 'ramda'
import { Result } from 'antd'
import React, { useState, useEffect } from 'react'
import { address, mainfields, requisites, orgFields } from './fields'
import { pipe, getArrayOfValuesByKey } from '@/utils/index'
import ProfileInfo from './ProfileInfo'
import { Wrapper, SettingsCont, Buttons } from './styles'
import NewAccount from './NewAccount'
import { AddressSuggestions } from '@/components/2_molecules/Dadata/AddressSuggestions'

const AccountCont = ({ userId }) => {
  const [personals, cPersonals] = useState(null)
  const [errors] = useState({})
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }))
  const { account, creatingOrder } = useStore({})
  const user = path(['user'], account)
  const [userExist, setUserExist] = useState(false)
  useEffect(() => {
    account.getUser(+userId).then((el) => {
      if (el && el.user) {
        setUserExist(true)
        const { profile } = el.user
        const name =
          profile &&
          `${profile.firstName || ''} ${profile.middleName || ''} ${
            profile.lastName || ''
          }`
        cPersonals({ ...profile, ...{ FIO: name || '' }, ...el.user })
      }
    })
  }, [])

  if (!personals) return <p>Загрузка</p>
  const setSettings = () => {
    const commonData = [
      'apartment',
      'area',
      'floor',
      'mail',
      'state',
      'street',
      'telegram',
      'town',
      'vk',
      'whatsapp',
      'houseNumber',
    ]
    const getProfileData = () => {
      if (personals.isOrganization) {
        return pick(
          [...commonData, 'bik', 'contactPersonFio', 'inn', 'orgName', 'rs'],
          personals,
        )
      }
      return pick([...commonData], personals)
    }
    const FIO = personals.FIO.trim().split(' ')
    const firstName = FIO[0] || null
    const middleName = FIO[1] || null
    const lastName = FIO[2] || null
    const userData = assoc(
      'id',
      +personals.id,
      pick(
        ['notes', 'oldPassword', 'password', 'passwordConfirmation'],
        personals,
      ),
    )
    const profileData = pipe(getProfileData(user.isOrganization))(
      (x) => assoc('userId', +personals.id, x),
      (x) => assoc('firstName', firstName, x),
      (x) => assoc('middleName', middleName, x),
      (x) => assoc('lastName', lastName, x),
      (x) =>
        assoc(
          'contactPersonFio',
          personals.FIO.trim() || personals.contactPersonFio || null,
          x,
        ),
    )
    profileData.town = personals?.town?.value
    profileData.area = personals?.town?.value
    profileData.houseNumber = personals?.town?.value
    profileData.street = personals?.town?.value
    account.setUserSettings(userData, profileData, personals.isOrganization)
  }

  if (userId === -1) {
    return <p>none</p>
  }

  if (!userExist) {
    return (
      <Result
        status="500"
        title="500"
        subTitle="Данного пользователя не существует"
        extra={
          <Button
            onClick={() => {
              window.location.href = '/'
            }}
            type="primary"
          >
            На главную
          </Button>
        }
      />
    )
  }

  const getOtherSettings = () => {
    if (personals.isOrganization) {
      return (
        <>
          <h3>Контакты представителя</h3>
          {formBuilder(orgFields, personals, cData, errors, '100%')}
          <h3>Адрес организации</h3>
          <AddressSuggestions
            onChange={(e) => {
              cData('town')(e)
            }}
            value={personals?.town || ''}
            token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
            defaultQuery={personals?.town || ''}
          />
          {formBuilder(address, personals, cData, errors, '100%')}
          <h3>Реквизиты организации</h3>
          {formBuilder(requisites, personals, cData, errors, '100%')}
        </>
      )
    }
    console.log(personals?.town)
    return (
      <>
        <h3>Ваши контакты</h3>
        {formBuilder(mainfields, personals, cData, errors, '100%')}
        <h3>Адрес</h3>
        <AddressSuggestions
          onChange={(e) => {
            cData('town')(e)
          }}
          value={personals?.town === undefined ? '' : personals?.town}
          token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
          placeholder="Введите адрес"
          defaultQuery={personals?.town ? '' : `${personals?.town}`}
        />
        {formBuilder(address, personals, cData, errors, '100%')}
      </>
    )
  }

  const edtUserNotAdmin =
    personals.roles?.filter((el) => el.id === 2 || el.id === 5).length > 0
  return (
    <Wrapper>
      <ProfileInfo {...personals} />
      <SettingsCont>
        <h1>Настройки</h1>
        <Center>
          {getOtherSettings()}
          <Buttons>
            <Button reversed onClick={setSettings}>
              Сохранить
            </Button>
            {account.isSuperLgst() && edtUserNotAdmin ? (
              <Button
                onClick={() => {
                  const idxRls = getArrayOfValuesByKey(personals.roles, 'id')
                  if (idxRls.includes(5)) {
                    const password = prompt(
                      'Укажите пароль курьера для релога',
                      100,
                    )
                    creatingOrder.relogSendCourier(+personals.id, `${password}`)
                  } else {
                    creatingOrder.relogSendClient(+personals.id)
                  }
                }}
              >
                Отправить в релог
              </Button>
            ) : null}
          </Buttons>
        </Center>
      </SettingsCont>
    </Wrapper>
  )
}

export default AccountCont

export { NewAccount }
