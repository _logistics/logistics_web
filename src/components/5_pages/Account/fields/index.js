export const mainfields = {
  params: [
    { name: 'FIO', label: 'Фамилия Имя Отчество', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'mail', label: 'Почта', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'phone', label: 'Телефон', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 }, disabled: true },
    { name: 'oldPassword', label: 'Старый пароль', size: '100%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'password', label: 'Пароль', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'passwordConfirmation', label: 'Повторите пароль', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
  ]
}

export const orgFields = {
  params: [
    { name: 'contactPersonFio', label: 'Фамилия Имя Отчество', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'mail', label: 'Почта', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'phone', label: 'Телефон', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 }, disabled: true },
    { name: 'oldPassword', label: 'Старый пароль', size: '100%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'password', label: 'Пароль', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'passwordConfirmation', label: 'Повторите пароль', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
  ]
}

export const address = {
  params: [
    { name: 'floor', label: 'Этаж', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'apartment', label: 'Номер квартиры', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
  ]
}

export const requisites = {
  params: [
    { name: 'orgName', label: 'Название юр. лица', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'inn', label: 'ИНН', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'bik', label: 'БИК', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'rs', label: 'Рассчетный счет', size: '49%', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
  ]
}
