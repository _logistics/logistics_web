/* eslint-disable react-hooks/exhaustive-deps */
import { Button } from '@/components/1_atoms/Buttons'
import Center from '@/components/1_atoms/Center'
import { formBuilder, checkValidForm } from '@/components/0_functionals/builder'
import { useStore } from '@/stores/init'
import { curry } from 'ramda'
import React, { useState } from 'react'
import { openNotifi, randKey } from '@/utils/index'
import { Wrapper, SettingsCont, Buttons, ChoosePersonal } from './styles'
import { Radio, Select, Result } from 'antd';
import TwoButtons from '@/components/2_molecules/TwoButtons';

const mainfields = {
  params: [
    { name: 'phone', label: 'Телефон', size: '100%', nonNullable: true, type: 'text', length: { min: 5, max: 30 }, error: 'Небходимо указать телефон' },
    { name: 'password', label: 'Пароль', size: '100%', nonNullable: true, type: 'text', length: { min: 5, max: 30 }, error: 'Небходимо указать пароль' },
    { name: 'passwordConfirmation', label: 'Повторите пароль', size: '100%', nonNullable: true, type: 'text', length: { min: 5, max: 30 }, error: 'Небходимо повторить пароль' },
  ]
}

const roles = [
  {
    id: 2,
    slug: 'пользователь'
  },
  {
    id: 3,
    slug: 'администратор'
  },
  {
    id: 4,
    slug: 'логист'
  },
  {
    id: 5,
    slug: 'курьер'
  }
]

const NewAccount = () => {
  const [personals, cPersonals] = useState({});
  const [errors] = useState({});
  const { Option } = Select;
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }));
  const [isOrganization, setIsOrganization] = useState(false);
  const [profileIsCreated, setProfileIsCreated] = useState(false);
  const [userRoles, setUserRoles] = useState([]);
  const { account } = useStore({});
  const createUser = () => {
    const isValid = checkValidForm(personals, mainfields.params);
    if (userRoles.length === 0) {
      openNotifi('Ошибка', 'Выбирите хотя бы одну роль', 'error');
    }
    if (personals?.password?.length < 5) {
      openNotifi('Ошибка', 'Пароль должен быть более пяти символов', 'error');
    }
    if (personals.password !== personals.passwordConfirmation) {
      openNotifi('Ошибка', 'Пароли не совпадают', 'error');
    }
    if (personals?.phone?.length !== 10) {
      openNotifi('Ошибка', 'Телефон должен быть 10 символов', 'error');
    }
    if (isValid &&
      userRoles.length > 0 &&
      personals.password === personals.passwordConfirmation &&
      personals.phone.length === 10 &&
      personals.password.length > 5
    ) {
      const isUserCreated = !!account.register(
        personals.phone,
        personals.password,
        personals.passwordConfirmation,
        isOrganization,
        userRoles
      )
      setProfileIsCreated(isUserCreated);
    }
  }

  if (profileIsCreated) {
    return (
      <Wrapper>
        <Center>
          <Result
            status="success"
            title="Пользователь успешно создан"
            subTitle="УРА!"
            extra={[
              <TwoButtons
                key={randKey()}
                fText="К списку пользователей"
                sText="Создать нового"
                fClick={() => {
                  window.location.href = '/account/search';
                }}
                sClick={() => {
                  window.location.reload();
                }}
              />
            ]}
          />
        </Center>
      </Wrapper>
    )
  }

  return (
    <Wrapper>
      <SettingsCont>
        <Center>
          <h1>Создание нового пользователя</h1>
          <h3>Контакты представителя</h3>
          {formBuilder(mainfields, personals, cData, errors, '100%')}
          <ChoosePersonal>
            <h3>Выбрать лицо</h3>
            <Radio.Group
              className="mb3"
              onChange={(e) => {
                setIsOrganization(e.target.value)
              }}
              value={isOrganization}
            >
              <Radio value={false}>Физическое</Radio>
              <Radio value>Юридическое</Radio>
            </Radio.Group>
          </ChoosePersonal>
          <Select
            mode="tags"
            size="large"
            placeholder="Выбирете роли для пользователя"
            onChange={(e) => {
              const data = e.map((el) => +el);
              setUserRoles(data);
            }}
            style={{ width: '100%' }}
          >
            {roles.map((el) => (
              <Option key={el.id}>{el.slug}</Option>
            ))}
          </Select>
          <Buttons>
            <Button reversed onClick={createUser}>Сохранить</Button>
          </Buttons>
        </Center>
      </SettingsCont>
    </Wrapper>
  )
}

export default NewAccount
