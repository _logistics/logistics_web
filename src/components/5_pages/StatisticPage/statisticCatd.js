/* eslint-disable react/no-array-index-key */
/* eslint-disable no-case-declarations */
import React, { useState } from 'react'
import { CardWrapper } from './styles'

const StatisticCard = ({ postfix = '', prefix = '', options = [], title, value = '', values = {} }) => {
  const [currMode, setCurrMode] = useState('month');
  const count = values[currMode] ? values[currMode]?.count[0]?.count : '0';
  const currValue = value && value.count[0] ? `${prefix}${value.count[0].count}${postfix}` : `${prefix}0${postfix}`;
  return (
    <CardWrapper>
      { title ?
        (
          <h3>{title}</h3>
        ) : (
          <select
            onChange={(e) => {
              setCurrMode(e.target.value);
            }}
            style={{ marginBottom: '10px' }}
          >
            {options.map((el, idx) => (
              <option
                key={idx}
                value={el.value}
              >{`${prefix} ${el.text} ${postfix}`}
              </option>
            ))}
          </select>
        )}
      <p>{title ? currValue : count}</p>
    </CardWrapper>
  )
}

export default StatisticCard;
