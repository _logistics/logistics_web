/* eslint-disable no-case-declarations */
import React, { useEffect, useState } from 'react'
import Center from '@/components/1_atoms/Center'
import {
  Wrapper,
  GraphHeader,
  GraphWrapper,
  CardsColumn,
  GraphBody,
} from './styles'
import { ResponsiveBar } from '@nivo/bar'
import StatisticCard from './statisticCatd'
import { useStore } from '@/stores/init'
import { Result, Button } from 'antd'
import { isEmpty } from 'ramda'

const StatisticPage = () => {
  const { account } = useStore({})
  const [activeGraph, setActiveGraph] = useState('count')
  const [gpraphTypeDate, setGpraphTypeDate] = useState('month')
  const [statData, setStatData] = useState(null)
  const [updatingGraph, setUpdatingGraph] = useState(false)

  const logistOrAdmin =
    account.isAdmin() || account.isLogistician() || account.isSuper()
  const me = parseInt(account.myUser.id)
  const date = new Date()

  useEffect(() => {
    account
      .getStatisticData(
        gpraphTypeDate,
        logistOrAdmin ? null : me.id,
        date.getFullYear(),
        date.getMonth(),
      )
      .then(({ listOrdersForGraph: listOrdersForGraph }) => {
        console.log(listOrdersForGraph)
        if (
          listOrdersForGraph.reduce(
            (acc, { payedCount, notPayedCount, notDeliveredCount }) => {
              return acc + payedCount + notPayedCount + notDeliveredCount
            },
            0,
          ) > 0
        ) {
          setStatData(
            listOrdersForGraph.map(
              ({
                count,
                inserted,
                price,
                payedSum,
                payedCount,
                notPayedSum,
                notPayedCount,
                notDeliveredSum,
                notDeliveredCount,
              }) => {
                return {
                  inserted,
                  'количество оплаченых заказов': payedCount,
                  'количество не оплаченых заказов': notPayedCount,
                  'количество не доставленных заказов': notDeliveredCount,
                  'сумма оплаченых заказов': payedSum,
                  'сумма не оплаченых заказов': notPayedSum,
                  'сумма не доставленных заказов': notDeliveredSum,
                }
              },
            ),
          )
        } else {
          setStatData([])
        }
      })
  }, [account, gpraphTypeDate])
  const selectOptions = [
    {
      value: 'month',
      text: 'месяц',
    },
    {
      value: 'prev_month',
      text: 'месяц',
    },
    {
      value: 'year',
      text: 'год',
    },
    {
      value: 'prev_year',
      text: 'год',
    },
    {
      value: 'all_time',
      text: 'все время',
    },
  ]
  if (!statData) {
    return <p>Загрузка</p>
  }
  const mths = [
    'январь',
    'февраль',
    'март',
    'апрель',
    'май',
    'июнь',
    'июль',
    'август',
    'сентябрь',
    'октябрь',
    'ноябрь',
    'декабрь',
  ]
  const updateGraph = () => {
    setUpdatingGraph(true)
    setTimeout(() => {
      setUpdatingGraph(false)
    }, [50])
  }

  const Graph = () => {
    if (updatingGraph) return <p>loading</p>
    if (!statData || isEmpty(statData))
      return (
        <Result
          status="warning"
          title="Нет данных по заказах за указанный период"
          extra={''}
          style={{ marginTop: 100 }}
        />
      )
    return (
      <ResponsiveBar
        data={statData}
        margin={{ top: 50, right: 110, bottom: 100, left: 100 }}
        keys={
          activeGraph === 'count'
            ? [
                'количество оплаченых заказов',
                'количество не оплаченых заказов',
                'количество не доставленных заказов',
              ]
            : [
                'сумма оплаченых заказов',
                'сумма не оплаченых заказов',
                'сумма не доставленных заказов',
              ]
        }
        indexBy="inserted"
        borderColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 35,
          legendPosition: 'middle',
          legendOffset: 600,
        }}
        axisLeft={{
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend:
            activeGraph === 'count'
              ? 'количество заказов'
              : 'совокупная стоимость',
          legendPosition: 'middle',
          legendOffset: -90,
        }}
        labelSkipWidth={12}
        labelSkipHeight={12}
        labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        legends={[
          {
            dataFrom: 'keys',
            anchor: 'bottom',
            direction: 'row',
            justify: false,
            translateX: -99,
            translateY: 74,
            itemsSpacing: 2,
            itemWidth: 250,
            itemHeight: 20,
            itemDirection: 'left-to-right',
            itemOpacity: 0.85,
            symbolSize: 20,
            effects: [
              {
                on: 'hover',
                style: {
                  itemOpacity: 1,
                },
              },
            ],
          },
        ]}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
      />
    )
  }
  if (!account.user.profile) {
    return (
      <Result
        status="404"
        title="Заполните профиль"
        subTitle="Вы не заполнили профиль пользователя. Для просмотра истории заказов заполните профиль"
        extra={
          <Button
            onClick={() => {
              window.location.href = '/account'
            }}
            type="primary"
          >
            Перейти в профиль
          </Button>
        }
      />
    )
  }
  if (account.isCourier() && !account.isSuperLgst()) {
    window.location.href = 'orders-history'
  }
  return (
    <Center>
      <Wrapper>
        <GraphWrapper>
          <GraphHeader>
            <div className="mod">
              <span
                className={activeGraph === 'count' ? 'active' : ''}
                style={{ marginRight: '20px' }}
                onClick={() => {
                  updateGraph()
                  setActiveGraph('count')
                }}
              >
                Количество заказов
              </span>
              <span
                className={activeGraph === 'summ' ? 'active' : ''}
                onClick={() => {
                  updateGraph()
                  setActiveGraph('summ')
                }}
              >
                Совокупная стоимость
              </span>
            </div>
            <select
              value={gpraphTypeDate}
              onChange={(e) => {
                updateGraph()
                setGpraphTypeDate(e.target.value)
              }}
            >
              <option value="month">за текущий месяц</option>
              <option value="prev_month">за предыдущий месяц</option>
              <option value="year">за год</option>
              <option value="prev_year">за предыдущий год</option>
              <option value="all_time">за все время</option>
            </select>
          </GraphHeader>
          <GraphBody>{Graph()}</GraphBody>
        </GraphWrapper>
      </Wrapper>
      <span>
        <i>* Оплаченными считаются заказы со статусом - Заказ оплачен</i>
        <br />
        <i>
          * Не оплаченными считаются заказы со статусом - Заказ не оплачен,
          Заказ доставлен
        </i>
        <br />
        <i>
          * Не доставленными считаются заказы со статусом - Заказ не доставлен,
          Есть потери, Заказ отменен, Возврат товара
        </i>
      </span>
    </Center>
  )
}

export default StatisticPage
