import styled from 'styled-components';
import { lighten } from 'polished'

/* eslint-disable import/prefer-default-export */
export const Wrapper = styled.div`
  display: flex;
  padding-top: 20px;
  @media only screen and (max-width: 1200px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const GraphWrapper = styled.div`
  max-width: 1024px;
  height: 600px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border-right: 1px solid ${lighten(0.4, 'gray')};
  padding-right: 20px;
  margin-bottom: 150px;
`;

export const GraphBody = styled.div`
  height: 700px;
  width: 1000px;
  @media only screen and (max-height: 900px) {
    height: 600px;
  }
  @media only screen and (max-width: 1500px) {
    width: 800px;
  }
  @media only screen and (max-width: 1400px) {
    width: 600px;
  }
  @media only screen and (max-width: 1200px) {
    border: none;
  }
  @media only screen and (max-width: 800px) {
    width: 500px;
  }
  @media only screen and (max-width: 700px) {
    width: 400px;
  }
  @media only screen and (max-width: 400px) {
    width: 320px;
  }
`;

export const GraphHeader = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  & .mod span {
    cursor: pointer;
    font-weight: 600;
    font-size: 14px;
    color: #8b8b7e;
    transition: all .3s;
  }
  & .mod span.active {
    cursor: pointer;
    color: #141413;
  }
  & .mod span:hover {
    color: #1f1f15
  }
  & select {
    border: none;
    background-color: transparent;
    cursor: pointer;
    padding: 10px;
    border-radius: 9px;
  }
  @media only screen and (max-width: 700px) {
    & .mod span {
    cursor: pointer;
    font-size: 12px;
  }
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;

export const CardWrapper = styled.div`
  max-width: 300px;
  width: 280px;
  height: 100px;
  display: flex;
  align-items: center;
  flex-direction: column;
  justify-content: center;
  border-bottom: 1px solid ${lighten(0.4, 'gray')};
  & select, h3 {
    border: none;
    background-color: white;
    font-size: 16px;
    font-weight: 700;
    cursor: pointer;
    color: gray;
  }
  & p {
    font-size: 18px;
    font-weight: 600;
  }
  @media only screen and (max-width: 800px) {
    margin: 5px;
    & select, h3 {
      font-size: 14px;
    }
    & p {
      font-size: 15px;
    }
  }
`;

export const CardsColumn = styled.div`
  @media only screen and (max-width: 1200px) {
      margin-left: 20px;
      display: flex;
      flex-wrap: wrap;
      width: 100%;
      justify-content: center;
    }
`;
