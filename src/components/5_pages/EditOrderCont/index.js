/* eslint-disable prefer-template */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from 'react'
import { Select, Button, Popconfirm, Radio, Input, Tooltip } from 'antd'
import { formBuilder } from '@/components/0_functionals/builder'
import { curry, path } from 'ramda'
import Center from '@/components/1_atoms/Center'
import { useStore } from '@/stores/init'
import SelectPackages from '../CreateOrder/SelectPackages'
import {
  receiverFields,
  senderFields,
  orderFields,
} from '../CreateOrder/formFields'
import { Form, ButtonWrapper } from '../CreateOrder/styles'
import { useRouter } from 'next/router'
import MainDatePicker from '@/components/2_molecules/DatePicker'
import { randKey } from '@/utils/index'
import { AddressSuggestions } from '@/components/2_molecules/Dadata/AddressSuggestions'
import { listOrderStatuses } from './data'
import { masks } from '@dev-bi/rct_utils'

const EditOrderCont = () => {
  const { Option } = Select
  const { creatingOrder, account } = useStore({})
  const [currOrder, updateCurrentOrder] = useState(null)
  const cOrderData = curry((name, val) =>
    updateCurrentOrder({ ...currOrder, [name]: val }),
  )
  const [orderStatus, setOrderStatus] = useState(null)
  const [courierList, setСourierList] = useState([])
  const router = useRouter()
  const [listPaymentsTypes, setListPaymentsTypes] = useState([])
  const iCanEdit = () => {
    if (account.isSuperLgst()) {
      return true
    }
    if (account.isLogistician()) {
      return false
    }
    const status = path(['orderStatus', 'name'], currOrder)
    if (!status || status === 'Ожидает подтверждения') {
      return true
    }
    return false
  }

  const courierBtn = () => {
    if (currOrder?.orderStatus?.id === 3) {
      return (
        <Button
          style={{ marginRight: '20px' }}
          size="large"
          type="primary"
          onClick={() => {
            creatingOrder.changeOrderStatus(currOrder.id, 4)
          }}
        >
          Принять в доставку
        </Button>
      )
    }
    if (currOrder?.orderStatus?.id === 4) {
      return (
        <Button
          style={{ marginRight: '20px' }}
          size="large"
          type="primary"
          onClick={() => {
            creatingOrder.changeOrderStatus(currOrder.id, 7)
          }}
        >
          Заказ доставлен
        </Button>
      )
    }
  }
  useEffect(() => {
    getListPaymentTypes()
    if (account.isSuperLgst()) {
      account.getCouriers().then((el) => {
        const couriers = el.couriers.entries
          ?.map((el) => {
            if (el.profile && el.id) {
              const { profile } = el
              const firstName = profile.firstName || ''
              const middleName = profile.middleName || ''
              const lastName = profile.lastName || ''
              return {
                id: el.id,
                name: firstName + ' ' + middleName + ' ' + lastName,
              }
            }
            return null
          })
          .filter((el) => el)
        setСourierList(couriers)
      })
    }
    creatingOrder.getOrder(router.query.index).then((el) => {
      updateCurrentOrder(el.getLastIncompleted)
      if (el.getOrder) {
        const profile = el.getOrder[0].courier?.userProfile
        let name = null
        if (profile) {
          const firstName = profile.firstName || ''
          const middleName = profile.middleName || ''
          const lastName = profile.lastName || ''
          name = `${firstName} ${middleName} ${lastName}`
        }
        el.getOrder[0].courierId = +el.getOrder[0].courier?.id
        el.getOrder[0].courierName = name
        const dt = new Date(el.getOrder[0].deliveryAwaitedDatetime)
        Date.prototype.addHours = function (h) {
          this.setTime(this.getTime() + h * 60 * 60 * 1000)
          return this
        }
        el.getOrder[0].deliveryAwaitedDatetime = dt.addHours(3)

        updateCurrentOrder({
          ...el.getOrder[0],
          paymentType: el.getOrder[0]?.paymentType?.name,
        })
      } else {
        updateCurrentOrder({
          products: [],
        })
      }
    })
  }, [])

  const getListPaymentTypes = () => {
    creatingOrder.listPaymentTypes().then((el) => {
      setListPaymentsTypes(el.listPaymentTypes)
    })
  }
  if (!currOrder || !courierList) {
    return <p>Загрузка</p>
  }
  const OrderSettingsForm = () => {
    if (account.isSuperLgst()) {
      return (
        <Form>
          <h2>Настройка заказа</h2>
          {currOrder?.insurance && (
            <p
              style={{ marginBottom: 15, color: 'gray' }}
            >{`Заказ ${currOrder.insurance}`}</p>
          )}
          {formBuilder(orderFields, currOrder, cOrderData, {})}
          <Select
            defaultValue={currOrder.orderStatus?.name}
            size="large"
            onChange={(e) => {
              setOrderStatus(e)
            }}
            style={{ width: '100%', marginBottom: '20px' }}
            placeholder="Выберите статус"
          >
            {listOrderStatuses.map((el) => (
              <Option key={randKey()} value={el.id}>
                {el.name}
              </Option>
            ))}
          </Select>
          <Select
            defaultValue={currOrder.courierName}
            size="large"
            onChange={(courierId) => cOrderData('courierId')(+courierId)}
            style={{ width: '100%' }}
            placeholder="Выберете курьера"
          >
            {courierList.map((el) => (
              <Option key={randKey()} value={el.id}>
                {el.name}
              </Option>
            ))}
          </Select>
        </Form>
      )
    }
  }

  const deleteOrder = () => {
    creatingOrder.deleteOrder(currOrder.id).then((el) => {
      if (el) {
        setTimeout(() => {
          window.location.href = '/orders-history'
        }, 300)
      }
    })
  }

  const canToRelog = !!(
    currOrder &&
    currOrder.price &&
    currOrder.deliveryAwaitedDatetime &&
    currOrder.receiverAddress &&
    currOrder.receiverPhone &&
    currOrder.receiverTown &&
    currOrder.senderAddress &&
    currOrder.senderFio &&
    currOrder.senderPhone
  )

  return (
    <>
      <Center>
        <div>
          {currOrder?.orderStatus === null && account.isCourier() && (
            <div
              style={{ color: 'darkred', fontSize: 22, textAlign: 'center' }}
            >
              Форма недозаполнена обратитесь к логисту
            </div>
          )}

          <SelectPackages
            nextStep={null}
            updateCurrentOrder={updateCurrentOrder}
            currOrder={currOrder}
            updatePackage={() => {}}
            disabled={!iCanEdit() || account.isCourier()}
            disabledNextButton
          />
          {!account.isCourier() && OrderSettingsForm()}
          <Form>
            <h2>Отправитель</h2>
            <AddressSuggestions
              onChange={(e) => {
                cOrderData('senderAddress')(e)
              }}
              value={currOrder?.senderAddress || ''}
              token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
              defaultQuery={currOrder?.senderAddress || ''}
            />
            {formBuilder(
              senderFields,
              currOrder,
              cOrderData,
              {},
              null,
              !iCanEdit(),
            )}
            <h3>Тип оплаты</h3>
            <Radio.Group
              onChange={(e) => {
                cOrderData('paymentType')(e.target.value)
              }}
              value={currOrder.paymentType}
              style={{ marginBottom: 20 }}
            >
              {listPaymentsTypes.map((el, idx) => (
                <Radio key={`item-${idx * 1.1}`} value={el.name}>
                  {el.name}
                </Radio>
              ))}
            </Radio.Group>
            {currOrder.paymentType === 'Наличный расчет' ? (
              <>
                <h3>Сумма сдачи</h3>
                <Input
                  style={{ marginBottom: 20 }}
                  value={currOrder.oddMoney}
                  onChange={(e) =>
                    cOrderData('oddMoney')(masks.wrapNumMsk(e.target.value))
                  }
                  placeholder="Сумма сдачи"
                />
              </>
            ) : null}
            <h3>Кто платит</h3>
            <Radio.Group
              onChange={(e) => {
                cOrderData('payer')(e.target.value)
              }}
              value={currOrder.payer}
            >
              <Radio value="Получатель">Получатель</Radio>
              <Radio value="Отравитель">Отравитель</Radio>
            </Radio.Group>
          </Form>
          <Form>
            <h2>Получатель</h2>
            <AddressSuggestions
              onChange={(e) => {
                cOrderData('receiverAddress')(e)
              }}
              defaultQuery={currOrder?.receiverAddress || ''}
              value={currOrder?.receiverAddress || ''}
              token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
            />
            {formBuilder(
              receiverFields,
              currOrder,
              cOrderData,
              {},
              null,
              !iCanEdit(),
            )}
            <MainDatePicker
              showTimeSelect
              minDate={new Date()}
              selected={currOrder.deliveryAwaitedDatetime}
              placeholderText="Дата доставки"
              onChange={(e) => cOrderData('deliveryAwaitedDatetime')(e)}
              isClearable
              disabled={!iCanEdit()}
            />
          </Form>
          <ButtonWrapper>
            {account.isSuperLgst() || account.isClient() ? (
              <Button
                type="primary"
                onClick={() => {
                  const newOrder = currOrder
                  newOrder.price = +currOrder.price
                  const paymentTypeId =
                    listPaymentsTypes.find(
                      (el) => el.name === currOrder.paymentType,
                    )?.id || null
                  creatingOrder.updateOrder(
                    { ...newOrder, paymentTypeId },
                    orderStatus,
                  )
                }}
                style={{ marginRight: '20px', marginBottom: '20px' }}
                size="large"
                className="bt_button"
                disabled={!iCanEdit()}
              >
                Обновить
              </Button>
            ) : account.isCourier() ? (
              courierBtn()
            ) : null}
            {account.isSuperLgst() ? (
              <>
                <Button
                  onClick={() => {
                    creatingOrder.oneCpush(currOrder.id)
                  }}
                  style={{ marginRight: '20px', marginBottom: '20px' }}
                  size="large"
                  className="bt_button"
                >
                  Отправить в 1С
                </Button>
                <Tooltip
                  title={
                    !canToRelog
                      ? 'Для того чтобы заказ можно было отправит в релог должны быть заполнены следующие поля: цена, дата доставки, адрес получателя, ФИО получателя, телефон получателя, адрес отправителя, ФИО отрпавителя, телефон отравителя'
                      : ''
                  }
                >
                  <Button
                    style={{ marginRight: '20px' }}
                    size="large"
                    className="bt_button"
                    onClick={() => {
                      creatingOrder.relogSendOrder(currOrder.id)
                    }}
                    disabled={!canToRelog}
                  >
                    Отправить в релог
                  </Button>
                </Tooltip>
              </>
            ) : null}
            <Button
              onClick={() => {
                window.location.href = '/orders-history'
              }}
              className="bt_button"
              style={{ marginRight: '20px' }}
              size="large"
            >
              К списку заказов
            </Button>
            <Button
              onClick={() => {
                window.location.href = `/nakladnaya/${router?.query?.index}`
              }}
              className="bt_button"
              style={{ marginRight: '20px' }}
              size="large"
              type="dashed"
            >
              К накладной
            </Button>
            {!account.isCourier() && (
              <Popconfirm
                title="Вы уверены что хотите удалить этот заказ?"
                onConfirm={deleteOrder}
                okText="Да"
                cancelText="Нет"
              >
                <Button
                  className="bt_button"
                  style={{ marginRight: '20px' }}
                  size="large"
                  danger
                  type="primary"
                >
                  Удалить заказ
                </Button>
              </Popconfirm>
            )}
          </ButtonWrapper>
        </div>
      </Center>
    </>
  )
}

export default EditOrderCont
