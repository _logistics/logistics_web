/* eslint-disable import/prefer-default-export */
export const listOrderStatuses = [
  {
    id: 1,
    name: 'Ожидает подтверждения',
    key: 1
  },
  {
    id: 2,
    name: 'Подтвержден',
    key: 2
  },
  {
    id: 3,
    name: 'Заказ в работе',
    key: 3
  },
  {
    id: 4,
    name: 'Заказ в пути',
    key: 4
  },
  {
    id: 5,
    name: 'Заказ оплачен',
    key: 5
  },
  {
    id: 6,
    name: 'Заказ не оплачен',
    key: 6
  },
  {
    id: 7,
    name: 'Заказ доставлен',
    key: 7
  },
  {
    id: 8,
    name: 'Заказ не доставлен',
    key: 8
  },
  {
    id: 9,
    name: 'Есть потери',
    key: 9
  },
  {
    id: 10,
    name: 'Заказ отменен',
    key: 10
  },
  {
    id: 11,
    name: 'Возврат товара',
    key: 11
  }
];
