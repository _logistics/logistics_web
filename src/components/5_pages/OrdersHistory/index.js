import React, { useState, useEffect } from 'react'
import {
  Table,
  Button,
  Space,
  DatePicker,
  Tooltip,
  Collapse,
  Upload,
} from 'antd'
import { useStore } from '@/stores/init'
import { getArrayOfValuesByKey } from 'core_js_devbi'
import { Wrapper } from './styles'
import { LinkOutlined, InboxOutlined } from '@ant-design/icons'
import { openNotifi, CacheStore } from '@/utils/index'
import { GRAPHQL_ENDPOINT } from '@/config/index'
import { last, pipe, split, cond, T, equals } from 'ramda'
import { always } from 'ramda'
import Link from 'next/link'

const { Panel } = Collapse
const { Dragger } = Upload

const uploadOrders = async (b64, user) => {
  const token = CacheStore.get('token')
  const response = await fetch('http://lk.tempkurier.ru:4000/api/file_reader', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      sitetoken: 'Bearer sjdfgeuw3r783r2gqfdebsiurfhew9875y98435rh98hefnduh',
      authorization: `Bearer ${token}`,
    },
    body: JSON.stringify({
      b64,
      user,
    }), // body data type must match "Content-Type" header
  })
  return response.json() // parses JSON response into native JavaScript objects
}

const OrdersHistoryCont = () => {
  const { creatingOrder, account } = useStore({})
  const [ordersData, setOrdersData] = useState([])
  const [fileList, setFileList] = useState(null)
  const [page, setPage] = useState(1)
  const [me, setMe] = useState(null)
  const size = 10
  const userId = account.user.id
  const [dateStart, setDateStart] = useState(null)
  const [deteEnd, setDeteEnd] = useState(null)
  const [reportLoading, setReportLoading] = useState(false)
  const domain = GRAPHQL_ENDPOINT.split('/help')[0].split('/api')[0]
  Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + h * 60 * 60 * 1000)
    return this
  }

  const courier = account.isCourier()

  const getListRequest = (rolesId) => {
    if (rolesId) {
      switch (true) {
        case rolesId.includes(1):
          return creatingOrder
            .getListOrdersFull()
            .then((el) => setOrdersData(el.listOrders.entries))
        case rolesId.includes(3):
          return creatingOrder
            .getListOrdersFull()
            .then((el) => setOrdersData(el.listOrders.entries))
        case rolesId.includes(4):
          return creatingOrder
            .getListOrdersFull()
            .then((el) => setOrdersData(el.listOrders.entries))
        case rolesId.includes(5):
          return creatingOrder
            .getListOrdersCourier(page, size, userId)
            .then((el) => setOrdersData(el.listOrdersCourier.entries))
        case rolesId.includes(2):
          return creatingOrder
            .getListOrdersSender(page, size, userId)
            .then((el) => setOrdersData(el.listOrdersSender.entries))
        default:
          return null
      }
    }
    return []
  }

  const uploadProps = {
    accept: '.csv',
    onRemove: () => {
      setFileList([])
    },
    beforeUpload: (file) => {
      const fileType = pipe(split('.'), last)(file.name)
      return cond([
        [
          equals('csv'),
          () => {
            openNotifi(
              'Успех',
              'Файл загружен браузером и готов к отправке на сервер',
              'success',
            )
            setFileList([file])
            return true
          },
        ],
        [
          T,
          () => {
            openNotifi(
              'Неудача',
              'Вы можете загружать только .csv файлы',
              'error',
            )
            return Upload.LIST_IGNORE
          },
        ],
      ])(fileType)
    },
    customRequest: () => {
      return true
    },
    onChange: (file) => {
      if (fileList.length > 0) return
      const list = fileList
      const index = list.indexOf(file)
      const newFileList = list.slice()
      newFileList.splice(index, 1)
      setFileList(newFileList)
    },
    fileList,
    loading: false,
    multiple: false,
  }

  useEffect(() => {
    account.me().then((el) => {
      setMe(el?.me)
    })
  }, [account])

  useEffect(() => {
    const rolesId = getArrayOfValuesByKey(account.user.roles, 'id')
    getListRequest(rolesId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [page])

  const parseSenderRecord = (record) => {
    const senderFio = record.senderFio
    const userProfile = record.sender?.userProfile || null
    const orgProfile = record.sender?.orgProfile || null
    if (senderFio && userProfile) {
      const { firstName, lastName, middleName } = userProfile
      return `${firstName || ''} ${middleName || ''} ${lastName || ''}` + ''
    } else if (senderFio && senderFio !== '' && orgProfile) {
      const { orgName, contactPersonFio } = orgProfile
      return `${orgName} ${contactPersonFio && `(${contactPersonFio})`}`
    } else if (senderFio) {
      return senderFio
    } else if (userProfile) {
      const { firstName, lastName, middleName } = userProfile
      return `${firstName || ''} ${middleName || ''} ${lastName || ''}`
    } else if (orgProfile) {
      const { orgName, contactPersonFio } = orgProfile
      return `${orgName} ${contactPersonFio && `(${contactPersonFio})`}`
    }
    return ''
  }

  const parseCourierRecord = (record) => {
    const userProfile = record.courier?.userProfile || null
    const orgProfile = record.courier?.orgProfile || null
    if (userProfile) {
      const { firstName, lastName, middleName } = userProfile
      return `${firstName || ''} ${middleName || ''} ${lastName || ''}`
    } else if (orgProfile) {
      const { orgName, contactPersonFio } = orgProfile
      return `${orgName} ${contactPersonFio && `(${contactPersonFio})`}`
    }
    return ''
  }

  const colums = [
    {
      title: 'Номер заказа',
      dataIndex: 'id',
      key: 'id',
      width: '8%',
      align: 'center',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: 'Статус заказа',
      dataIndex: 'status',
      key: 'status',
      render: (text, record) => {
        const rcdName = record.orderStatus?.name || ''
        const clr = cond([
          [equals('Подтвержден'), always('#64b5f6')],
          [equals('Заказ доставлен'), always('#dce775')],
          [equals('Заказ оплачен'), always('#81c784')],
          [equals('Заказ в работе'), always('#a1887f')],
          [equals('Заказ в пути'), always('#a1887f')],
          [equals('Ожидает подтверждения'), always('#ffb74d')],
          [T, always('red')],
        ])(rcdName)
        return <span style={{ color: clr }}>{rcdName}</span>
      },
      sorter: (a, b) => a.orderStatus?.id - b.orderStatus?.id,
      filters: [
        { text: 'Заказ доставлен', value: 'Заказ доставлен' },
        { text: 'Заказ оплачен', value: 'Заказ оплачен' },
        { text: 'Подтвержден', value: 'Подтвержден' },
        { text: 'Заказ в работе', value: 'Заказ в работе' },
        { text: 'Заказ не оплачен', value: 'Заказ не оплачен' },
        { text: 'Заказ в пути', value: 'Заказ в пути' },
        { text: 'Ожидает подтверждения', value: 'Ожидает подтверждения' },
      ],
      onFilter: (value, record) => record.orderStatus?.name === value,
    },
    {
      title: 'Отправитель',
      dataIndex: 'sender',
      key: 'sender',
      render: (text, record) => parseSenderRecord(record),
      sorter: (a, b) =>
        parseSenderRecord(a).localeCompare(parseSenderRecord(b)),
    },
    {
      title: 'Курьер',
      dataIndex: 'age',
      key: 'age',
      render: (text, record) => parseCourierRecord(record),
      sorter: (a, b) =>
        parseCourierRecord(a).localeCompare(parseCourierRecord(b)),
    },
    {
      title: 'Место доставки',
      dataIndex: 'receiverAddress',
      key: 'receiverAddress',
    },
    {
      title: 'Дата доставки',
      dataIndex: 'deliveryAwaitedDatetime',
      key: 'deliveryAwaitedDatetime',
      render: (text, record) => {
        const dt = record?.deliveryAwaitedDatetime
        return `${
          (dt &&
            new Date(dt).addHours(3).toLocaleString('ru-RU').slice(0, -3)) ||
          ''
        }`
      },
      sorter: (a, b) =>
        new Date(a.deliveryAwaitedDatetime) -
        new Date(b.deliveryAwaitedDatetime),
    },
    {
      title: 'Доставлено',
      dataIndex: 'isDelivered',
      align: 'center',
      sorter: (a, b) => {
        const isA = a.orderStatus?.id > 4,
          isB = b.orderStatus?.id > 4
        return isA === isB ? 0 : isA ? -1 : 1
      },
      render: (text, record) => {
        if (record.orderStatus?.id > 4)
          return (
            <span
              style={{
                textAlign: 'center',
                fontSize: '20px',
                fontWeight: '900',
              }}
            >
              +
            </span>
          )
        return ''
      },
    },
    {
      title: 'Сумма',
      dataIndex: 'price',
      key: 'price',
      fixed: 'right',
      sorter: (a, b) => a.price - b.price,
    },
    {
      title: 'Перейти',
      dataIndex: 'inv',
      key: 'inv',
      render: (text, record) =>
        record?.id ? (
          <>
            <Link href={`/nakladnaya/${record?.id}`}>накладная</Link>
            <br />
            <Link href={`/order/${record?.id}`}>к заказу</Link>
          </>
        ) : (
          ''
        ),
    },
  ]

  const getDataLink = () => {
    setReportLoading(true)
    creatingOrder.makeUserDocs(userId, dateStart, deteEnd).then((el) => {
      const excel = el && el.makeUserDocs.excel
      window.open(`${domain}/${excel}`, '_blank')
      openNotifi('Успех', 'Файл успешно создан', 'success')
      setReportLoading(false)
    })
  }

  const handleUpload = () => {
    if (fileList) {
      const reader = new FileReader()
      reader.readAsDataURL(fileList[0])
      reader.onload = () => {
        pipe(split('data:application/octet-stream;base64,'), last, (x) => {
          uploadOrders(x, me).then((el) => {
            if (el?.res === '[]') {
              openNotifi('Ошибка', 'Не удалось распознать заказ', 'error')
            } else {
              openNotifi('Успех', 'Заказ(ы) успешно создан', 'success')
              const rolesId = getArrayOfValuesByKey(account.user.roles, 'id')
              getListRequest(rolesId)
            }
          })
        })(reader.result)
      }
      return true
    }
  }
  return (
    <Wrapper>
      <Space style={{ margin: 20 }} size="middle">
        <DatePicker
          placeholder="Дата начала"
          value={dateStart}
          showTime
          onChange={(v) => {
            setDateStart(v)
          }}
          onOk={() => {}}
        />
        <DatePicker
          placeholder="Дата начала"
          value={deteEnd}
          showTime
          onChange={(v) => {
            setDeteEnd(v)
          }}
          onOk={() => {}}
        />
        <Tooltip
          title={
            !dateStart || !deteEnd
              ? 'Необходимо заполнить дату начала и дату конца'
              : !ordersData || ordersData.length === 0
              ? 'Нет данных'
              : ''
          }
        >
          <Button
            loading={reportLoading}
            disabled={
              !dateStart || !deteEnd || !ordersData || ordersData.length === 0
            }
            type="primary"
            onClick={getDataLink}
            icon={<LinkOutlined />}
          >
            Выгрузить данные за период
          </Button>
        </Tooltip>
      </Space>
      {!courier && (
        <Collapse style={{ margin: 20 }}>
          <Panel header="Заполнить автоматичски" key="1">
            <Dragger fileList={fileList} {...uploadProps}>
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">
                Нажимите или наведите файл, чтобы его загрузить
              </p>
              <p className="ant-upload-hint">Поддерживает только один файл</p>
            </Dragger>
            <Button
              style={{ margin: 20 }}
              type="primary"
              onClick={handleUpload}
            >
              Загрузить
            </Button>
          </Panel>
        </Collapse>
      )}

      <Table
        dataSource={ordersData}
        columns={colums}
        size={size}
        pagination={{ pageSize: 10 }}
        style={{ maxWidth: 'calc(100% - 20px)' }}
        scroll={{ x: 'calc(700px + 50%)' }}
      />
      {(getArrayOfValuesByKey(account.user.roles, 'id') === 2 ||
        getArrayOfValuesByKey(account.user.roles, 'id') === 5) && (
        <div>
          <Button
            onClick={() => {
              page > 1 && setPage(page - 1)
            }}
            disabled={page < 2}
          >
            Назад
          </Button>
          <Button
            onClick={() => {
              setPage(page + 1)
            }}
          >
            Далее
          </Button>
        </div>
      )}
    </Wrapper>
  )
}

export default OrdersHistoryCont
