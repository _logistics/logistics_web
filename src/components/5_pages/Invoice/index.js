/* eslint-disable new-cap */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
/* eslint-disable react/no-this-in-sfc */
/* eslint-disable no-extend-native */
import React, { useEffect, useState } from 'react'
import T from 'prop-types'
import { Wrapper, FlexWrapper } from './styles'
import { useRouter } from 'next/router'
import { useStore } from '@/stores/init'
import InvoiceHeader from './components/InvoiceHeader'
import TKLogo from './components/TKLogo'
import TKDate from './components/TKDate'
import TKText from './components/TKText'
import InvoiceBody from './components/InvoiceBody'
import InvoiceBodyLeft from './components/InvoiceBodyLeft'
import InvoiceBodyRight from './components/InvoiceBodyRight'
import InvoiceSender from './components/InvoiceSender'
import InvoiceReceiver from './components/InvoiceReceiver'
import ConfirmationSendage from './components/ConfirmationSendage'
import InvoiceContacts from './components/InvoiceContacts'
import ConfirmationReceiving from './components/ConfirmationReceiving'
import SendageInfo from './components/SendageInfo'
import Insurance from './components/Insurance'
import SendageInformation from './components/SendageInformation'
import { Button } from 'antd'
import jsPDF from 'jspdf'
import html2canvas from 'html2canvas'
import { cond, T as TRUE } from 'ramda'
// import { Router } from 'express'

const Invoice = ({ errorCode, page, target }) => {
  const {
    query: { index },
  } = useRouter()
  const router = useRouter()
  const [currOrder, updateCurrentOrder] = useState(null)
  const [listPaymentsTypes, setListPaymentsTypes] = useState([])
  const [redirect, changeRedirect] = useState(true)
  const { creatingOrder, account } = useStore({})

  const getListPaymentTypes = () => {
    creatingOrder.listPaymentTypes().then((el) => {
      setListPaymentsTypes(el.listPaymentTypes)
    })
  }
  useEffect(() => {
    getListPaymentTypes()
    creatingOrder.getOrder(index).then((el) => {
      updateCurrentOrder(el.getLastIncompleted)
      if (el.getOrder) {
        const profile = el.getOrder[0].courier?.userProfile
        let name = null
        if (profile) {
          const firstName = profile.firstName || ''
          const middleName = profile.middleName || ''
          const lastName = profile.lastName || ''
          name = `${firstName} ${middleName} ${lastName}`
        }
        el.getOrder[0].courierId = +el.getOrder[0].courier?.id
        el.getOrder[0].courierName = name
        const dt = new Date(el.getOrder[0].deliveryAwaitedDatetime)
        Date.prototype.addHours = function (h) {
          this.setTime(this.getTime() + h * 60 * 60 * 1000)
          return this
        }
        el.getOrder[0].deliveryAwaitedDatetime = dt.addHours(3)

        updateCurrentOrder({
          ...el.getOrder[0],
          paymentType: el.getOrder[0]?.paymentType?.name,
        })
      } else {
        updateCurrentOrder({
          products: [],
        })
      }
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  if (redirect && account.isSuperLgst()) {
    changeRedirect(false)
  }
  if (redirect && account.isLogistician()) {
    changeRedirect(false)
  }
  let sender_id = currOrder?.sender?.id
  let account_id = account?.myUser?.id
  if (
    redirect &&
    !!(sender_id && account_id && `${sender_id}` === `${account_id}`)
  ) {
    changeRedirect(false)
  }

  const print = (scale = 1) => {
    const width = document.body.offsetWidth
    const diff = (1903 - document.body.offsetWidth) / 90
    const diff2 = (1400 - document.body.offsetWidth) / 120

    const x = cond([
      [() => width > 1400, () => 18 - diff],
      [() => width > 1000, () => 12 - diff2],
      [TRUE, () => 14 - diff2],
    ])(width)

    const y = cond([
      [() => width > 1400, () => 9],
      [TRUE, () => 16],
    ])(width)

    const input = document.getElementById('fileToPrint')
    html2canvas(input, {
      windowWidth: 1920,
      windowHeight: 1080,
      width: 1920,
      height: 1080,
      scale,
    }).then((canvas) => {
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jsPDF({
        orientation: 'landscape',
        unit: 'in',
        format: [x, y],
      })
      pdf.addImage(imgData, 'JPEG', 0.2, 0.2)
      // pdf.output('dataurlnewwindow');
      pdf.save(`накладная-${index}.pdf`)
    })
  }

  const printHalf = () => {
    const input = document.getElementById('fileToPrint')
    document.body.style.width = '1920px'
    html2canvas(input, {
      windowWidth: 1920,
      windowHeight: 1080,
      width: 1920,
      height: 1080
    }).then((canvas) => {
      const imgData = canvas.toDataURL('image/png')
      const pdf = new jsPDF({
        orientation: 'portrait',
        unit: 'mm',
        format: 'a4',
      })
      pdf.addImage(imgData, 'JPEG', 25, 20, 180, 95)
      // pdf.output('dataurlnewwindow');
      pdf.save(`накладная-${index}.pdf`)
    })
  }
  return (
    <>
      <Button
        style={{ width: 'calc(50% - 20px)', margin: '0 10px' }}
        type="primary"
        ghost
        onClick={() => print(1)}
      >
        Печать (горизонтально)
      </Button>
      <Button
        style={{ width: 'calc(50% - 20px)', margin: '0 10px' }}
        type="primary"
        ghost
        onClick={() => printHalf()}
      >
        Печать (верт. лист)
      </Button>
      <Wrapper id="fileToPrint">
        <InvoiceHeader>
          <FlexWrapper>
            <TKLogo />
            <TKText />
            <TKDate />
          </FlexWrapper>
        </InvoiceHeader>
        <InvoiceBody>
          <InvoiceBodyLeft>
            <InvoiceSender
              senderFio={currOrder?.senderFio}
              senderAddress={currOrder?.senderAddress}
              senderTown={currOrder?.senderTown}
              senderState={currOrder?.senderState}
              senderPhone={currOrder?.senderPhone}
              senderArea={currOrder?.senderArea}
              company={currOrder?.isOrganization ? '+' : '-'}
            />
            <InvoiceReceiver
              receiverFio={currOrder?.receiverFio}
              receiverAddress={currOrder?.receiverAddress}
              receiverTown={currOrder?.receiverTown}
              receiverState={currOrder?.receiverState}
              receiverPhone={currOrder?.receiverPhone}
              receiverArea={currOrder?.receiverArea}
              company={currOrder?.isOrganization ? '+' : '-'}
              comment={`${currOrder?.senderComment} ${currOrder?.receiverComment}`}
            />
            <ConfirmationSendage>Прием отправления</ConfirmationSendage>
          </InvoiceBodyLeft>
          <InvoiceBodyRight>
            <InvoiceContacts />
            <SendageInfo
              clentId={currOrder?.sender?.id}
              paymentType={currOrder?.paymentType}
            >
              Информация об оплате
            </SendageInfo>
            <Insurance insurance={!!currOrder?.insurance}>
              Страхование
            </Insurance>
            <SendageInformation products={currOrder?.products}>
              Описание отправления
            </SendageInformation>
            <ConfirmationReceiving>
              Подтверждение доставки
            </ConfirmationReceiving>
          </InvoiceBodyRight>
        </InvoiceBody>
      </Wrapper>
    </>
  )
}

Invoice.propTypes = {
  errorCode: T.number,
  page: T.string,
  target: T.string,
}

Invoice.defaultProps = {
  errorCode: 404,
  page: '',
  target: '',
}

export default React.memo(Invoice)
