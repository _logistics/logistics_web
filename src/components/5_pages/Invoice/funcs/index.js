export const addrFetcher = (town, addr) => {
  Array.prototype.last = function () {
    return this[this.length - 1]
  }
  if (town && addr && town === addr) {
    let regex = /(?:from|г|Г|)[ ]+([\wА-Яа-я\- ]+)[,]/gm
    let townStr = town.match(regex)[0]
    let addr = town.split(townStr)?.last()?.trim()
    townStr = townStr.replace(',', '')?.trim()
    if (townStr !== '') return { town: townStr, addr }
  }
  return { town, addr }
}
