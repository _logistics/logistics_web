import styled from 'styled-components'
import { comStyles, theme } from '@/utils/index'

export const Wrapper = styled.div`
  ${comStyles.flexColumn('align-center', 'justify-between')};
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
  & * {
    font-weight: 700;
  }
`
export const InvoiceHeaderWrapper = styled.div`
  ${comStyles.flexColumn('align-center', 'justify-between')};
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
`
export const InvoiceBodyWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  & > div {
    width: calc(50% - 20px);
    margin-right: 20px;
  }
  @media only screen and (max-width: 1360px) {
    flex-direction: column;
    & > div {
      width: 100%;
      margin: 0;
    }
  }
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
`
export const InvoiceSenderWrapper = styled.div`
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
`
export const ConfirmationSendageWrapper = styled.div`
  margin-top: 10px;
  border: 1px solid gray;
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
`
export const InvoiceBodyColumnWrapper = styled.div`
  ${comStyles.flexColumn('align-center', 'justify-between')};
  & > div {
    width: calc(100%);
  }
  width: ${comStyles.ARTICLE_PAGE_MAX_CONTENT_WIDTH};
`
export const Lines = styled.div`
  width: 100%;
  height: 15px;
  background-image: url('/images/lines.png');
  background-repeat: no-repeat;
  background-size: cover;
`
export const FlexWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 60px;
  width: 100%;
  padding: 10px 5%;
  min-width: 600px;
  @media only screen and (max-width: 760px) {
    flex-direction: column;
  }
`
export const Logo = styled.div`
  width: 100px;
  height: 50px;
  background-image: url('/images/logo.png');
  background-repeat: no-repeat;
  background-size: cover;
  @media only screen and (max-width: 760px) {
    display: none;
  }
`
export const InvoiceText = styled.div`
  text-align: center;
  & > p {
    margin-top: -15px;
  }
  & > h3 {
    font-weight: 900;
  }
  @media only screen and (max-width: 760px) {
    & > p {
      font-size: 0.8em;
      margin-top: -15px;
    }
    & > h3 {
      font-size: 1.1em;
    }
  }
  @media only screen and (max-width: 760px) {
    text-align: left;
    & > p {
      font-size: 0.8em;
      margin-top: -15px;
    }
    & > h3 {
      font-size: 1.1em;
    }
  }
`
export const InvoiceDate = styled.div`
  text-align: right;
  max-width: 150px;
  word-break: break-word;
  @media only screen and (max-width: 760px) {
    display: none;
  }
`
export const BlockHead = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
  width: 100%;
  & > div {
    border: 1px solid gray;
    padding: 2px 5px;
    min-height: 30px;
    margin-right: 10px;
    background-color: #e2e2e2;
    display: flex;
    align-items: center;
    font-size: 12px;
  }
  & > div:first-child {
    width: 20%;
  }
  & > div:nth-child(2) {
    width: 60%;
  }
  & > div:last-child {
    width: 20%;
    margin-right: 0;
  }
`
export const BlockConfirmation = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;
  & > div {
    margin-right: 10px;
    display: flex;
    align-items: center;
    font-size: 12px;
  }
  & > div:first-child {
    width: 20%;
  }
  & > div:nth-child(2) {
    width: 60%;
  }
  & > div:last-child {
    width: 20%;
    margin-right: 0;
  }
`
export const BlockTable = styled.div`
  margin-top: 10px;
  width: 100%;
  border: 1px solid gray;
  & > div {
    border: 1px solid gray;
    width: 100%;
    min-height: 30px;
    display: flex;
    align-items: center;
    font-size: 12px;
    & > div {
      width: 100%;
      margin-right: 10px;
      display: flex;
      align-items: center;
      font-size: 12px;
    }
  }
`
