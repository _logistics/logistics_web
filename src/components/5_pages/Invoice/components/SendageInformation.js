import { merge } from 'ramda'
import React from 'react'
import { ConfirmationSendageWrapper, BlockConfirmation } from '../styles'

const Insurance = ({ children, products }) => {
  const cellStyle = {
    width: '16.05%',
    textAlign: 'center',
    borderRight: '1px solid gray',
    justifyContent: 'center',
    minHeight: '30px',
  }
  const cellLastStyle = {
    width: '15.6%',
    borderRight: '1px solid gray',
    justifyContent: 'start',
    minHeight: '30px',
    padding: '0 10px',
    fontWeight: '900',
  }
  return (
    <ConfirmationSendageWrapper>
      <div
        style={{
          width: '100%',
          fontWeight: 900,
          backgroundColor: '#e2e2e2',
          textAlign: 'center',
          padding: '5px 0',
          borderBottom: '1px solid gray',
        }}
      >
        {children}
      </div>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
        }}
      >
        <div style={{ width: '25%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp; Только документы
        </div>
        <div style={{ width: '50%' }}>Описание вложения</div>
        <div style={{ width: '25%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp; Опасный груз
        </div>
      </BlockConfirmation>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
        }}
      >
        <div style={{ width: '25%' }}>Направление доставки</div>
        <div style={{ width: '25%' }}>
          <i class="far fa-check-square"></i>&nbsp; &nbsp; Внутри города
        </div>
        <div style={{ width: '25%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp; Внутри страны
        </div>
        <div style={{ width: '25%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp; За рубеж
        </div>
      </BlockConfirmation>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
        }}
      >
        <div style={{ width: '25%' }}>Тариф</div>
        <div style={{ width: '25%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp; Экспресс
        </div>
        <div style={{ width: '25%' }}>
          <i class="far fa-check-square"></i>&nbsp; &nbsp; Стандарт
        </div>
        <div style={{ width: '25%' }}></div>
      </BlockConfirmation>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
          padding: '0',
        }}
      >
        <div style={cellStyle}>Количество мест</div>
        <div style={cellStyle}>Вес (кг.)</div>
        <div style={merge(cellStyle, { width: '51%' })}>
          Габариты (см.) <br /> (высота / ширина / длина)
        </div>
        <div style={cellStyle}>Объем. вес (кг.)</div>
      </BlockConfirmation>
      {products?.map((el) => {
        return (
          <BlockConfirmation
            style={{
              borderBottom: '1px solid gray',
              padding: '0',
            }}
          >
            <div style={cellStyle}>{el?.count}</div>
            <div style={cellStyle}>{el?.weight}</div>
            <div style={cellStyle}>{el?.gHeight}</div>
            <div style={cellStyle}>{el?.gWidth}</div>
            <div style={cellStyle}>{el?.gLength}</div>
            <div style={cellStyle}>{el?.count * el?.weight}</div>
          </BlockConfirmation>
        )
      })}
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
          padding: '0',
        }}
      >
        <div style={cellLastStyle}>
          Итого:{' '}
          {products
            ?.map((el) => el?.count)
            .map((el) => el)
            .reduce((acc, el) => acc + el, 0)}
        </div>
        <div style={cellLastStyle}>
          Итого:{' '}
          {products
            ?.map((el) => el?.weight)
            .map((el) => el)
            .reduce((acc, el) => acc + el, 0)}
        </div>
        <div style={cellLastStyle}>Контр.:</div>
        <div style={merge(cellLastStyle, { width: '32.2%' })}>
          <i class="far fa-square"></i>&nbsp; &nbsp; Спецификация №
        </div>
        <div style={cellLastStyle}>
          Итого:{' '}
          {products
            ?.map((el) => el?.count * el?.weight)
            .map((el) => el)
            .reduce((acc, el) => acc + el, 0)}
        </div>
      </BlockConfirmation>
    </ConfirmationSendageWrapper>
  )
}

export default Insurance
