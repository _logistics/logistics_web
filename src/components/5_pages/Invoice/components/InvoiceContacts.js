import React from 'react'
import { InvoiceSenderWrapper } from '../styles'

const InvoiceContacts = () => {
  return (
    <InvoiceSenderWrapper style={{ margin: '20px 0 0' }}>
      <div
        style={{ display: 'flex', fontSize: '16px', justifyContent: 'center' }}
      >
        <div style={{ display: 'flex' }}>
          <span style={{ color: '#509736', width: '68px' }}>
            +7&nbsp;(812)&nbsp;
          </span>
          <span style={{ width: '100px' }}>3097088</span>
        </div>
        <div style={{ width: '150px' }}>tempkourier.ru</div>
      </div>
      <div
        style={{ display: 'flex', fontSize: '16px', justifyContent: 'center' }}
      >
        <div style={{ display: 'flex' }}>
          <span style={{ color: '#509736', width: '68px' }}>
            +7&nbsp;(495)&nbsp;
          </span>
          <span style={{ width: '100px' }}>1180745</span>
        </div>
        <div style={{ width: '150px' }}> info@tempkurier.ru</div>
      </div>
    </InvoiceSenderWrapper>
  )
}

export default InvoiceContacts
