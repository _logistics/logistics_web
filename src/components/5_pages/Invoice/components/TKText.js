import React from 'react'
import { InvoiceText } from '../styles'

const TKText = () => {
  return (
    <InvoiceText>
      <h2>Курьерская служба Темп Курьер</h2>
      <p>НАКЛАДНАЯ (ЭКСПЕДИТОРСКАЯ РАСПИСКА)</p>
    </InvoiceText>
  )
}

export default TKText
