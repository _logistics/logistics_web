import React from 'react'
import { InvoiceBodyColumnWrapper } from '../styles'

const InvoiceBodyRigth = ({ children }) => {
  return <InvoiceBodyColumnWrapper>{children}</InvoiceBodyColumnWrapper>
}

export default InvoiceBodyRigth
