import React from 'react'
import { InvoiceSenderWrapper, BlockHead, BlockTable } from '../styles'
import { addrFetcher } from '../funcs'

const InvoiceSender = ({
  senderFio,
  senderAddress,
  senderTown,
  senderState,
  senderPhone,
  senderArea,
  company,
}) => {
  const { town, addr } = addrFetcher(senderTown, senderAddress)
  return (
    <InvoiceSenderWrapper>
      <BlockHead>
        <div>Дата</div>
        <div>Курьер</div>
        <div>Подпись</div>
      </BlockHead>
      <BlockTable>
        <div>
          <div
            style={{
              backgroundColor: '#e2e2e2',
              borderRight: '1px solid gray',
              height: '30px',
              padding: '0 10px',
              fontWeight: 'bold',
            }}
          >
            Отправитель: {senderFio}
          </div>
          <div>Страна: Россия</div>
        </div>
        <div>Компания: {company}</div>
        <div>Город: {town}</div>
        <div>
          <div
            style={{
              borderRight: '1px solid gray',
              height: '30px',
            }}
          >
            Область: {senderState}
          </div>
          <div>Район: {senderArea}</div>
        </div>
        <div>Адрес: {addr}</div>
        <div>Контактное лицо: {senderFio}</div>
        <div>
          Телефон &nbsp;{' '}
          <span style={{ color: '#509736' }}>(обязательно): </span> +7{' '}
          {senderPhone}
        </div>
        <div>Номер присвойки</div>
      </BlockTable>
    </InvoiceSenderWrapper>
  )
}

export default InvoiceSender
