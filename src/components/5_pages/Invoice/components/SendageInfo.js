import React from 'react'
import { ConfirmationSendageWrapper, BlockConfirmation } from '../styles'

const SendageInfo = ({ children, clentId, paymentType }) => {
  return (
    <ConfirmationSendageWrapper>
      <div
        style={{
          width: '100%',
          fontWeight: 900,
          backgroundColor: '#e2e2e2',
          textAlign: 'center',
          padding: '5px 0',
          borderBottom: '1px solid gray',
        }}
      >
        {children}
      </div>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
        }}
      >
        <div style={{ width: '45%' }}>
          {paymentType === 'Наличный расчет' ? (
            <i class="far fa-check-square"></i>
          ) : (
            <i class="far fa-square"></i>
          )}
          &nbsp; &nbsp;Отправителем наличными
        </div>
        <div style={{ width: '45%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp;По договору
        </div>
      </BlockConfirmation>
      <BlockConfirmation>
        <div style={{ width: '100%' }}>
          <i class="far fa-square"></i>&nbsp; &nbsp;Наложенный платеж (оплата
          получателем)
        </div>
      </BlockConfirmation>
    </ConfirmationSendageWrapper>
  )
}

export default SendageInfo
