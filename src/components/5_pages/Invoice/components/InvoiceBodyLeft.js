import React from 'react'
import { InvoiceBodyColumnWrapper } from '../styles'

const InvoiceBodyLeft = ({ children }) => {
  return <InvoiceBodyColumnWrapper>{children}</InvoiceBodyColumnWrapper>
}

export default InvoiceBodyLeft
