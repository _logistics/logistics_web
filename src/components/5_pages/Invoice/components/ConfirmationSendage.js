import React from 'react'
import { ConfirmationSendageWrapper, BlockConfirmation } from '../styles'

const ConfirmationSendage = ({ children }) => {
  return (
    <ConfirmationSendageWrapper>
      <div
        style={{
          width: '100%',
          fontWeight: 900,
          backgroundColor: '#e2e2e2',
          textAlign: 'center',
          padding: '5px 0',
          borderBottom: '1px solid gray',
        }}
      >
        {children}
      </div>
      <BlockConfirmation>
        <div>
          Я подтверждаю, что отправление не содержит предметы, запрещенные к
          пересылке.
        </div>
        <div></div>
        <div>Подпись отправителя</div>
      </BlockConfirmation>
    </ConfirmationSendageWrapper>
  )
}

export default ConfirmationSendage
