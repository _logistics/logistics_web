import React from 'react'
import { InvoiceHeaderWrapper, Lines } from '../styles'

const InvoiceHeader = ({ children }) => {
  return (
    <InvoiceHeaderWrapper>
      {children}
      <Lines />
    </InvoiceHeaderWrapper>
  )
}

export default InvoiceHeader
