import React from 'react'
import { InvoiceSenderWrapper, BlockHead, BlockTable } from '../styles'
import { addrFetcher } from '../funcs'

const InvoiceReceiver = ({
  receiverFio,
  receiverAddress,
  receiverTown,
  receiverState,
  receiverPhone,
  receiverArea,
  comment,
  company,
}) => {
  const { town, addr } = addrFetcher(receiverTown, receiverAddress)
  return (
    <InvoiceSenderWrapper>
      <BlockHead>
        <div style={{ width: '80%' }}>Курьер</div>
        <div>Подпись</div>
      </BlockHead>
      <BlockTable>
        <div>
          <div
            style={{
              backgroundColor: '#e2e2e2',
              borderRight: '1px solid gray',
              height: '30px',
              padding: '0 10px',
              fontWeight: 'bold',
            }}
          >
            Получатель: {receiverFio}
          </div>
          <div>Страна: Россия</div>
        </div>
        <div>Компания: -</div>
        <div>Город: {town}</div>
        <div>
          <div
            style={{
              borderRight: '1px solid gray',
              height: '30px',
            }}
          >
            Область: {receiverState}
          </div>
          <div>Район: {receiverArea}</div>
        </div>
        <div>Адрес: {addr}</div>
        <div>Контактное лицо: {receiverFio}</div>
        <div>
          Телефон &nbsp; <span style={{ color: '#509736' }}>(обязательно)</span>
          : +7{receiverPhone}
        </div>
        <div>Доп информация</div>
      </BlockTable>
    </InvoiceSenderWrapper>
  )
}

export default InvoiceReceiver
