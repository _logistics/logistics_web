import React from 'react'
import { InvoiceDate } from '../styles'

const TKDate = () => {
  const today = new Date()
  return (
    <InvoiceDate>
      <p>Дата создания: {today.toLocaleString('ru-RU')}</p>
    </InvoiceDate>
  )
}

export default TKDate
