import React from 'react'
import { InvoiceBodyWrapper } from '../styles'

const InvoiceBody = ({ children }) => {
  return <InvoiceBodyWrapper>{children}</InvoiceBodyWrapper>
}

export default InvoiceBody
