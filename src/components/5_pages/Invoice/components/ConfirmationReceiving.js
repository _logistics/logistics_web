import React from 'react'
import { ConfirmationSendageWrapper, BlockConfirmation } from '../styles'

const ConfirmationReceiving = ({ children }) => {
  return (
    <ConfirmationSendageWrapper>
      <div
        style={{
          width: '100%',
          fontWeight: 900,
          backgroundColor: '#e2e2e2',
          textAlign: 'center',
          padding: '5px 0',
          borderBottom: '1px solid gray',
        }}
      >
        {children}
      </div>
      <BlockConfirmation
        style={{
          borderBottom: '1px solid gray',
        }}
      >
        <div style={{ width: '30%' }}>Дата</div>
        <div style={{ width: '25%' }}>Время</div>
        <div style={{ width: '45%' }}>Фамилия</div>
      </BlockConfirmation>
      <BlockConfirmation>
        <div>Должность</div>
        <div></div>
        <div>Подпись</div>
      </BlockConfirmation>
    </ConfirmationSendageWrapper>
  )
}

export default ConfirmationReceiving
