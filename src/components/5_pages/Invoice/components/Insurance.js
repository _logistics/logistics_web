import React from 'react'
import { ConfirmationSendageWrapper, BlockConfirmation } from '../styles'

const Insurance = ({ children, insurance }) => {
  return (
    <ConfirmationSendageWrapper>
      <div
        style={{
          width: '100%',
          fontWeight: 900,
          backgroundColor: '#e2e2e2',
          textAlign: 'center',
          padding: '5px 0',
          borderBottom: '1px solid gray',
        }}
      >
        {children}
      </div>
      <BlockConfirmation>
        <div style={{ width: '25%' }}>
          {insurance ? (
            <i class="far fa-check-square"></i>
          ) : (
            <i class="far fa-square"></i>
          )}
          &nbsp; &nbsp;Да
        </div>
        <div style={{ width: '25%', borderRight: '1px solid gray' }}>
          {!insurance ? (
            <i class="far fa-check-square"></i>
          ) : (
            <i class="far fa-square"></i>
          )}
          &nbsp; &nbsp;Нет
        </div>
        <div style={{ width: '50%' }}>Объявленная ценность</div>
      </BlockConfirmation>
    </ConfirmationSendageWrapper>
  )
}

export default Insurance
