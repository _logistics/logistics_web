/* eslint-disable no-restricted-globals */
/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-case-declarations */
import React, { useState, useEffect } from 'react'
import { Wrapper, Form } from './styles'
import { Steps, Tabs, Radio, Result, notification, Button, Input } from 'antd'
import { formBuilder, checkValidForm } from '@/components/0_functionals/builder'
import { curry, pick, assoc, merge, cond, T } from 'ramda'
import Center from '@/components/1_atoms/Center'
import SelectPackages from './SelectPackages'
import TwoButtons from '@/components/2_molecules/TwoButtons'
import MainDatePicker from '@/components/2_molecules/DatePicker'
import { receiverFields, senderFields } from './formFields'
import { useStore } from '@/stores/init'
import { getArrayOfValuesByKey, updateObjectKeys } from 'core_js_devbi'
import {
  randKey,
  pipe,
  lowerFirstLetter,
  objFieldsIsNotEmpty,
  openNotifi,
} from '@/utils/index'
import { AddressSuggestions } from '@/components/2_molecules/Dadata/AddressSuggestions'
import { masks } from '@dev-bi/rct_utils'

const CreateOrderContainer = () => {
  const { Step } = Steps
  const { TabPane } = Tabs
  const [current, setCurrent] = useState(0)
  const [errors] = useState({})
  const { creatingOrder, account } = useStore({})
  const [insurance, setInsurance] = useState('со страховкой')
  const [payment, setPayment] = useState({
    paymentType: 'Наличный расчет',
    oddMoney: '0',
    payer: 'Отравитель',
  })
  const [currOrder, updateCurrentOrder] = useState(null)
  const [listPaymentsTypes, setListPaymentsTypes] = useState([])
  const cOrderData = curry((name, val) =>
    updateCurrentOrder({ ...currOrder, [name]: val }),
  )
  const cPayment = curry((name, val) => setPayment({ ...payment, [name]: val }))
  const updateOrder = () => {
    creatingOrder.updateOrder(currOrder)
  }

  const userId = account.user.id

  const goToStep = (newStep) => {
    setCurrent(newStep)
    updateOrder()
  }
  const openNotificationWithIcon = (title, description) => {
    notification.error({
      message: title,
      description,
      placement: 'bottomRight',
    })
  }

  const changeCurrentStep = (newStep) => {
    const packageList = currOrder.products
    const isPackageList = packageList && packageList.length > 0
    switch (current) {
      case 0:
        switch (newStep) {
          case 1:
            if (isPackageList) {
              goToStep(newStep)
            } else {
              openNotificationWithIcon(
                'Ошибка',
                'Необходимо указать как минимум одну посылку',
              )
            }
            break
          case 2:
            if (isPackageList) {
              goToThird()
            } else {
              openNotificationWithIcon(
                'Ошибка',
                'Необходимо указать как минимум одну посылку',
              )
            }
            break
          case 3:
            if (isPackageList) {
              goToFourth()
            } else {
              openNotificationWithIcon(
                'Ошибка',
                'Необходимо указать как минимум одну посылку',
              )
            }
            break
          default:
            break
        }
        break
      case 1:
        switch (newStep) {
          case 0:
            goToStep(newStep)
            break
          case 2:
            let isThirdStepValid =
              checkValidForm(currOrder, senderFields.params) &&
              currOrder?.senderAddress
            !currOrder?.senderAddress &&
              openNotifi('Ошибка', 'Заполните ваш адрес', 'error')
            if (isThirdStepValid) {
              goToStep(newStep)
            }
            break
          case 3:
            isThirdStepValid = checkValidForm(currOrder, senderFields.params)
            !currOrder?.senderAddress &&
              openNotifi('Ошибка', 'Заполните ваш адрес', 'error')
            if (isThirdStepValid && currOrder?.senderAddress) {
              const isFourthStepValid = checkValidForm(
                currOrder,
                receiverFields.params,
              )
              if (isFourthStepValid) {
                goToStep(newStep)
              }
            }
            break
          default:
            break
        }
        break
      case 2:
        switch (newStep) {
          case 0:
            goToStep(newStep)
            break
          case 1:
            goToStep(newStep)
            break
          case 3:
            const isThirdStepValid = checkValidForm(
              currOrder,
              senderFields.params,
            )
            if (isThirdStepValid) {
              const isFourthStepValid = checkValidForm(
                currOrder,
                receiverFields.params,
              )
              if (isFourthStepValid) {
                goToStep(newStep)
              }
            }
            break
          default:
            break
        }
        break
      case 3:
        goToStep(newStep)
        break
      default:
        break
    }
    return null
  }

  const goToThird = () => {
    const isValid =
      checkValidForm(currOrder, senderFields.params) && currOrder?.senderAddress
    !currOrder?.senderAddress &&
      openNotifi('Ошибка', 'Заполните ваш адрес', 'error')
    if (isValid && currOrder.deliveryAwaitedDatetime) {
      const senderFieldsNames = getArrayOfValuesByKey(
        senderFields.params,
        'name',
      )
      const newAddress = pick(senderFieldsNames, currOrder)
      const updater = (str) => {
        return pipe(str)(
          (x) => x.substring(1),
          (x) => lowerFirstLetter(x),
        )
      }
      creatingOrder.updateAddress(
        updateObjectKeys(newAddress, updater),
        'senderAddres',
      )
      goToStep(2)
    }
    if (!currOrder.deliveryAwaitedDatetime) {
      openNotificationWithIcon('Ошибка', 'Необходимо заполнить дату доставки')
    }
  }

  const goToFourth = () => {
    const isValid =
      checkValidForm(currOrder, receiverFields.params) &&
      currOrder?.receiverAddress
    !currOrder?.receiverAddress &&
      openNotifi('Ошибка', 'Заполните адрес доставки', 'error')
    const sA = currOrder?.senderAddress?.value || currOrder?.senderAddress
    const rA = currOrder?.receiverAddress?.value || currOrder?.receiverAddress
    if (sA === rA) {
      if (!confirm('Вы ввели однаковый адрес, продолжить?')) return null
    }
    if (isValid) {
      const reciverFieldsNames = getArrayOfValuesByKey(
        receiverFields.params,
        'name',
      )
      const newAddress = pick(reciverFieldsNames, currOrder)
      const updater = (str) => {
        return pipe(str)(
          (x) => x.substring(1),
          (x) => lowerFirstLetter(x),
        )
      }
      creatingOrder.updateAddress(
        updateObjectKeys(newAddress, updater),
        'reciverAddress',
      )
      goToStep(3)
    }
  }

  const nextStep = () => goToStep(current + 1)

  const preiewStep = () => goToStep(current - 1)

  const prepairTime = (hr) => {
    const d = new Date()
    const nhr = Math.floor(hr / 2) * 2 + 2
    return new Date(d.getFullYear(), d.getMonth(), d.getDate(), nhr, 0, 0)
  }
  const minHr = cond([
    [(hr) => hr < 10, () => prepairTime(10)],
    [(hr) => hr >= 10 && hr <= 17, (hr) => prepairTime(hr)],
    [
      T,
      () => {
        const d = new Date()
        return new Date(
          d.getFullYear(),
          d.getMonth(),
          d.getDate() + 1,
          10,
          0,
          0,
        )
      },
    ],
  ])(new Date().getHours())

  const getListPaymentTypes = () => {
    creatingOrder.listPaymentTypes().then((el) => {
      setListPaymentsTypes(el.listPaymentTypes)
    })
  }

  useEffect(() => {
    creatingOrder.fillOrCreateOrder(userId)
    creatingOrder.getLastInComplitedOrder(userId).then((el) => {
      if (el.getLastIncompleted && account.user.profile) {
        const order = el.getLastIncompleted
        updateCurrentOrder(order)
        const senderData = pick(
          [
            'senderAddress',
            'senderArea',
            'senderComment',
            'senderFio',
            'senderPhone',
            'senderState',
            'senderTown',
          ],
          el.getLastIncompleted,
        )
        objFieldsIsNotEmpty(senderData)
        if (!objFieldsIsNotEmpty(senderData)) {
          const updater = (str) => {
            return pipe(str)(
              (x) => x.replace('town', 'senderTown'),
              (x) => x.replace('street', 'senderAddress'),
              (x) => x.replace('phone', 'senderPhone'),
            )
          }
          pipe(account.user.profile)(
            (x) => pick(['town', 'street', 'apartment'], x),
            (x) => assoc('phone', account.user.phone, x),
            (x) => updateObjectKeys(x, updater),
            (x) => {
              x.senderAddress =
                order?.senderAddress || account.user.profile.street
              const { firstName, middleName, lastName } = account.user.profile
              const userName = `${firstName || ' '} ${middleName || ' '} ${
                lastName || ' '
              }`
              x.senderFio = userName
              return x
            },
            (x) => merge(el.getLastIncompleted, x),
            (x) => updateCurrentOrder(x),
          )
        }
      } else if (account.user.profile) {
        window.location.reload()
      }
    })
    getListPaymentTypes()
  }, [])

  const sendOrder = async () => {
    const paymentTypeId =
      listPaymentsTypes.find((el) => el.name === payment.paymentType)?.id ||
      null
    const answer = !!creatingOrder.sendOrder({
      ...currOrder,
      insurance,
      ...payment,
      paymentTypeId,
    })
    if (answer) {
      nextStep()
    }
  }
  if (account.isCourier() && !account.isSuperLgst()) {
    window.location.href = 'orders-history'
  }
  if (!account.user.profile) {
    return (
      <Result
        status="404"
        title="Заполните профиль"
        subTitle="Вы не заполнили профиль пользователя. Для создания заказа необходимо заполнить профиль"
        extra={
          <Button
            onClick={() => {
              window.location.href = '/account'
            }}
            type="primary"
          >
            Перейти в профиль
          </Button>
        }
      />
    )
  }

  if (!currOrder) {
    return <p>loading</p>
  }

  return (
    <Wrapper>
      <h2>Пошаговая форма</h2>
      <Center>
        <Steps className="steps" current={current} onChange={changeCurrentStep}>
          <Step title="Посылка" />
          <Step title="Ваш адрес" />
          <Step title="Адрес доставки" />
          <Step title="Стоимость" />
        </Steps>
      </Center>
      <Tabs
        activeKey={`${current}`}
        hideAdd
        defaultActiveKey="0"
        onChange={changeCurrentStep}
      >
        <TabPane tab={null} key="0">
          <Center>
            <SelectPackages
              updateCurrentOrder={updateCurrentOrder}
              currOrder={currOrder}
              updatePackage={() => {}}
              nextStep={nextStep}
            />
          </Center>
        </TabPane>
        <TabPane tab={null} key="1">
          <Center>
            <Form>
              <MainDatePicker
                showTimeSelect
                minDate={minHr}
                selected={currOrder.deliveryAwaitedDatetime}
                placeholderText="Дата доставки"
                onChange={(e) => cOrderData('deliveryAwaitedDatetime')(e)}
                isClearable
                className="mb2"
              />
              <p style={{ marginBottom: 0 }}>Адрес отправителя:</p>
              <AddressSuggestions
                onChange={(e) => {
                  cOrderData('senderAddress')(e)
                }}
                value={currOrder?.senderAddress || ''}
                token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
                defaultQuery={currOrder?.senderAddress || ''}
              />
              {formBuilder(senderFields, currOrder, cOrderData, errors)}
              <TwoButtons
                fText="Далее"
                sText="Назад"
                fClick={goToThird}
                sClick={preiewStep}
              />
            </Form>
          </Center>
        </TabPane>
        <TabPane tab={null} key="2">
          <Center>
            <Form>
              <p style={{ marginBottom: 0 }}>Адрес получателя:</p>
              <AddressSuggestions
                onChange={(e) => {
                  cOrderData('receiverAddress')(e)
                }}
                defaultQuery={currOrder?.receiverAddress || ''}
                value={currOrder?.receiverAddress || ''}
                token="517ab8df1ac515cbc23a2a4974643baca6a364d9"
              />
              {formBuilder(receiverFields, currOrder, cOrderData, errors)}
              <TwoButtons
                fText="Далее"
                sText="Назад"
                fClick={goToFourth}
                sClick={preiewStep}
              />
            </Form>
          </Center>
        </TabPane>
        <TabPane tab={null} key="3">
          <Center>
            <div>
              <Form display="flex">
                <h3>Тип оплаты</h3>
                <Radio.Group
                  onChange={(e) => {
                    cPayment('paymentType')(e.target.value)
                  }}
                  value={payment.paymentType}
                  style={{ marginBottom: 20 }}
                >
                  {listPaymentsTypes.map((el, idx) => (
                    <Radio key={`item-${idx * 1.1}`} value={el.name}>
                      {el.name}
                    </Radio>
                  ))}
                </Radio.Group>

                {payment.paymentType === 'Наличный расчет' ? (
                  <>
                    <h3>Сумма сдачи</h3>
                    <Input
                      style={{ marginBottom: 20 }}
                      value={payment.oddMoney}
                      onChange={(e) =>
                        cPayment('oddMoney')(masks.wrapNumMsk(e.target.value))
                      }
                      placeholder="Сумма сдачи"
                    />
                  </>
                ) : null}
                <h3>Кто платит</h3>
                <Radio.Group
                  onChange={(e) => {
                    cPayment('payer')(e.target.value)
                  }}
                  value={payment.payer}
                >
                  <Radio value="Получатель">Получатель</Radio>
                  <Radio value="Отравитель">Отравитель</Radio>
                </Radio.Group>
              </Form>
              <Form display="flex">
                <h3>Тип оплаты</h3>
                <p style={{ color: 'gray', fontSize: '14px' }}>
                  Страхование увеличит стоимость на 0.09% от стоимости груза
                </p>
                <Radio.Group
                  onChange={(e) => {
                    setInsurance(e.target.value)
                  }}
                  value={insurance}
                >
                  <Radio value="со страховкой">Да</Radio>
                  <Radio value="без страховки">Нет</Radio>
                </Radio.Group>
              </Form>
              <Form>
                <h3>Дополнительно</h3>
                <h4>Итоговую сумму рассчитает менеджер и перезвонит вам.</h4>
                <h4>
                  Укажите почту если вы хотите получить уведомление о статусе
                  заказа
                </h4>
                <Button
                  style={{ marginBottom: 10 }}
                  onClick={() => {
                    const { mail } = account?.user?.profile
                    cOrderData('sEmail')(mail || '')
                  }}
                  type="primary"
                  ghost
                  size="small"
                >
                  Указать свою почту
                </Button>
                <Input
                  value={currOrder.sEmail || ''}
                  onChange={(e) => cOrderData('sEmail')(e.target.value)}
                  placeholder="Почта"
                />
              </Form>
              <Form>Итоговую сумму рассчитает менеджер и перезвонит вам.</Form>
              <TwoButtons
                fText="Отправить заказ"
                sText="Назад"
                fClick={sendOrder}
                sClick={preiewStep}
              />
            </div>
          </Center>
        </TabPane>
        <TabPane tab={null} key="4">
          <Center>
            <Result
              status="success"
              title="Заказ успешно создан"
              subTitle={`Номер заказа: ${currOrder.id}. Скоро менеджер рассчитает цену заказа. Она отобразиться в списке ваших заказов.`}
              extra={[
                <Wrapper>
                  <Button
                    type="primary"
                    size="large"
                    onClick={() => window.location.reload()}
                    style={{ margin: '10px 0' }}
                  >
                    К списку заказов
                  </Button>
                  <Button
                    type="secondary"
                    size="large"
                    onClick={() => (window.location.href = '/orders-history')}
                    style={{ margin: '10px 0' }}
                  >
                    Новый заказ
                  </Button>
                  <Button
                    type="secondary"
                    size="large"
                    type="dashed"
                    style={{ margin: '10px 0' }}
                  >
                    Скачать накладную
                  </Button>
                </Wrapper>,
              ]}
            />
          </Center>
        </TabPane>
      </Tabs>
    </Wrapper>
  )
}

export default CreateOrderContainer
