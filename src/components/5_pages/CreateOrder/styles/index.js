import styled from 'styled-components';
import { lighten } from 'polished'

/* eslint-disable import/prefer-default-export */
export const Wrapper = styled.div`
  padding: 24px;
  display: flex;
  flex-direction: column;
  & .tab {
    display: none;
  }
  & .steps {
    width: 900px;
  }
  & h2 {
    font-size: 16px;
  }
  @media only screen and (max-width: 1400px) {
    & h2 {
      margin-left: 100px
    }
    & .steps {
      width: 100%;
    }
  }
  @media only screen and (max-width: 800px) {
    padding: 10px;
  }
`;

export const Form = styled.div`
  background-color: #FAFAFA;
  padding: 24px 30px;
  width: 600px;
  margin-bottom: 20px;
  display: ${({ lineStyle = false }) => lineStyle ? 'flex' : 'block'};
  align-items: ${({ lineStyle = false }) => lineStyle ? 'center' : 'auto'};
  & .package_name {
    width: 100px;
    font-weight: 600;
    font-size: 16px;
  };
  & .package_description {
    width: 380px;
  };
  & div.package_icons {
    font-size: 20px;
    cursor: pointer;
    & i {
      transition: all .3s;
    }
    & .fa-edit {
      margin-right: 10px;
      color: #0050B3;
      &:hover {
        color: ${lighten(0.2, '#0050B3')}
      }
    }
    & .fa-trash {
      color: #9E1068;
      &:hover {
        color: ${lighten(0.2, '#9E1068')}
      }
    }
  };
  @media only screen and (max-width: 800px) {
    width: 100%;
  }
  @media only screen and (max-width: 680px) {
    padding: 15px 15px;
    & .package_name {
    width: 70px;
    font-size: 11px;
  };
  & .package_description {
    width: 160px;
    font-size: 10px;
  };
  & div.package_icons {
    font-size: 20px;
    & .fa-edit {
      margin-right: 10px;
    }
  };
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-wrap: wrap; 
  max-width: 600px;
  margin-bottom: 120px;
  @media only screen and (max-width: 800px) {
      padding: 0 15px;
      & .bt_button {
        margin-bottom: 20px;
        width: 100%
      }
    }
`;
