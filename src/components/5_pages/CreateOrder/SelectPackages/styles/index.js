import styled from 'styled-components';
import { lighten, darken } from 'polished';


export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  & h2 {
    margin-right: 20px;
  };
`

export const ApproximateContainer = styled.div`
  width: 600px;
  @media only screen and (max-width: 700px) {
    width: 400px;
  }
  @media only screen and (max-width: 400px) {
    width: 320px;
  }
`

export const ApproximateItem = styled.div`
  display: flex;
  cursor: pointer;
  padding: 0 20px;
  background-color: #FAFAFA;
  transition: all .3s;
  & div {
    padding: 20px 0;
    display: flex;
    align-items: center;
  }
  & div.title {
    width: 20%;
    border-bottom: 1px solid ${lighten(0.3, 'gray')};
  }
  & div.size {
    width: 20%;
    border-bottom: 1px solid ${lighten(0.3, 'gray')};
  }
  & div.weight {
    width: 20%;
    border-bottom: 1px solid ${lighten(0.3, 'gray')};
  }
  & div.description {
    width: 35%;
    border-bottom: 1px solid ${lighten(0.3, 'gray')};
  }
  &:hover {
    background-color: ${darken(0.1, '#FAFAFA')};
  }
  @media only screen and (max-width: 700px) {
    & div {
      padding: 10px 0;
      display: flex;
      align-items: center;
    }
    font-size: 10px;
  }
`
