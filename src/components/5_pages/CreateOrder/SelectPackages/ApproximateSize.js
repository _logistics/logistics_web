import React from 'react'
import { ApproximateItem, ApproximateContainer } from './styles'
import { randKey } from '@/utils/index'
import { assoc, dissoc } from 'ramda'
import { Button } from '@/components/1_atoms/Buttons'
import { pipe } from '@/utils/functions'


const packegeItems = [
  {
    title: 'Конверт',
    size: '35x25x5 см',
    gLength: 35,
    gHeight: 25,
    gWidth: 5,
    weight: 2,
    textWeight: 'до 2 кг',
    description: 'Маленькие предметы: документация, бижутерия, аксесуарты'
  },
  {
    title: 'Пакет',
    size: '40x30x20 см',
    weight: 5,
    gLength: 40,
    gHeight: 30,
    gWidth: 20,
    textWeight: 'до 5 кг',
    description: 'Небольшие отправления: обувь, одежда, мелкая техника '
  },
  {
    title: 'Коробка',
    size: '60x40x30 см',
    weight: 20,
    gLength: 60,
    gHeight: 40,
    gWidth: 30,
    textWeight: 'до 20 кг',
    description: 'Средний размер: набор посуды, домашний текстиль'
  },
  {
    title: 'Тележка',
    size: '100x50x50 см',
    weight: 50,
    gLength: 100,
    gHeight: 50,
    gWidth: 50,
    textWeight: 'до 50 кг',
    description: 'Тяжелая большая посылка: велосипед, крупная кухонная техника'
  },
  {
    title: 'Палета',
    size: '120x80x80 см',
    weight: 70,
    gLength: 120,
    gHeight: 80,
    gWidth: 80,
    textWeight: 'более 50 кг',
    description: 'Крупный груз: крупная бытовая техника, домашняя библиотека'
  },
]

const ApproximateSize = ({ packageAction, isEdit, undoEdit }) => {
  const handleAddPackage = (packageId) => {
    pipe(packegeItems[packageId])(
      (x) => assoc('type', 'approximate', x),
      (x) => dissoc('textWeight', x),
      (x) => packageAction(x),
    )
  }

  const getUndoEditBtb = () => {
    if (isEdit) {
      return <Button onClick={undoEdit} reversed>Отмена</Button>
    }
  }
  return (
    <ApproximateContainer>
      {packegeItems.map((el, idx) => (
        <ApproximateItem
          onClick={() => {
            handleAddPackage(idx)
          }}
          key={randKey()}
        >
          <div className="title">
            {el.title}
          </div>
          <div className="size">
            {el.size}
          </div>
          <div className="weight">
            {el.textWeight}
          </div>
          <ApproximateContainer className="description">
            {el.description}
          </ApproximateContainer>
        </ApproximateItem>
      ))}
      {getUndoEditBtb()}
    </ApproximateContainer>
  )
}

export default ApproximateSize
