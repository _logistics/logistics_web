/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import { Radio } from 'antd'
import { Wrapper } from './styles'
import { Form } from '../styles'
import ApproximateSize from './ApproximateSize'
import ExactSize from './ExactSize'
import TwoButtons from '@/components/2_molecules/TwoButtons'
import {
  randKey,
  pipe,
  substringIfNeed,
  converPackageToServer,
} from '@/utils/index'
import { is, remove, insert, assoc } from 'ramda'
import { useStore } from '@/stores/init'

const SelectPackages = ({
  nextStep,
  currOrder,
  updateCurrentOrder,
  disabled,
}) => {
  const [packageType, setPackageType] = useState('approximate')
  const packagesList = currOrder ? currOrder.products : []
  const [selectPackage, setSelectPackage] = useState(true)
  const [updater, setUpdater] = useState(0)
  const { creatingOrder } = useStore({})
  const [editablePackageId, setEditablePackageId] = useState(null)

  useEffect(() => {
    if (packagesList && packagesList.length > 0) {
      setSelectPackage(false)
    }
    setTimeout(() => {
      setUpdater(updater + 1)
    }, 1000)
  }, [])

  const addPackage = (newPackage) => {
    const updatingPackage = currOrder

    const product = pipe(newPackage)(
      (x) => converPackageToServer(x, currOrder.id),
      (x) => creatingOrder.putProduct(x),
    )
    product.then((el) => {
      newPackage.productId = el.id
      const upPackage = {
        ...newPackage,
        productId: el.putProduct.id,
      }
      updatingPackage.products = [...currOrder.products, upPackage]
      setSelectPackage(false)
      setUpdater(updater + 1)
    })
  }
  const isEmptyPackageList = !packagesList || !packagesList.length

  const updatePackage = (newPackage) => {
    const updatingPackage = currOrder
    const packageId =
      currOrder.products[editablePackageId].id ||
      packagesList[editablePackageId].productId
    const remPackageList = remove(editablePackageId, 1, currOrder.products)
    const newPackageList = insert(editablePackageId, newPackage, remPackageList)
    updatingPackage.products = newPackageList
    pipe(newPackage)(
      (x) => assoc('title', newPackage.description, x),
      (x) => assoc('productId', packageId, x),
      (x) => creatingOrder.updateProduct(x),
    )
    updateCurrentOrder(updatingPackage)
    setUpdater(updater + 1)
    setEditablePackageId(null)
  }

  const deletePackage = (id) => {
    if (packagesList && packagesList.length > 0) {
      const delPackId = currOrder.products[id].id
      creatingOrder.deleteProduct(delPackId)
      const updatingPackage = currOrder
      const newPackageList = remove(id, 1, packagesList)
      updatingPackage.products = newPackageList
      updateCurrentOrder(updatingPackage)
      setUpdater(updater + 1)
    } else {
      return {
        status: false,
        data: 'list of packages is empty',
      }
    }
    return {
      status: true,
      data: 'package deleted',
      listIsEmpty: currOrder.products.length,
    }
  }

  const onChangePackegeType = (e) => {
    setPackageType(e.target.value)
  }

  const getTypePackageSelecter = () => {
    if (selectPackage || is(Number, editablePackageId)) {
      return (
        <Form>
          <h3>Вес и размер посылки</h3>
          <Radio.Group onChange={onChangePackegeType} value={packageType}>
            <Radio value="approximate">Примерно</Radio>
            <Radio value="exact">Точно</Radio>
          </Radio.Group>
        </Form>
      )
    }
    return null
  }

  const undoEdit = () => {
    setEditablePackageId(null)
    setSelectPackage(false)
  }

  const getPackageSelecter = () => {
    const getButtonAction = (newPackage) => {
      if (is(Number, editablePackageId)) {
        updatePackage(newPackage)
      } else {
        addPackage(newPackage)
      }
    }
    if (selectPackage || is(Number, editablePackageId)) {
      if (packageType === 'exact') {
        return (
          <ExactSize
            isEdit={is(Number, editablePackageId)}
            editPackage={packagesList ? packagesList[editablePackageId] : null}
            packageAction={getButtonAction}
            undoEdit={undoEdit}
          />
        )
      }
      return (
        <ApproximateSize
          isEdit={is(Number, editablePackageId)}
          undoEdit={undoEdit}
          packageAction={getButtonAction}
        />
      )
    }
  }

  const getButtons = () => {
    if (isEmptyPackageList || disabled) {
      return null
    }
    if (!selectPackage) {
      return (
        <TwoButtons
          fText="Далее"
          sText="Добавить посылку"
          fClick={nextStep}
          sClick={() => {
            setSelectPackage(true)
          }}
        />
      )
    }
    if (selectPackage) {
      return (
        <TwoButtons
          fText="Далее"
          sText="Отмена"
          fClick={nextStep}
          sClick={() => {
            setSelectPackage(false)
          }}
        />
      )
    }
    return null
  }

  const getSelectesPackagesList = () => {
    const getDescribtion = (el) => {
      if (el.type === 'exact') {
        const description = el.description || el.places || ''
        const weight = description ? ` весом ${el.weight}` : `Вес: ${el.weight}`
        const dimensions = description
          ? `габаритами: ${el.gLength}`
          : `габариты: ${el.gLength}x${el.gWidth}x${el.gHeight}`
        const describtion = `${description}${weight} кг и ${
          dimensions || ''
        } см`
        return substringIfNeed(describtion, 43)
      }
      return substringIfNeed(el.description || el.places, 43)
    }
    if (isEmptyPackageList) return null
    return packagesList.map((el, idx) => {
      if (idx !== editablePackageId) {
        return (
          <Form lineStyle key={randKey()}>
            <div className="package_name">{`Посылка ${idx + 1}`}</div>
            <div className="package_description">{getDescribtion(el)}</div>
            <div className="package_icons">
              {disabled ? null : (
                <>
                  <i
                    onClick={() => {
                      setEditablePackageId(idx)
                    }}
                    className="fal fa-edit"
                  />
                  <i
                    onClick={() => {
                      const answer = deletePackage(idx)
                      if (answer.listIsEmpty === 0) {
                        setSelectPackage(true)
                      }
                      setUpdater(updater + 1)
                    }}
                    className="fal fa-trash"
                  />
                </>
              )}
            </div>
          </Form>
        )
      }
      return null
    })
  }

  return (
    <Wrapper>
      {getTypePackageSelecter()}
      {getPackageSelecter()}
      {getSelectesPackagesList()}
      {getButtons()}
    </Wrapper>
  )
}

export default SelectPackages
