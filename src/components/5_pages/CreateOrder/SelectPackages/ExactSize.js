import React, { useState, useEffect } from 'react';
import { formBuilder, checkValidForm } from '@/components/0_functionals/builder';
import { getArrayOfValuesByKey } from '@/utils/finders';
import { curry, assoc, lensProp, set } from 'ramda';
import { Button } from '@/components/1_atoms/Buttons';
import { Form } from '../styles';
import TwoButtons from '@/components/2_molecules/TwoButtons';
import { pipe } from '@/utils/functions'

export const mainfields = {
  params: [
    { name: 'weight', mask: { onlyNumber: true, nonNullable: true }, label: 'Вес (кг)', type: 'text', length: { min: 5, max: 30 }, error: 'Вы не заполнили поле Вес (кг)' },
    { name: 'gLength', mask: { onlyNumber: true, nonNullable: true }, label: 'Длина (см)', type: 'text', length: { min: 5, max: 30 }, error: 'Вы не заполнили поле Длинна (см)' },
    { name: 'gWidth', mask: { onlyNumber: true, nonNullable: true }, label: 'Ширина (см)', type: 'text', length: { min: 5, max: 30 }, error: 'Вы не заполнили поле Ширина (см)' },
    { name: 'gHeight', mask: { onlyNumber: true, nonNullable: true }, label: 'Высота (см)', type: 'text', length: { min: 5, max: 30 }, error: 'Вы не заполнили поле Высота (см)' },
    { name: 'places', label: 'Комментарий', type: 'text', length: { min: 5, max: 30 } },
  ]
}

const ExactSize = ({ packageAction, isEdit = false, editPackage = {}, undoEdit }) => {
  const [personals, cPersonals] = useState({});
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }));
  const [errors] = useState({});

  const handleAddPackage = () => {
    const valid = checkValidForm(personals, mainfields.params);
    if (valid) {
      pipe(personals)(
        (x) => assoc('type', 'exact', x),
        (x) => set(lensProp('weight'), parseInt(personals.weight, 10), x),
        (x) => set(lensProp('gLength'), parseInt(personals.gLength, 10), x),
        (x) => set(lensProp('gWidth'), parseInt(personals.gWidth, 10), x),
        (x) => set(lensProp('gHeight'), parseInt(personals.gHeight, 10), x),
        (x) => packageAction(x)
      )
    }
  }

  useEffect(() => {
    if (isEdit) {
      const fieldsNames = getArrayOfValuesByKey(mainfields.params, 'name');
      let newPersonals = {};
      for (let i = 0; i < fieldsNames.length; i += 1) {
        const el = fieldsNames[i];
        if (editPackage[el]) {
          const newEl = assoc(el, editPackage[el], newPersonals);
          newPersonals = newEl;
        }
      }
      cPersonals(newPersonals);
    }
  }, [editPackage, isEdit])

  const getButtons = () => {
    if (isEdit) {
      return (
        <TwoButtons
          fText="Обновить посылку"
          sText="Отмена"
          fClick={handleAddPackage}
          sClick={undoEdit}
        />
      )
    }
    return (<Button onClick={handleAddPackage} reversed>Добавить посылку</Button>)
  }

  return (
    <Form>
      {formBuilder(mainfields, personals, cData, errors, '100%')}
      {getButtons()}
    </Form>
  )
}

export default ExactSize
