/* eslint-disable react/destructuring-assignment */
import React from 'react'
import { ru } from 'date-fns/locale';
import DatePicker, { registerLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { zipObj, without, cond, T } from 'ramda';
import { ValidationText } from './ValidationText';
// import convertDataService from '../../services/conversionData.service';
// import { ValidationText } from '../../services/checkValidation.servise';

/**
 * Another props:
 * disabled
 * showTimeSelect
 * selfStyle
 * */


const MainDatePicker = (props) => {
  let selfClas = 'mainDatePicker';

  const getComponentProps = (props, badKeys) => {
    const goodPropsList = Object.keys(props);
    const newKeysList = without(badKeys, goodPropsList);
    const itemsList = newKeysList.map((el) => props[el]);
    return zipObj(newKeysList, itemsList);
  };

  registerLocale('ru', ru);

  if (props.className) selfClas += ` ${props.className}`;

  const ignorKeys = ['selected', 'minDate', 'maxDate', 'className'];

  const componentProps = getComponentProps(props, ignorKeys);

  if (props.selfStyle) {
    selfClas += ` mainDatePicker--${props.selfStyle}`;
  }

  const getFormat = (format) => {
    switch (format) {
      case 'DMYHM':
        return 'dd.MM.yyyy HH:mm'
      case 'DMY':
        return 'dd.MM.yyyy'
      default:
        return 'dd.MM.yyyy HH:mm'
    }
  }

  const pickerFormat = getFormat(props.format);

  const renameTimeTitle = () => {
    setTimeout(() => {
      if (document.getElementsByClassName('react-datepicker-time__header')[0]) {
        document.getElementsByClassName('react-datepicker-time__header')[0].innerHTML = 'Время';
      } else {
        renameTimeTitle();
      }
    }, 5);
  }

  const getDate = (date) => {
    if (date === null) return null
    switch (typeof (date)) {
      case 'string':
        return new Date(date.replace(/\s/, 'T'))
      case 'object':
        return new Date(date);
      default:
        return null;
    }
  }

  const selected = getDate(props.selected);

  const prepairTime = (hr, day = 0) => {
    const d = new Date();
    const nhr = Math.floor(hr / 2) * 2 + 2;
    return new Date(d.getFullYear(), d.getMonth(), d.getDate() + day, nhr, 0, 0);
  };
  const minHr = cond([
    [() => selected && selected.getDate() !== new Date().getDate(), () => prepairTime(9)],
    [(hr) => hr < 10, () => prepairTime(10)],
    [(hr) => hr >= 10 && hr <= 17, (hr) => prepairTime(hr)],
    [
      T,
      () => {
        const d = new Date();
        return new Date(
          d.getFullYear(),
          d.getMonth(),
          d.getDate() + 1,
          10,
          0,
          0
        );
      }
    ]
  ])(new Date().getHours());

  const minD = cond([
    [(hr) => hr < 10, () => prepairTime(10)],
    [(hr) => hr >= 10 && hr <= 17, (hr) => prepairTime(hr)],
    [
      T,
      () => {
        const d = new Date();
        return new Date(
          d.getFullYear(),
          d.getMonth(),
          d.getDate() + 1,
          10,
          0,
          0
        );
      }
    ]
  ])(new Date().getHours());

  return (
    <div className={selfClas}>
      <DatePicker
        showPopperArrow={false}
        locale="ru"
        className="mainDatePicker_container"
        timeFormat="p"
        dateFormat={pickerFormat}
        timeIntervals={30}
        onCalendarOpen={renameTimeTitle}
        {...componentProps}
        selected={selected}
        minTime={minHr}
        maxTime={new Date(new Date(new Date().setHours(18)).setMinutes(0))}
        minDate={minD}
        isClearable
        disabled={props.disabled}
      />
      <ValidationText
        viewValidation={props.viewValidation}
        validation={props.validation}
        value={selected}
      />
    </div>
  )
}

export default MainDatePicker
