import styled from 'styled-components';
import { ReactSVG } from 'react-svg';

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  min-height: calc(75vh - 50px);
  background: url("/images/login_bg_pattern.jpg");
  background-repeat: no-repeat;
  background-size: cover;
  opacity: 0.9;
  position: relative;
`

export const LoginPageImage = styled(ReactSVG)`
  position: absolute;
  bottom: 35px;
  right: 50px;
  & svg{
    width: 100%;
    max-width: 300px;
    min-width: 150px;
    height: auto;
  }
`;


export const HelloMessage = styled.div`

`;
export const MainMessage = styled.h1`
  font-size: 5em;
  font-family: 'Source Sans Pro', sans-serif;
  font-weight: 200;
  color: whitesmoke;
  padding-left: 5%;
  padding-top: 10%;
  line-height: .8em;
  margin-bottom: 0;
`;
export const ServiceName = styled.h2`
  font-size: 3em;
  font-family: 'Source Sans Pro', sans-serif;
  font-weight: 800;
  color: whitesmoke;
  padding-left: 5%;
  margin: 0.5em 0;
`;
export const SelectorsWrapper = styled.div`
  position: absolute;
  top: 5%;
  right: 0px;
`;


export const Selector = styled.div`
    padding: 10px 20px 10px 20px;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
    text-align: right;
    margin: 3px 0;
    font-weight: bold;
    text-transform: uppercase;
    cursor: pointer;
    font-size: .9em;
  &.active{
    background-color: white;
  }
`;
