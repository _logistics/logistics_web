import React from 'react'
import { Wrapper, LoginPageImage, HelloMessage, MainMessage, SelectorsWrapper, Selector } from './styles'
import { Button } from '@/components/1_atoms/Buttons'
import { withTranslation } from '@/i18n'

const UserMessage = ({ scroll }) => {
  return (
    <HelloMessage>
      <MainMessage>Добро пожаловать!</MainMessage>
      <div style={{ marginLeft: '5%', marginTop: '7%' }}>
        <Button onClick={scroll} reversed whiteTxt>О Сервисе</Button>
      </div>

    </HelloMessage>
  )
}

const Selectors = withTranslation('auth')(({ t, activeSelector, selcs, scroll }) => {
  return (
    <SelectorsWrapper>
      {selcs.map((el) => (
        <Selector
          key={el.id}
          className={el.name === activeSelector ? 'active' : ''}
          onClick={() => scroll(el.id)}
        >
          {t(el.name)}
        </Selector>
      ))}
    </SelectorsWrapper>
  )
})

const AuthHelloScreen = ({ activeSelector, selcs, scroll }) => {
  return (
    <Wrapper>
      <Selectors
        activeSelector={activeSelector}
        selcs={selcs}
        scroll={scroll}
      />
      <UserMessage scroll={() => scroll(0)} />
      <LoginPageImage src="/images/login_image.svg" />
    </Wrapper>
  )
}

export default AuthHelloScreen
