import React from 'react'
import { FullScreenBlurer } from './styles'


// eslint-disable-next-line import/prefer-default-export
export const FullScreenLoader = () => {
  return (
    <FullScreenBlurer>
      <p>loading</p>
    </FullScreenBlurer>
  )
}
