import styled from 'styled-components'
import { comStyles } from '@/utils/index'

// eslint-disable-next-line import/prefer-default-export
export const FullScreenBlurer = styled.div`
  position: fixed;
  width: 100vw;
  height: 100vh;
  background-color: rgba(50,50,50,0.8);
  align-items: center;
  overflow: auto;
  ${comStyles.flex('justify-center')};
  & > p {
    font-weight: 900;
    color: white;
    font-size: 20px;
  }
`;

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  min-width: 300px;
  min-height: 300px;
  background-color: #e2e2e2;
  border-radius: 20px;
  box-shadow: 3px 0 20px rgba(0,0,0,0.2);
  align-items: center;
  ${comStyles.flex('justify-center')};
  & svg{
    filter: drop-shadow(-1px 6px 3px rgba(50, 50, 0, 0.5));
  }
`;
