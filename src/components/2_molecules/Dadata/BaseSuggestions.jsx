/* eslint-disable no-console */
/* eslint-disable react/no-did-update-set-state */
/* eslint-disable react/button-has-type */
/* eslint-disable no-unused-vars */
/* eslint-disable import/prefer-default-export */
import React, { ChangeEvent, MouseEvent, FocusEvent, ReactNode } from 'react';
import shallowEqual from 'shallowequal';
import debounce from 'lodash.debounce';
import { makeRequest } from './request';


export class BaseSuggestions extends React.PureComponent {
  /**
   * URL для загрузки подсказок, переопределяется в конкретном компоненте
   */
  loadSuggestionsUrl = '';

  /**
   * HTML-input
   */
  textInput;

  constructor(props) {
    super(props);

    const { defaultQuery, value, delay } = this.props;
    const valueQuery = value ? value.value : undefined;

    this.setupDebounce(delay);

    this.state = {
      query: (defaultQuery) || valueQuery || '',
      inputQuery: (defaultQuery) || valueQuery || '',
      isFocused: false,
      displaySuggestions: true,
      suggestions: [],
      suggestionIndex: -1
    };
  }

  componentDidUpdate(
    prevProps
  ) {
    const { value, delay } = this.props;
    const { query, inputQuery } = this.state;
    if (!shallowEqual(prevProps.value, value)) {
      const newQuery = value ? value.value : '';
      if (query !== newQuery || inputQuery !== newQuery) {
        this.setState({ query: newQuery, inputQuery: newQuery });
      }
    }

    if (delay !== prevProps.delay) {
      this.setupDebounce(delay);
    }
  }

  setupDebounce = (delay) => {
    if (typeof delay === 'number' && delay > 0) {
      this.fetchSuggestions = debounce(this.performFetchSuggestions, delay);
    } else {
      this.fetchSuggestions = this.performFetchSuggestions;
    }
  };

  /**
   * Функция, которая вернет данные для отправки для получения подсказок
   */
  getLoadSuggestionsData = () => { };

  fetchSuggestions = () => {
    //
  };

  handleInputFocus = (event) => {
    this.setState({ isFocused: true });

    const { suggestions } = this.state;

    if (suggestions.length === 0) {
      this.fetchSuggestions();
    }

    const { inputProps } = this.props;
    if (inputProps && inputProps.onFocus) {
      inputProps.onFocus(event);
    }
  };

  handleInputBlur = (event) => {
    const { suggestions } = this.state;

    this.setState({ isFocused: false });
    if (suggestions.length === 0) {
      this.fetchSuggestions();
    }

    const { inputProps } = this.props;
    if (inputProps && inputProps.onBlur) {
      inputProps.onBlur(event);
    }
  };

  handleInputChange = (event) => {
    const { value } = event.target;
    const { inputProps } = this.props;
    this.setState(
      { query: value, inputQuery: value, displaySuggestions: true },
      () => {
        this.fetchSuggestions();
      }
    );

    if (inputProps && inputProps.onChange) {
      inputProps.onChange(event);
    }
  };

  handleInputKeyDown = (
    event
  ) => {
    this.handleKeyboard(event);

    const { inputProps } = this.props;
    if (inputProps && inputProps.onKeyDown) {
      inputProps.onKeyDown(event);
    }
  };

  handleInputKeyPress = (
    event
  ) => {
    this.handleKeyboard(event);

    const { inputProps } = this.props;
    if (inputProps && inputProps.onKeyPress) {
      inputProps.onKeyPress(event);
    }
  };

  handleKeyboard = (event) => {
    const { suggestions, suggestionIndex, inputQuery } = this.state;
    if (event.which === 40) {
      // Arrow down
      event.preventDefault();
      if (suggestionIndex < suggestions.length - 1) {
        const newSuggestionIndex = suggestionIndex + 1;
        const newInputQuery = suggestions[newSuggestionIndex].value;
        this.setState({
          suggestionIndex: newSuggestionIndex,
          query: newInputQuery
        });
      }
    } else if (event.which === 38) {
      // Arrow up
      event.preventDefault();
      if (suggestionIndex >= 0) {
        const newSuggestionIndex = suggestionIndex - 1;
        const newInputQuery =
          newSuggestionIndex === -1
            ? inputQuery
            : suggestions[newSuggestionIndex].value;
        this.setState({
          suggestionIndex: newSuggestionIndex,
          query: newInputQuery
        });
      }
    } else if (event.which === 13) {
      // Enter
      event.preventDefault();
      if (suggestionIndex >= 0) {
        this.selectSuggestion(suggestionIndex);
      }
    }
  };

  performFetchSuggestions = () => {
    const { minChars, token } = this.props;
    const { query } = this.state;

    // Проверяем на минимальное количество символов для отправки
    if (
      typeof minChars === 'number' &&
      minChars > 0 &&
      query.length < minChars
    ) {
      this.setState({ suggestions: [], suggestionIndex: -1 });
      return;
    }

    makeRequest(
      'POST',
      this.loadSuggestionsUrl,
      {
        headers: {
          Accept: 'application/json',
          Authorization: `Token ${token}`,
          'Content-Type': 'application/json'
        },
        json: this.getLoadSuggestionsData() || {}
      },
      (suggestions) => {
        this.setState({ suggestions, suggestionIndex: -1 });
      }
    );
  };

  onSuggestionClick = (
    index,
    event
  ) => {
    event.stopPropagation();
    this.selectSuggestion(index);
  };

  selectSuggestion = (index) => {
    const { suggestions } = this.state;
    const { onChange } = this.props;

    if (suggestions.length >= index - 1) {
      const suggestion = suggestions[index];
      this.setState(
        {
          query: suggestion.value,
          inputQuery: suggestion.value,
          displaySuggestions: false
        },
        () => {
          this.fetchSuggestions();
          setTimeout(() => this.setCursorToEnd(this.textInput));
        }
      );

      if (onChange) {
        onChange(suggestion);
      }
    }
  };

  setCursorToEnd = (element) => {
    if (element) {
      const valueLength = element.value.length;
      if (element.selectionStart || element.selectionStart === 0) {
        // eslint-disable-next-line no-param-reassign
        element.selectionStart = valueLength;
        // eslint-disable-next-line no-param-reassign
        element.selectionEnd = valueLength;
        element.focus();
      }
    }
  };

  getHighlightWords = () => {
    const { inputQuery } = this.state;
    const wordsToPass = [
      'г',
      'респ',
      'ул',
      'р-н',
      'село',
      'деревня',
      'поселок',
      'пр-д',
      'пл',
      'к',
      'кв',
      'обл',
      'д'
    ];
    let words = '';
    try {
      words = inputQuery.replace(',', '').split(' ');
    } catch (error) {
      console.log();
    }
    words = words.filter((word) => {
      return wordsToPass.indexOf(word) < 0;
    });
    return words;
  };

  /**
   * Функция, которая вернет уникальный key для списка React
   * @param suggestion
   */
  getSuggestionKey = (
    suggestion
  ) => suggestion.value;

  focus = () => {
    if (this.textInput) {
      this.textInput.focus();
    }
  };

  setInputValue = (value) => {
    this.setState({ query: value || '', inputQuery: value || '' });
  };

  renderOption = (suggestion) => { };

  render() {
    const {
      inputProps,
      hintText,
      containerClassName,
      hintClassName,
      suggestionsClassName,
      suggestionClassName,
      currentSuggestionClassName,
      children
    } = this.props;
    const {
      query,
      isFocused,
      suggestions,
      suggestionIndex,
      displaySuggestions
    } = this.state;

    return (
      <div
        className={containerClassName || 'react-dadata react-dadata__container'}
      >
        <div>
          <input
            autoComplete="off"
            className="react-dadata__input"
            {...inputProps}
            value={query}
            ref={(input) => {
              this.textInput = input;
            }}
            onChange={this.handleInputChange}
            onKeyPress={this.handleInputKeyPress}
            onKeyDown={this.handleInputKeyDown}
            onFocus={this.handleInputFocus}
            onBlur={this.handleInputBlur}
          />
        </div>
        {isFocused &&
          suggestions &&
          displaySuggestions &&
          suggestions.length > 0 && (
            <div
              className={suggestionsClassName || 'react-dadata__suggestions'}
            >
              {typeof hintText !== 'undefined' && (
                <div
                  className={hintClassName || 'react-dadata__suggestion-note'}
                >
                  {hintText}
                </div>
              )}
              {suggestions.map((suggestion, index) => {
                let suggestionClass =
                  suggestionClassName || 'react-dadata__suggestion';
                if (index === suggestionIndex) {
                  suggestionClass += ` ${currentSuggestionClassName ||
                    'react-dadata__suggestion--current'
                    }`;
                }
                return (
                  <button
                    key={this.getSuggestionKey(suggestion)}
                    onMouseDown={this.onSuggestionClick.bind(this, index)}
                    className={suggestionClass}
                  >
                    {this.renderOption(suggestion)}
                  </button>
                );
              })}
            </div>
          )}
        {children}
      </div>
    );
  }
}
