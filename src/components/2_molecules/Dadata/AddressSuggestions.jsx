/* eslint-disable react/jsx-indent */
/* eslint-disable import/prefer-default-export */
import React from 'react'
import { BaseSuggestions } from './BaseSuggestions'
import { HighlightWords } from './HighlightWords'

export class AddressSuggestions extends BaseSuggestions {
  loadSuggestionsUrl =
    'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address'

  getLoadSuggestionsData = () => {
    const {
      count,
      filterFromBound,
      filterToBound,
      filterLocations,
      filterLocationsBoost,
      filterLanguage,
    } = this.props
    const { query } = this.state

    const requestPayload = {
      query,
      count: count || 10,
    }

    // Ограничение поиска по типу
    if (filterFromBound && filterToBound) {
      requestPayload.from_bound = { value: filterFromBound }
      requestPayload.to_bound = { value: filterToBound }
    }

    // Язык подсказок
    if (filterLanguage) {
      requestPayload.language = filterLanguage
    }

    // Сужение области поиска
    if (filterLocations) {
      requestPayload.locations = filterLocations
    }

    // Приоритет города при ранжировании
    if (filterLocationsBoost) {
      requestPayload.locations_boost = filterLocationsBoost
    }

    return requestPayload
  }

  renderOption = (suggestion) => {
    const { renderOption, highlightClassName } = this.props

    return renderOption ? (
      renderOption(suggestion)
    ) : (
      <HighlightWords
        highlightClassName={highlightClassName || 'react-dadata--highlighted'}
        words={this.getHighlightWords() || ''}
        text={suggestion.value}
      />
    )
  }
}
