import styled from 'styled-components';

export const SignWrapper = styled.div`
  width: 100%;
  margin: 5px 0;
  padding: 20px 10px;
`;

export const SignHeader = styled.h2`

`;

export const SignText = styled.div`

`
export const SignUl = styled.ul`
  list-style: 'none';
  padding-inline-start: 10px;
  padding-top: 20px;
`

export const SignLi = styled.li`
  display: flex;
  align-items: center;
  margin-bottom: 20px;

  & svg{
    width: 20px;
    padding: 0 20px 0 0;
  }
`
