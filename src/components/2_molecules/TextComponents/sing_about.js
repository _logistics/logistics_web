import React from 'react'
import { SignWrapper } from './styles'
import { Button } from '@/components/1_atoms/Buttons'

const SignAbout = ({ btn }) => {
  return (
    <SignWrapper>
      <Button onClick={btn} reversed>Войти</Button>
    </SignWrapper>
  )
}

export default SignAbout
