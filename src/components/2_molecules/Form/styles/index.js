import styled from 'styled-components'
import { comStyles } from '@/utils/index';

export const RegisterWrapper = styled.div``;

export const LoginForm = styled.form`
  width: 80%;
  min-width: 290px;
  margin: auto;
  height: 100%;
  position: relative;
  background-color: rgba(255,255,255, .2);
  padding: 30px 0 20px 0;
`;

export const Registform = styled.form`
  width: 80%;
  min-width: 360px;
  margin: auto;
  height: 20%;
  position: relative;
  padding: 20px;
  background-color: rgba(255,255,255, .3);
  @media only screen and (max-width: 1024px) {
    min-width: 290px;
  }
`;

export const LogginButtonsSection = styled.div`

`;

export const PasswordForgot = styled.p`
  text-align: center;
  font-size: 12px;
  color: blue;
  font-weight: 700;
  cursor: pointer;
`

export const LoginFooter = styled.div`
  position: absolute;
  ${comStyles.flex('justify-center')};
  bottom: 0;
  color: #676767;
  transform: translateY(-180%);
  font-size: .8em;
`;
