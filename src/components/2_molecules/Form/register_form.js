/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState, useEffect } from 'react'
import { Registform } from './styles'
import { withTranslation } from '@/i18n'
import { Input } from '@/components/1_atoms/Input'
import { SignHeader } from '@/components/1_atoms/Headers'
import TwoButtons from '@/components/2_molecules/TwoButtons';
import { useStore } from '@/stores/init'
import { Radio } from 'antd'
import { openErrorNotifi } from '@/utils/index'
import { curry } from 'ramda'
import ReCAPTCHA from 'react-google-recaptcha';

const RegisterForm = ({ setStep, prefix }) => {
  const phoneName = `${prefix}phone`;
  const dataName = `${prefix}date`;
  const signRequests = localStorage.getItem(`${prefix}phone`);
  const phoneNameStep = signRequests ? 'repeatCode' : 'signPressPhone';
  const [formStep, setFormStep] = useState(phoneNameStep);
  const [isOrganization, setIsOrganization] = useState(false);
  const [personals, cPersonals] = useState({});
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }));
  const [phone, setPhone] = useState(signRequests || '');
  const { account } = useStore({})
  const [recapcha, setRecapcha] = useState(false);
  const removePhoneIfNeed = () => {
    const ld = localStorage.getItem(dataName);
    if (new Date(ld).getDate() !== new Date().getDate()) {
      localStorage.removeItem(dataName)
    }
  }
  useEffect(() => {
    removePhoneIfNeed()
  }, [])
  const setStoragePhone = () => {
    localStorage.setItem(phoneName, phone);
    localStorage.setItem(dataName, new Date());
  }
  const getFormData = () => {
    switch (formStep) {
      case 'signPressPhone':
        return {
          description: 'Введите номер телефона на который мы отправим пароль с кодом',
          fClick: () => {
            if (phone) {
              if (phone.length !== 10) {
                openErrorNotifi('Некорректный телефон', 'В номере телефона должно быть 10 символов, пример 9503302121')
                return null;
              }
              // setFormStep('signPressPassword');
              account.signup(phone).then((el) => {
                if (el.signup === 'сообщение успешно отправлено') {
                  setStoragePhone();
                  setFormStep('signPressPassword');
                }
              });
            } else {
              openErrorNotifi('Ошибка', 'Введите номер телефона');
            }
          },
          sClick: () => { setStep('login') },
          fText: 'Дальше',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: phone,
                  change: setPhone,
                  mask: {
                    onlyNumber: true

                  }
                }}
                type="name"
                label="телефон"
              />
              <p>Выберите лицо</p>
              <Radio.Group
                onChange={(e) => { setIsOrganization(e.target.value) }}
                value={isOrganization}
                className="mb2"
              >
                <Radio value>Юр. лицо</Radio>
                <Radio value={false}>Физ. лицо</Radio>
              </Radio.Group>
            </>
          )

        }
      case 'repeatCode':
        return {
          description: 'Мы отправили смс на выбранный номер',
          fClick: () => {
            setFormStep('signPressPassword');
          },
          sClick: () => { setStep('login') },
          fText: 'Дальше',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: phone,
                  change: setPhone,
                  mask: {
                    onlyNumber: true

                  }
                }}
                style={{ marginBottom: 5 }}
                type="name"
                label="телефон"
              />
              <p
                style={{ color: 'blue', cursor: 'pointer', display: 'flex', alignItems: 'center' }}
                onClick={() => {
                  if (phone) {
                    if (phone.length !== 10) {
                      openErrorNotifi('Некорректный телефон', 'В номере телефона должно быть 10 символов, пример 9503302121')
                      return null;
                    }
                    // setFormStep('signPressPassword');
                    account.signup(phone).then((el) => {
                      if (el.signup === 'сообщение успешно отправлено') {
                        setStoragePhone();
                        setFormStep('signPressPassword');
                      }
                    });
                  } else {
                    openErrorNotifi('Ошибка', 'Введите номер телефона');
                  }
                }}

              >
                <i className="fal fa-redo" style={{ marginRight: 10 }} />
                <span> Отправить еще раз</span>
              </p>
              {prefix === 's' ? (
                <>
                  <p>Выберите лицо</p>
                  <Radio.Group
                    onChange={(e) => { setIsOrganization(e.target.value) }}
                    value={isOrganization}
                    className="mb2"
                  >
                    <Radio value>Юр. лицо</Radio>
                    <Radio value={false}>Физ. лицо</Radio>
                  </Radio.Group>
                </>
              ) :
                null}
            </>
          )

        }
      case 'signPressPassword':
        return {
          description: 'Введите новый пароль',
          fClick: () => {
            if (!personals?.code) {
              openErrorNotifi('Ошибка', 'Вы не указали код')
              return null;
            }
            if (!personals?.pass) {
              openErrorNotifi('Ошибка', 'Введите пароль')
              return null;
            }
            if (personals?.pass.length < 5) {
              openErrorNotifi('Ошибка', 'Пароль должен быть более пяти символов')
              return null;
            }
            if (!personals?.confPass) {
              openErrorNotifi('Ошибка', 'Подтвердите пароль')
              return null;
            }
            if (personals?.pass !== personals?.confPass) {
              openErrorNotifi('Ошибка', 'Пароли не совпадают')
              return null;
            }
            account.submitSignup(phone,
              personals.pass,
              personals.confPass,
              isOrganization,
              personals?.code,
              [2]
            ).then((answer) => {
              if (answer) {
                setStep('login');
              }
            });
          },
          sClick: () => { setFormStep('repeatCode') },
          fText: 'Ввести пароль',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: personals?.code || '',
                  change: cData('code'),
                }}
                label="код из смс"
              />
              <Input
                input={{
                  value: personals?.pass || '',
                  change: cData('pass'),
                  type: 'password'
                }}
                label="пароль"
              />
              <Input
                input={{
                  value: personals?.confPass || '',
                  change: cData('confPass'),
                  type: 'password'
                }}
                label="повторите пароль"
              />
            </>
          )
        }
      default:
        return {
          description: 'Ошибка',
          fClick: () => { setStep('login') },
          sClick: () => { setStep('login') },
          input: null
        }
    }
  }
  const recapchaChange = (token) => {
    setRecapcha(token);
  }
  return (
    <Registform onSubmit={(e) => e.preventDefault()}>
      <SignHeader>
        <h1>Темп курьер</h1>
        <p>Личный кабинет для создания и управления курьерскими заказами</p>
      </SignHeader>
      {recapcha ? (
        <>
          <p style={{ fontSize: 16 }}>{getFormData().description}</p>
          <p style={{ color: 'gray', fontSize: '12px' }}>Пример телефона: 9133978678</p>
          {getFormData().input}
          <TwoButtons
            fText={getFormData().fText}
            sText={getFormData().sText}
            fClick={getFormData().fClick}
            sClick={getFormData().sClick}
          />
        </>
      ) :
        (
          <div style={{ paddingBottom: 30 }}>
            <p style={{ fontSize: 18, fontWeight: 500, color: '#605E5A', marginBottom: 10 }}>
              <i className="far fa-user-shield" style={{ marginRight: 5 }} />
              <span>Пройдите рекапчу</span>
            </p>
            <ReCAPTCHA
              sitekey="6LcqaiUaAAAAAOGvyRVgxqhHNhju0gYxpYmDmS5K"
              onChange={recapchaChange}
            />
          </div>
        )
      }
    </Registform>
  )
}

export default withTranslation(['auth', 'common'])(RegisterForm)
