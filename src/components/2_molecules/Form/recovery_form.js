/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { useState } from 'react'
import { Registform } from './styles'
import { withTranslation } from '@/i18n'
import { Input } from '@/components/1_atoms/Input'
import { SignHeader } from '@/components/1_atoms/Headers'
import TwoButtons from '@/components/2_molecules/TwoButtons';
import { useStore } from '@/stores/init'
import { Radio } from 'antd'
import { openErrorNotifi } from '@/utils/index'
import { curry } from 'ramda'

const RecoveryForm = ({ setStep }) => {
  const signRequests = localStorage.getItem('tryReg');
  const phoneNameStep = signRequests ? 'repeatCode' : 'signPressPhone';
  const [formStep, setFormStep] = useState(phoneNameStep);
  const [isOrganization, setIsOrganization] = useState(false);
  const [personals, cPersonals] = useState({});
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }));
  const [phone, setPhone] = useState(localStorage.getItem('sphone') || '');
  const { account } = useStore({})
  const getFormData = () => {
    switch (formStep) {
      case 'signPressPhone':
        return {
          description: 'Введите номер телефона на который мы отправим пароль с кодом',
          fClick: () => {
            if (phone) {
              if (phone.length !== 10) {
                openErrorNotifi('Некорректный телефон', 'В номере телефона должно быть 10 символов, пример 9503302121')
                return null;
              }
              // setFormStep('signPressPassword');
              account.signup(phone).then((el) => {
                if (el.signup === 'сообщение успешно отправлено') {
                  setFormStep('signPressPassword');
                }
              });
            } else {
              openErrorNotifi('Ошибка', 'Введите номер телефона');
            }
          },
          sClick: () => { setStep('login') },
          fText: 'Дальше',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: phone,
                  change: setPhone,
                  mask: {
                    onlyNumber: true

                  }
                }}
                type="name"
                label="телефон"
              />
              <p>Выберите лицо</p>
              <Radio.Group
                onChange={(e) => { setIsOrganization(e.target.value) }}
                value={isOrganization}
                className="mb2"
              >
                <Radio value>Юр. лицо</Radio>
                <Radio value={false}>Физ. лицо</Radio>
              </Radio.Group>
            </>
          )

        }
      case 'repeatCode':
        return {
          description: 'Мы отправили смс на выбранный номер',
          fClick: () => {
            setFormStep('signPressPassword');
          },
          sClick: () => { setStep('login') },
          fText: 'Дальше',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: phone,
                  change: setPhone,
                  mask: {
                    onlyNumber: true

                  }
                }}
                type="name"
                label="телефон"
              />
              <p
                style={{ color: 'blue', cursor: 'pointer' }}
                onClick={() => {
                  if (phone) {
                    if (phone.length !== 10) {
                      openErrorNotifi('Некорректный телефон', 'В номере телефона должно быть 10 символов, пример 9503302121')
                      return null;
                    }
                    // setFormStep('signPressPassword');
                    account.signup(phone).then((el) => {
                      if (el.signup === 'сообщение успешно отправлено') {
                        setFormStep('signPressPassword');
                      }
                    });
                  } else {
                    openErrorNotifi('Ошибка', 'Введите номер телефона');
                  }
                }}
              >Отправить еще раз
              </p>
              <p>Выберите лицо</p>
              <Radio.Group
                onChange={(e) => { setIsOrganization(e.target.value) }}
                value={isOrganization}
                className="mb2"
              >
                <Radio value>Юр. лицо</Radio>
                <Radio value={false}>Физ. лицо</Radio>
              </Radio.Group>
            </>
          )

        }
      case 'signPressPassword':
        return {
          description: 'Введите новый пароль',
          fClick: () => {
            if (!personals?.code) {
              openErrorNotifi('Ошибка', 'Вы не указали код')
              return null;
            }
            if (!personals?.pass) {
              openErrorNotifi('Ошибка', 'Введите пароль')
              return null;
            }
            if (personals?.pass.length < 5) {
              openErrorNotifi('Ошибка', 'Пароль должен быть более пяти символов')
              return null;
            }
            if (!personals?.confPass) {
              openErrorNotifi('Ошибка', 'Подтвердите пароль')
              return null;
            }
            if (personals?.pass !== personals?.confPass) {
              openErrorNotifi('Ошибка', 'Пароли не совпадают')
              return null;
            }
            account.submitSignup(phone,
              personals.pass,
              personals.confPass,
              isOrganization,
              personals?.code,
              [2]
            ).then((answer) => {
              if (answer) {
                setStep('login');
              }
            });
          },
          sClick: () => { setFormStep('repeatCode') },
          fText: 'Ввести пароль',
          sText: 'Назад',
          input: (
            <>
              <Input
                input={{
                  value: personals?.code || '',
                  change: cData('code'),
                }}
                label="код из смс"
              />
              <Input
                input={{
                  value: personals?.pass || '',
                  change: cData('pass'),
                  type: 'password'
                }}
                label="пароль"
              />
              <Input
                input={{
                  value: personals?.confPass || '',
                  change: cData('confPass'),
                  type: 'password'
                }}
                label="повторите пароль"
              />
            </>
          )
        }
      default:
        return {
          description: 'Ошибка',
          fClick: () => { setStep('login') },
          sClick: () => { setStep('login') },
          input: null
        }
    }
  }
  return (
    <Registform onSubmit={(e) => e.preventDefault()}>
      <SignHeader>
        Темп курьер
      </SignHeader>
      <p>{getFormData().description}</p>
      <p style={{ color: 'gray', fontSize: '12px' }}>Пример телефона: 9133978678</p>
      {getFormData().input}
      <TwoButtons
        fText={getFormData().fText}
        sText={getFormData().sText}
        fClick={getFormData().fClick}
        sClick={getFormData().sClick}
      />
    </Registform>
  )
}

export default withTranslation(['auth', 'common'])(RecoveryForm)
