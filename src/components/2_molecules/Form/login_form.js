// eslint-disable
import React, { useState } from 'react'
import { LoginForm, PasswordForgot } from './styles'
import { withTranslation } from '@/i18n'
import { SignHeader } from '@/components/1_atoms/Headers'
// import { Button } from '@/components/1_atoms/Buttons'
import { useStore } from '@/stores/init'
import { curry, pipe, path } from 'ramda'
import { formBuilder } from '@/components/0_functionals/builder'
// eslint-disable-next-line import/no-unresolved
import { stringify, parse, redirectIfNeed, openNotifi } from '@/utils/index'
import TwoButtons from '../TwoButtons'
import Devider from '@/components/1_atoms/Devider'
// import GoogleIcon from './styles/icons'

const fields = {
  params: [
    { name: 'phone', label: 'телефон', icon: 'phone', nonNullable: true, type: 'text', length: { min: 5, max: 30 } },
    { name: 'password', label: 'пароль', icon: 'password', nonNullable: true, type: 'password', length: { min: 5, max: 30 } },
  ],
  validate: {
    nonNil: ['password', 'phone']
  }
}


const LoginFormContainer = ({ t, setStep }) => {
  const [personals, cPersonals] = useState({});
  const cData = curry((name, val) => cPersonals({ ...personals, [name]: val }));
  const [errors, cErrors] = useState({});
  const { account } = useStore({});

  const noRefresh = (e) => e.preventDefault();

  const doTheJob = () => account.login(personals);

  const prettify = () => true;

  const validateIt = (accepted) => accepted ? doTheJob() : cErrors({});

  const checkAuth = () => {
    return setTimeout(() => {
      const phone = path(['user'], account)
      const answer = pipe(
        stringify(2, undefined),
        parse,
      )(phone)
      redirectIfNeed(answer && answer.phone);
      if (!answer || !answer.phone) {
        openNotifi('Ошибка', 'Неверный логин или пароль', 'error')
      }
    }, 1000);
  }

  const onSubmit = pipe(
    noRefresh,
    prettify,
    validateIt,
    checkAuth
  );

  return (
    <LoginForm onSubmit={onSubmit}>
      <SignHeader>
        <h1>{t('auth-header')}</h1>
        <p>Личный кабинет для создания и управления курьерскими заказами</p>
      </SignHeader>
      {formBuilder(fields, personals, cData, errors)}
      <p style={{ color: 'gray', fontSize: '12px' }}>Пример телефона: 9232072287</p>
      <TwoButtons
        fText="Войти"
        sText="Регистрация"
        sClick={() => {
          setStep('signup');
        }}
      />
      <Devider style={{ margin: '10px 0' }} />
      {/* <LogginButtonsSection style={{ display: 'none' }}>
        <Button mod="secondary" size="full">
          <GoogleIcon />
          {t('googleAuthButton')}
        </Button>
        <Button iconColor="#10239E" icon="fab fa-facebook-square" mod="secondary" size="full">
          {t('facebookAuthButton')}
        </Button>
        <Button iconColor="#4e7cb2" icon="fab fa-vk" mod="secondary" size="full">
          {t('vkAuthButton')}
        </Button>
      </LogginButtonsSection> */}
      <PasswordForgot>
        <span onClick={() => {
          setStep('recovery')
        }}
        >Забыли пароль?
        </span>
      </PasswordForgot>
    </LoginForm>
  )
}

export default withTranslation(['auth', 'common'])(LoginFormContainer)
