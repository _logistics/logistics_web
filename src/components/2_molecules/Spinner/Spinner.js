import React from 'react'
import PacmanLoader from 'react-spinners/PacmanLoader';

const Spinner = ({ loading, text = 'Загрузка' }) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%' }}>
      <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'end', justifyContent: 'center', width: 200, height: 420 }}>
        <PacmanLoader color="#003079" loading={loading} size={64} />
        <p style={{ color: '#003079', fontSize: 64, marginTop: 70, marginLeft: 20 }}>{text}</p>
      </div>
    </div>
  )
}

export default Spinner
