import React from 'react'
import { i18n } from '@/i18n'
import { Swither, LangContainer } from './styles'

const LangSwitcher = () => {
  const { changeLanguage } = i18n
  return (
    <LangContainer>
      <Swither onClick={() => changeLanguage('ru')}>
        ru
      </Swither>
      /
      <Swither onClick={() => changeLanguage('en')}>
        en
      </Swither>
    </LangContainer>
  )
}

export default React.memo(LangSwitcher)
