import styled from 'styled-components';
import { comStyles } from '@/utils/index';

// eslint-disable-next-line import/prefer-default-export
export const Swither = styled.div`
  color: grey;
  font-size: 18px;
  font-weight: bold;
  cursor: pointer;
`;

export const LangContainer = styled.div`
  font-size: 18px;
  font-weight: bold;
  color: grey;
  margin-right: 10px;
  ${comStyles.flex('jcc')}
`;
