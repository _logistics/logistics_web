import React from 'react'
import T from 'prop-types'
import { Wrapper } from './styles'

import { Button } from '@/components/1_atoms/Buttons'


const TwoButtons = ({ fText, fClick, fActive, sText, sClick, sActive }) => {
  const getFirstButton = () => {
    if (fActive) {
      return <Button reversed onClick={fClick}>{fText}</Button>
    }
  }

  const getSeccondButton = () => {
    if (sActive) {
      return <Button onClick={sClick}>{sText}</Button>
    }
  }

  return (
    <Wrapper>
      {getFirstButton()}
      {getSeccondButton()}
    </Wrapper>
  )
}

TwoButtons.propTypes = {
  fText: T.oneOfType([T.string, T.node]),
  fClick: T.func,
  fActive: T.bool,
  sText: T.oneOfType([T.string, T.node]),
  sClick: T.func,
  sActive: T.bool
}

TwoButtons.defaultProps = {
  fText: 'Button',
  fClick: console.log,
  fActive: true,
  sText: 'Button',
  sClick: console.log,
  sActive: true
  // eslint-disable-next-line no-console
}

export default React.memo(TwoButtons)
