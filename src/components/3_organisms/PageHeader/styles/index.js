/* eslint-disable import/prefer-default-export */
import styled from 'styled-components'
import { theme } from '@/utils/index'

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  padding: 15px 0;
  padding-left: 20px;
  border-bottom: 1px solid ${theme('neitralColors.neutral4')};
  & h1 {
    margin-bottom: 0;
    font-size: 20px;
  }
  @media only screen and (max-width: 700px) {
    display: flex;
    justify-content: center;
  }
`
