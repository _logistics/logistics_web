import React from 'react'
import { Wrapper } from './styles'

const PageHeader = ({ children }) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  )
}

export default PageHeader
