import styled from 'styled-components'
import { theme } from '@/utils/index'

/* eslint-disable import/prefer-default-export */
export const Wrapper = styled.span`
  padding: 15px 0;
  padding-left: 10px;
  display: flex;
  text-decoration: none;
  align-items: center;
  transition: all 0.3s;
  cursor: pointer;
  color: ${({ active }) =>
    active ? theme('primaryColor.color6') : theme('neitralColors.neutral10')};
  border-right: ${({ active }) => (active ? '3px solid' : 'none')};
  background-color: ${({ active }) =>
    active ? theme('primaryColor.color1') : 'none'};
  border-color: ${theme('primaryColor.color6')};
  & span.icon-container {
    display: inline-block;
    width: 40px;
    justify-content: center;
    text-align: center;
    margin-right: 5px;
  }
  & span.menuItem-text {
    font-size: 14px;
    transition: all 0.6s;
    @media (max-width: ${theme('sreenWidth.tablet')}) {
      opacity: 0;
      font-size: 0px;
    }
  }
  &:hover {
    background-color: ${({ active }) =>
      active
        ? theme('primaryColor.color1')
        : theme('sidebar.menuItem.hoverLight')};
    color: ${({ active }) =>
      active
        ? theme('primaryColor.color6')
        : theme('sidebar.menuItem.hoverDark')};
  }
  & i {
    font-size: 24px;
  }
`
