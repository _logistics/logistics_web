import React from 'react'
import { Wrapper } from './styles'
import { useRouter } from 'next/router'
import Link from 'next/link'

const MenuItem = ({ icon = 'fal fa-icons', children = '', link = '/#' }) => {
  const router = useRouter()
  const activeLink = link === router.pathname

  return (
    <Link
      href={activeLink ? '#' : link}
      active={activeLink}
      className="sideBar-menuItem"
    >
      <Wrapper active={activeLink} className="sideBar-menuItem">
        <span className="icon-container">
          <i className={icon} />
        </span>
        <span className="menuItem-text">{children}</span>
      </Wrapper>
    </Link>
  )
}

export default MenuItem
