import { useEffect } from 'react'

let store = null

export const setPin = () => {
  if (store.pin) {
    store.mark({ sortOptActive: false })
  }
  store.mark({ pin: !store.pin })
}


export const useInit = (_store) => {
  useEffect(() => {
    store = _store

    return () => { }
  }, [_store])
}
