import React from 'react'
import { connectStore, randKey } from '@/utils/index'
import { Wrapper, SideBarLogotype } from './styles'
import { useInit } from './logic'
import MenuItem from './MenuItem/MenuItem'
import { getArrayOfValuesByKey } from 'core_js_devbi'
import { useStore } from '@/stores/init'

const SidebarContainer = ({ sidebar: store }) => {
  useInit(store);
  const { account } = useStore({});
  const getRoutes = () => {
    const rolesId = getArrayOfValuesByKey(account.user.roles, 'id');
    if (!rolesId) {
      return []
    }
    const adminRoutes = [
      {
        name: 'Создание заказа',
        link: '/',
        icon: 'fal fa-cart-plus'
      },
      {
        name: 'Статистика',
        link: '/statistic',
        icon: 'fal fa-chart-line'
      },
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Заказы админка',
        link: '/orders-admin',
        icon: 'fal fa-hand-holding-box'
      },
      {
        name: 'Пользователи',
        link: '/account/search',
        icon: 'fal fa-users'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    const userRoutes = [
      {
        name: 'Создание заказа',
        link: '/',
        icon: 'fal fa-cart-plus'
      },
      {
        name: 'Статистика',
        link: '/statistic',
        icon: 'fal fa-chart-line'
      },
      {
        name: 'Контакты',
        link: '/contacts',
        icon: 'fal fa-map-signs'
      },
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    const curierRoutes = [
      {
        name: 'История заказов',
        link: '/orders-history',
        icon: 'fal fa-clipboard-list-check'
      },
      {
        name: 'Настройки',
        link: '/account',
        icon: 'fal fa-cog'
      }
    ]
    switch (true) {
      case rolesId.includes(1):
        return adminRoutes;
      case rolesId.includes(3):
        return adminRoutes
      case rolesId.includes(4):
        return adminRoutes;
      case rolesId.includes(5):
        return curierRoutes
      case rolesId.includes(2):
        return userRoutes;
      default:
        return [];
    }
  }

  const { pin } = store

  const createMenuList = () => {
    return getRoutes().map((el) => (
      <MenuItem link={el.link} icon={el.icon} key={randKey()}>{el.name}</MenuItem>
    ))
  }

  return (
    <Wrapper pin={pin} testid="sidebar">
      <SideBarLogotype className="sidebar_logotype">
        <span className="mainSimbol">Т</span>
        <span className="secondarySimbol">емп</span>
        <span className="mainSimbol">К</span>
        <span className="secondarySimbol">урьер</span>
      </SideBarLogotype>
      {createMenuList()}
    </Wrapper>
  )
}
SidebarContainer.displayName = 'SidebarContainer'

export default connectStore(SidebarContainer)
