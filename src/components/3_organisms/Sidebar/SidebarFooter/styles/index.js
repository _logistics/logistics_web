import styled from 'styled-components';
import { comStyles } from '@/utils/index';

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.div`
  position: absolute;
  bottom: 60px;
  left: 0;
  border-top: 1px solid #e2e2e2;
  padding-bottom: 15px;
  width: 100%;
  height: 50px;
  ${comStyles.flex('jcc')};
  align-items: center;
  &>div:first-child{
    margin-left: 50%;
    transform: translatex(-50%);
  }
  &:hover>div:first-child{
    margin-left: 30px;
  }
  &>div:last-child{
    display: none;
  }
  &:hover>div:last-child{
    display: flex;
  }
`;

export const LeftAssigned = styled.div`
    position: absolute;
    right: 10px;
    top: 50%;
    transform: translateY(-20px);
    width: 80px;
    ${comStyles.flex('jcc')}
`;
