import React from 'react'
import { PinButton } from '@/components/1_atoms/Buttons'
import { Wrapper, LeftAssigned } from './styles'
import SettingsButton from '@/components/1_atoms/Buttons/SettingsButton'
import LangSwitcher from '@/components/2_molecules/LangSwitcher'

const SidebarFooter = ({ pin, setPin }) => {
  return (
    <Wrapper>
      <div className="">
        <SettingsButton />
      </div>
      <LeftAssigned>
        <LangSwitcher />
        <PinButton pin={pin} setPin={setPin} />
      </LeftAssigned>

    </Wrapper>
  )
}

export default SidebarFooter
