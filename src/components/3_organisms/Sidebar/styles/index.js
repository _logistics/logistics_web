import styled from 'styled-components'
import { comStyles, theme } from '@/utils/index'

// eslint-disable-next-line import/prefer-default-export
export const Wrapper = styled.aside.attrs((props) => ({
  'data-testid': props.testid,
}))`
  ${comStyles.flexColumn()};
  position: fixed;
  display: flex;
  flex-direction: column;
  height: 100vh;
  top: 0;
  border-right: 1px solid ${theme('neitralColors.neutral4')};
  width: ${theme('sidebar.widthBigBar')};
  background: ${theme('sidebar.bg')};
  z-index: ${comStyles.zIndex.sidebar};
  & div.sidebar_logotype {
    span.secondarySimbol {
      transition: all .3s;
      @media (max-width: ${theme('sreenWidth.tablet')}) {
        font-size: 0px;
      }
    }
  }
  @media (max-width: ${theme('sreenWidth.tablet')}) {
    width: ${theme('sidebar.widthSmallBar')};
  }
  &:hover {
    transition-delay: 0.1s;
    width: ${theme('sidebar.widthBigBar')};
    & div.sidebar_logotype {
      justify-content: start;
      padding-left: 20px;
      span.secondarySimbol {
        font-size: 24px;       
    }}
    @media (max-width: ${theme('sreenWidth.tablet')}) {
      box-shadow: 4px 4px 12px rgba(24, 10, 38, 0.15);
    }
    & a span.menuItem-text {
      opacity: 1;
      font-size: 14px
    }
  }
  @media (max-width: 800px) {
    display: none;
  }
  transition: width 0.2s, opacity 0.8s, box-shadow 0.1s linear 0.1s,
    background-color 0.3s, z-index 0.5s;

  ${comStyles.media.maxContent`
    left: 0;
  `};
`;

export const SideBarLogotype = styled.div`
  min-height: 64px;
  width: 100%;
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${theme('neitralColors.neutral4')};
  padding-left: 20px;
  margin-bottom: 25px;
  font-size: 24px;
  transition: all .3s;
  color: ${theme('primaryColor.color5')};
  @media (max-width: ${theme('sreenWidth.tablet')}) {
    padding-left: 0px;
    justify-content: center;
  }
`
