import styled from 'styled-components'

import { theme, themeCoverMap, themeCoverIndexMap, comStyles } from '@/utils/index'


// eslint-disable-next-line import/prefer-default-export
export const Dot = styled.div`
  ${comStyles.circle('25px')};

  margin-right: 10px;
  background: ${({ name }) => themeCoverMap[name]};
  border: ${() => '1px solid lightgrey'};
  position: relative;
  cursor: pointer;
  background-color: ${({ name }) => themeCoverMap[name]};
  color: ${({ active, name }) =>
    active ? theme('bodyBg') : themeCoverMap[name]};

  /* &:after {
    content: 'T';
    position: absolute;
    color: ${({ active, name }) => (active ? themeCoverIndexMap[name] : '')};
    top: 13%;
    left: 34%;
  } */
`
