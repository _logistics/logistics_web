import styled from 'styled-components'

import { comStyles } from '@/utils/index'
import { Dot } from './index'

export const Wrapper = styled.div`
  ${comStyles.flex('jcc')};
  background-color: 'grey';
`

export const ThemeDot = styled(Dot)``
