import React from 'react'
import { keys } from 'ramda'

import { themeSkins } from '@/utils/index'
import { Wrapper, ThemeDot } from './style/dot_selector'

const DotSelector = ({ curTheme, changeTheme }) => {
  return (
    <Wrapper>
      {keys(themeSkins).map((name) => (
        <div key={name} style={{ backgroundColor: 'grey', padding: '30px' }}>
          {name}:
          <ThemeDot
            key={name}
            active={curTheme === name}
            name={name}
            onClick={() => changeTheme(name)}
          />
        </div>
      ))}
    </Wrapper>
  )
}

export default React.memo(DotSelector)
