import React from 'react'
import T from 'prop-types';

import DotSelector from './DotSelector'

const ThemeSelector = ({ displayStyle, curTheme, changeTheme }) => {
  return displayStyle === 'default' ? (
    <DotSelector curTheme={curTheme} changeTheme={changeTheme} />
  ) : (
    <>{null}</>
  )
}

ThemeSelector.propTypes = {
  curTheme: T.string,
  displayStyle: T.oneOf(['default', 'card']),
  changeTheme: T.func.isRequired
}

ThemeSelector.defaultProps = {
  curTheme: '',
  displayStyle: 'default',
}

export default React.memo(ThemeSelector)
