import { useState, useEffect } from 'react'
import { merge } from 'ramda'
import { isGlobal } from '@/vars/index'

const inialPlatform = {
  isChrome: true,
  isFirefox: false,
  isSafari: false,
  isIE: false,
  isEdge: false,
  isMacOS: false,
  isMobile: false,
}

const usePlatform = () => {
  const [platform, setPlatform] = useState(inialPlatform)

  /* eslint-disable */
  useEffect(() => {
    const isFirefox = typeof InstallTrigger !== 'undefined'

    const isSafari =
      /constructor/i.test(isGlobal.HTMLElement) ||
      (function (p) {
        return p.toString() === '[object SafariRemoteNotification]'
      })(
        !isGlobal.safari ||
          (typeof safari !== 'undefined' && safari.pushNotification),
      )

    const isIE = /*@cc_on!@*/ false || !!document.documentMode

    const isEdge = !isIE && !!isGlobal.StyleMedia

    const isChrome =
      !!isGlobal.chrome && (!!isGlobal.chrome.webstore || !!isGlobal.chrome.runtime)

    const isMacOS = isGlobal.navigator.appVersion.indexOf('Mac') != -1
    /* eslint-enable */

    const isMobile = isGlobal.innerWidth <= 800 && window.innerHeight <= 600

    setPlatform(
      merge(inialPlatform, {
        isFirefox,
        isSafari,
        isIE,
        isEdge,
        isChrome,
        isMacOS,
        isMobile,
      }),
    )

    return () => {}
  }, [])

  return platform
}

export default usePlatform
