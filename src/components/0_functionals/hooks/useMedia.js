import { useState, useEffect } from 'react'
import { merge } from 'ramda'

import { comStyles, runAfter } from '@/utils/index'
import { isGlobal } from '@/vars/index'

const defaultMedia = {
  mobile: false,
  tablet: false,
  laptop: false,
  desktop: false,
}

const useMedia = () => {
  const [media, setMedia] = useState(defaultMedia)

  useEffect(() => {
    const handleResize = () => {
      const { innerWidth } = isGlobal

      if (innerWidth <= comStyles.mediaBreakPoints.mobile) {
        setMedia(merge(defaultMedia, { mobile: true }))
      } else if (
        innerWidth > comStyles.mediaBreakPoints.mobile &&
        innerWidth <= comStyles.mediaBreakPoints.tablet
      ) {
        setMedia(merge(defaultMedia, { tablet: true }))
      } else if (
        innerWidth > comStyles.mediaBreakPoints.tablet &&
        innerWidth <= comStyles.mediaBreakPoints.laptop
      ) {
        setMedia(merge(defaultMedia, { laptop: true }))
      } else {
        setMedia(merge(defaultMedia, { desktop: true }))
      }
    }

    handleResize()
    isGlobal.addEventListener('resize', runAfter(handleResize, 200))

    return () => {
      isGlobal.removeEventListener('resize', handleResize)
    }
  }, [])

  return media
}

export default useMedia
