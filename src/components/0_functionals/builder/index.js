/* eslint-disable react/jsx-closing-tag-location */
/* eslint-disable react/jsx-key */
import React from 'react'
import { rejectEmptyNames } from '@/utils/validators'
import { Input } from '@/components/1_atoms/Input'
import { Wrapper } from './styles'
import { getArrayOfValuesByKey, getObjectByKeyAndValueFromArray } from 'core_js_devbi'
import { notification } from 'antd';

export const formBuilder = (fields, object, cData, error, size, disabled) => {
  fields = rejectEmptyNames(fields.params)
  return (<Wrapper size={size}>
    {fields.map((el) => {
      const { name, type, icon, size, marginR, label, mask, disabled: iDisabled } = el
      return (
        <Input
          input={{
            value: object ? object[name] : '',
            change: cData(name),
            type,
            mask
          }}
          styles={{
            marginR,
            size
          }}
          disabled={disabled || iDisabled}
          size={size}
          error={error[name] || null}
          label={label || name}
          icon={icon}
        />)
    })}
  </Wrapper>
  )
}

const openNotificationWithIcon = (title, description) => {
  notification.error({
    message: title,
    description,
    placement: 'bottomRight'
  });
};

export const checkValidForm = (formFState, fParams, formName = '') => {
  let valid = true;
  const fieldsNames = getArrayOfValuesByKey(fParams, 'name');
  fieldsNames.forEach((el) => {
    const errorObj = getObjectByKeyAndValueFromArray('name', el, fParams)
    if (!formFState[el] && errorObj.error) {
      openNotificationWithIcon(`Вы заполнили не все поля ${formName}`, errorObj.error);
      valid = false;
    }
  })
  return valid;
}
