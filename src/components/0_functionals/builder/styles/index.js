import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  max-width: ${({ size }) => size || '100%'};
`

export const SettingsCont = styled.div`
  width: 100%;
  padding: 30px 24px;
`
